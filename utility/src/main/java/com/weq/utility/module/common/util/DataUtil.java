package com.weq.utility.module.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weq.utility.configuration.UtilityProperties;
import com.weq.utility.module.common.dao.GenericDao;

@Component
public class DataUtil {

	private static final Logger logger = Logger.getLogger(DataUtil.class);

	@Autowired
	private GenericDao genericDao;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@Autowired
	private UtilityProperties utilityProperties;

	public  String convertListToCommaSeparated(List<?> objectList,String fieldName) throws Exception {
		String dataList="";
		
		if(objectList!=null && objectList.size() > 0) {
			for(Object obj :objectList) {
				if(obj instanceof String) {
					dataList=dataList+obj+",";
				}else {
					 Class<?> clazz = obj.getClass();
					 Field field = clazz.getDeclaredField(fieldName);
					 field.setAccessible(true);
					 Object fieldValue = field.get(obj);
					 dataList=dataList+fieldValue+",";
				}
				
			}
			if(dataList!=null && !dataList.equalsIgnoreCase("")) {
				dataList=dataList.substring(0, dataList.length()-1);
			}
		}
			
		return dataList;	
	}
	
	
	public  <T> List<T> convertCommaSeparatedToList(String fieldValue,Class<T> className,String fieldName,boolean fetchMaster) throws Exception {
		List<T> resultList =new ArrayList<T>();
		
		if(fieldValue==null || fieldValue.equalsIgnoreCase("")) {
			return resultList;
		}
		
		
		List<String> dataList = Arrays.asList(fieldValue.split("\\s*,\\s*"));
		
		
		
		for(String value:dataList) {
			if(value!=null && ! value.equalsIgnoreCase("")) {
				Class<T> classObj = className;
				T obj = classObj.getConstructor().newInstance();
				Field field = classObj.getDeclaredField(fieldName);
				field.setAccessible(true);
							
				if(obj instanceof String) {
					field.set(obj, value);
				}else {
					if(fetchMaster) {
						field.set(obj, Integer.parseInt(value));
						obj=(T) genericDao.findOneInt(Integer.parseInt(value), classObj);	
					}else {
						field.set(obj, value);
					}
					
					
				}
				
				
				if(obj!=null) {
					resultList.add(obj);
				}
				
				
			}
		
			
			
		}
		
		
		return resultList;	
	}
	

	public void sendFCMNotification(String tokenId, String title, String message) {
		logger.info("DataUtil->>> sendFCMNotification Start-->");
		logger.info("DataUtil->>> sendFCMNotification TokeId--> "+tokenId);
		logger.info("DataUtil->>> sendFCMNotification message--> "+message);
		//String server_key = "AAAA5bGvBd8:APA91bHbQycb9Ran3ZcL6TANOjvWxtKFQhkLLoaZN3Wn_80KFDl4QAyBnVVQX6oL03JbHy9F3OYE4nfJyY4WhDJZij4I-2oZhSyzLvtrIHbWGBHqHBlpwTywGSXmuSRGDM8TVPr0fwia";
		String server_key = "";
		System.out.println("server_key :" + server_key);
		System.out.println("tokenId :" + tokenId);
		String FCM_URL = "https://fcm.googleapis.com/fcm/send";
		try {
			// Create URL instance.
			URL url = new URL(FCM_URL);
			// create connection.
			HttpURLConnection conn;
			conn = (HttpURLConnection) url.openConnection();
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			// set method as POST or GET
			conn.setRequestMethod("POST");
			// pass FCM server key
			conn.setRequestProperty("Authorization", "key=" + server_key);
			// Specify Message Format
			conn.setRequestProperty("Content-Type", "application/json");
			// Create JSON Object & pass value
			JSONObject infoJson = new JSONObject();

			infoJson.put("title", title);
			infoJson.put("body", message);

			JSONObject json = new JSONObject();
			json.put("to", tokenId.trim());
			// json.put("registration_ids",tokenId);
			json.put("data", infoJson);

			System.out.println("json :" + json.toString());
			System.out.println("infoJson :" + infoJson.toString());
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(json.toString());
			wr.flush();
			int status = 0;
			if (null != conn) {
				status = conn.getResponseCode();
			}
			System.out.println("status=" + status);
			if (status != 0) {

				if (status == 200) {
					// SUCCESS message
					BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					System.out.println("Android Notification Response : " + reader.readLine());
					logger.info("DataUtil->>> sendFCMNotification Android Notification Response 200 --> "+reader.readLine());
				} else if (status == 400) {
					// client side error
					System.out.println(
							"Notification Response : [ Syntax Error ]TokenId : " + tokenId + " Error occurred :");
					logger.info("DataUtil->>> sendFCMNotification Android Notification Response Error occurred 400 --> "+tokenId);
				} else if (status == 401) {
					// client side error
					System.out.println("Notification Response : TokenId : " + tokenId + " Error occurred :");
					logger.info("DataUtil->>> sendFCMNotification Android Notification Response Error occurred 401 --> "+tokenId);
				} else if (status == 501) {
					// server side error
					System.out.println("Notification Response : [ errorCode=ServerError ] TokenId : " + tokenId);
					logger.info("DataUtil->>> sendFCMNotification Android Notification Response 401 --> "+tokenId);
				} else if (status == 503) {
					// server side error
					System.out.println("Notification Response : FCM Service is Unavailable TokenId : " + tokenId);
					logger.info("DataUtil->>> sendFCMNotification Notification Response : FCM Service is Unavailable TokenId 503 --> "+tokenId);
				}
			}
		} catch (MalformedURLException mlfexception) {
			// Prototcal Error
			System.out.println("Error occurred while sending push Notification!.." + mlfexception.getMessage());
			logger.info("DataUtil->>> sendFCMNotification Error occurred while sending push Notification! --> "+ mlfexception.getMessage());
		} catch (Exception mlfexception) {
			// URL problem
			System.out.println(
					"Reading URL, Error occurred while sending push Notification!.." + mlfexception.getMessage());
			logger.info("DataUtil->>> sendFCMNotification Error occurred while sending push Notification! --> "+ mlfexception.getMessage());
		}
		
		logger.info("DataUtil->>> sendFCMNotification End-->");
	}
	
	public <T> String objectToJson( T object) throws Exception {
		ObjectMapper mapper = new ObjectMapper(); 
		String json = mapper.writeValueAsString(object); 		
		return json;
	}
	
	public <T> T jsonToObject(String json,Class<T> className) throws Exception {
		ObjectMapper mapper = new ObjectMapper(); 
		Class<T> classObj = className;
		T obj = classObj.getConstructor().newInstance();
		
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		obj = mapper.readValue(json, classObj);	
		
		return obj;
	}
	
	public String newLineToCommaSeparated(String textValue) throws Exception {
		 String commaSeparatedValue="";
	 	if(textValue!=null)
	 	{
		    String [] textValueList=textValue.split("\n");
		    
		    for(String separatedValue : textValueList)
		    {
		    	if(separatedValue!="" || separatedValue!=null || separatedValue!=" ")
		    	{
		    		commaSeparatedValue=commaSeparatedValue+"'"+separatedValue+"',";
		
		    	}
		    }
		    
		    if(commaSeparatedValue!=null && !commaSeparatedValue.equalsIgnoreCase("")) {
		    	commaSeparatedValue=commaSeparatedValue.substring(0, commaSeparatedValue.length()-1);
			}
			
	 	}
	 	
	 	return commaSeparatedValue;
	}
	
	public  List<String> newLineToStringList(String textValue) throws Exception {
		 List<String> stringList=new ArrayList<String>();
	 	if(textValue!=null)
	 	{
		    String [] textValueList=textValue.split("\n");
		    
		    for(String separatedValue : textValueList)
		    {
		    	if(separatedValue!="" || separatedValue!=null || separatedValue!=" ")
		    	{
		    		stringList.add(separatedValue);
		
		    	}
		    }
		    
		
			
	 	}
	 	
	 	return stringList;
	}
	
	public File convertMultipartToFile(MultipartFile file) throws IOException
	{    
	    File convFile = new File(utilityProperties.getDocumentUpload()+UUID.randomUUID().toString()+"-"+file.getOriginalFilename());
	    convFile.createNewFile(); 
	    FileOutputStream fos = new FileOutputStream(convFile); 
	    fos.write(file.getBytes());
	    fos.close(); 
	    return convFile;
	}
	
	public String exportToExcel(List<String> headingList, List<List<String>> dataList, String fileName) {

		XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(fileName);
		Calendar calendarDate = Calendar.getInstance();
		calendarDate.setTime(new Date());

		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");

		fileName += "-"+df.format(new Date())+".xlsx";
		
        String excelFilePath = utilityProperties.getDocumentUpload()+fileName; // master.documentUpload=/usr/share/tomcat8/webapps/tempfolder/

        try {
        
        	saveHeaderInExcel(workbook, sheet, headingList);
        	saveDataInExcel(workbook, sheet, dataList);

	        FileOutputStream outputStream = new FileOutputStream(excelFilePath);
	        workbook.write(outputStream);
	        workbook.close();

		} catch (IOException e) {
		        System.out.println("File IO error:");
		        e.printStackTrace();
		}
        
		String newFileFullPath = utilityProperties.getReportFullPathFolder()+fileName; // report.fullPathFolder=/usr/share/nginx/html/templilac/
		
		File fileOld = new File(excelFilePath); 
		File fileNew = new File(newFileFullPath);

        // renaming the file and moving it to a new location 
        if(fileOld.renameTo 
           (fileNew)) 
        { 
            // if file copied successfully then delete the original file 
        	fileOld.delete(); 
        	   
    	    if (fileNew.exists()) {
    	    	boolean readFlag = fileNew.setReadable(true,false);
    	    	logger.info("Datautil->>> convertMultipartToFile readFlag Permission --> "+readFlag);
    	    	boolean writeFlag = fileNew.setWritable(true,false);
    	    	logger.info("Datautil->>> convertMultipartToFile writeFlag Permission --> "+writeFlag);
    	    }
        	
            System.out.println("File moved successfully"); 
        } 
        else
        { 
            System.out.println("Failed to move the file"); 
        } 

        
        
        return utilityProperties.getReportFolder()+fileName; // report.folder=/templilac/

	}



	private void saveDataInExcel(XSSFWorkbook workbook, XSSFSheet sheet, List<List<String>> dataList) {

        int rowsCount = 1;

		for (List<String> rowData : dataList) {
			
	        Row row = sheet.createRow(rowsCount++);
	        int colsCount = 0;
			
			for (String columnData : rowData) {
				
				Cell cell = row.createCell(colsCount++);
	        	cell.setCellValue(columnData);
				
			}
			
		}
		
	}


	private void saveHeaderInExcel(XSSFWorkbook workbook, XSSFSheet sheet, List<String> headingList) {

        int rowsCount = 0;

        Row row = sheet.createRow(rowsCount);
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFillForegroundColor(IndexedColors.AQUA.getIndex());
        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        // Creating header
        
        int colsCount = 0;
        
        for (String header: headingList) {
	        Cell cell = row.createCell(colsCount++);
	        cell.setCellValue(header);
	        cell.setCellStyle(headerCellStyle);
		}
		
	}
		

}

package com.weq.utility.module.common.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.weq.utility.module.common.model.SequenceMaster;


@Repository
public interface SequenceDao extends JpaRepository<SequenceMaster, Integer>{

	@Query(value="Select s from SequenceMaster s where s.type=:type ")
	SequenceMaster getSequence(@Param("type") String type);
}
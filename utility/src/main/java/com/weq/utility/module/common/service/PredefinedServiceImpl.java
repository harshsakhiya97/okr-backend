package com.weq.utility.module.common.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.weq.utility.module.aws.AmazonS3DTO;
import com.weq.utility.module.aws.service.AwsService;
import com.weq.utility.module.common.dao.GenericDao;
import com.weq.utility.module.common.model.PredefinedMaster;
import com.weq.utility.module.common.util.DataUtil;

@Service
public class PredefinedServiceImpl implements PredefinedService{

	private static final Logger logger = Logger.getLogger(PredefinedServiceImpl.class);

	@Autowired
	private GenericDao genericDao;
	
	@Autowired
	private AwsService awsService;
	
	@Autowired
	private DataUtil dataUtil;
	
	@Override
	public PredefinedMaster savePredefinedMaster(PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedServiceImpl->>> savePredefinedMaster Start data -->"+predefinedMaster);
		if(predefinedMaster.getPredefinedId() == null || predefinedMaster.getPredefinedId() == 0) {
			predefinedMaster.setIsDelete("N");
			predefinedMaster = (PredefinedMaster) genericDao.save(predefinedMaster);
		}else {
			predefinedMaster = (PredefinedMaster) genericDao.update(predefinedMaster);
		}
		logger.info("PredefinedServiceImpl->>> savePredefinedMaster End");
		return predefinedMaster;
	}

	@Override
	@Transactional
	public void deletePredefinedMaster(Integer predefinedId) throws Exception {
		logger.info("PredefinedServiceImpl->>> deletePredefinedMaster Start Id -->"+predefinedId);
		
		String query = "Update PredefinedMaster set isDelete='Y' where predefinedId='"+predefinedId+"'  ";
		genericDao.customUpdate(query);
		
		/*
		PredefinedMaster predefinedMaster = (PredefinedMaster) genericDao.findOneInt(predefinedId, PredefinedMaster.class);
		if (predefinedMaster != null) {
			predefinedMaster.setIsDelete("Y");
			this.savePredefinedMaster(predefinedMaster);
		}*/
		logger.info("PredefinedServiceImpl->>> deletePredefinedMaster End Id");
	}

	@Override
	public List<PredefinedMaster> getPredefinedListByType(PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedServiceImpl->>> getPredefinedListByType Data type-->"+predefinedMaster.getEntityType());
		
		String query = "from PredefinedMaster where isDelete='N' and entityType='"+predefinedMaster.getEntityType()+"' ";
		
		if(predefinedMaster.getPredefinedId()!=null) {
			query = query + " and predefinedId="+predefinedMaster.getPredefinedId();
		}
		if(predefinedMaster.getParentPredefinedMaster()!=null && predefinedMaster.getParentPredefinedMaster().getPredefinedId()!=null) {
			query = query + " and parentPredefinedId="+predefinedMaster.getParentPredefinedMaster().getPredefinedId();
		}
		if(predefinedMaster.getParentPredefinedId()!=null) {
			query = query + " and parentPredefinedId="+predefinedMaster.getParentPredefinedId();
		}
		if(predefinedMaster.getSortOrder()!=null && !predefinedMaster.getSortOrder().isEmpty()) {
			query+=" order by displayOrder "+predefinedMaster.getSortOrder();
		}
		
		List<PredefinedMaster> preDefinedList = genericDao.findCustomList(query);
		List<PredefinedMaster> preDefinedList1;
		String query1 = "";
		for(PredefinedMaster predefinedMaster1:preDefinedList) {
			query1 = "from PredefinedMaster where isDelete='N' and predefinedId='"+predefinedMaster1.getParentPredefinedId()+"' ";
			preDefinedList1 = genericDao.findCustomList(query1);
			if(preDefinedList1.size()>0)
				predefinedMaster1.setParentName(preDefinedList1.get(0).getName());
		}
		
		
		return genericDao.findCustomList(query);
	}

	@Override
	public PredefinedMaster getPredefinedByCode(String locationCode) throws Exception {
		logger.info("PredefinedServiceImpl getPredefinedByCode START ");
		List<PredefinedMaster> predefinedMasterList=new ArrayList<PredefinedMaster>();
		predefinedMasterList=genericDao.findCustomList("from PredefinedMaster where isDelete='N' and code='"+locationCode+"'");
		if(predefinedMasterList!=null && predefinedMasterList.size() > 0) {
			logger.info("PredefinedServiceImpl getPredefinedByCode END");
			return predefinedMasterList.get(0);
		}else {
			logger.info("PredefinedServiceImpl getPredefinedByCode ELSE END");
			return null;
		}
	}

	@Override
	public PredefinedMaster getPredefinedByName(String name,String entityType) throws Exception {
		logger.info("PredefinedServiceImpl getPredefinedByName START ");
		List<PredefinedMaster> predefinedMasterList=new ArrayList<PredefinedMaster>();
		predefinedMasterList = genericDao.findCustomList("from PredefinedMaster  where isDelete='N' and entityType = '"+entityType+"' and name = '"+name+"'");
		if(predefinedMasterList != null && predefinedMasterList.size() > 0) {
			logger.info("PredefinedServiceImpl getPredefinedByName END");
			return predefinedMasterList.get(0);
		}else {
			logger.info("PredefinedServiceImpl getPredefinedByName END ELSE");
			return null;
		}
	}
	
	
	@Override
	public PredefinedMaster getPredefinedWithSaveByName(String name,String entityType) throws Exception {
		logger.info("PredefinedServiceImpl getPredefinedWithSaveByName START ");
		
		PredefinedMaster predefinedMaster = getPredefinedByName(name,entityType);
		
		if(predefinedMaster == null) {
		
			PredefinedMaster predefinedMaster2 = new PredefinedMaster();
			predefinedMaster2.setEntityType(entityType);
			predefinedMaster2.setName(name);
			predefinedMaster = savePredefinedMaster(predefinedMaster2);
			
			predefinedMaster = getPredefinedByName(name,entityType);
		}
		
		logger.info("PredefinedServiceImpl getPredefinedWithSaveByName END");
		return predefinedMaster;
	}

	@Override
	public PredefinedMaster getPredefinedWithSaveByNameChild(String name,String parentName,String entityType) throws Exception {
		logger.info("PredefinedServiceImpl getPredefinedWithSaveByNameChild START ");
		
		PredefinedMaster predefinedMaster = getPredefinedByName(name,entityType);
		
		if(predefinedMaster == null) {
		
			PredefinedMaster predefinedMaster2 = new PredefinedMaster();
			predefinedMaster2.setEntityType(entityType);
			predefinedMaster2.setName(name);
			predefinedMaster2.setParentPredefinedMaster(getPredefinedByName(parentName,"DEPARTMENT"));
			predefinedMaster = savePredefinedMaster(predefinedMaster2);
			
			predefinedMaster = getPredefinedByName(name,entityType);
		}
		
		logger.info("PredefinedServiceImpl getPredefinedWithSaveByNameChild END");
		return predefinedMaster;
	}


	@Override
	public Integer getDesignationEntityListLength(PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedServiceImpl->>> getDesignationEntityListLength Start" );
		Integer countOfRows = 0;
		String query="Select count(predefinedId) from PredefinedMaster pm where pm.isDelete='N' ";
		countOfRows = genericDao.countCustomList(query).intValue();
		logger.info("PredefinedServiceImpl->>> getDesignationEntityListLength End" );
		return countOfRows;
	}

	@Override
	public List<PredefinedMaster> getFilteredDesignationNames(PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedServiceImpl getFilteredDesignationNames START ");
		List<PredefinedMaster> filteredPredefinedMasterList =new ArrayList<PredefinedMaster>();
		if(predefinedMaster.getName().length()  <= 5 )
		{
			return filteredPredefinedMasterList;
		}
		filteredPredefinedMasterList = genericDao.findCustomList("from PredefinedMaster where name like '"+ predefinedMaster.getName() +"%' "
				+ "and entity_type = '"+ predefinedMaster.getEntityType() +"' order by name");
		logger.info("PredefinedServiceImpl getFilteredDesignationNames END");
		return filteredPredefinedMasterList;
	}
	
	@Override
	public PredefinedMaster getPredefinedByNameAndSubname(String name, String subName, String entityType)
			throws Exception {
		logger.info("PredefinedServiceImpl getPredefinedByNameAndSubname START ");
		List<PredefinedMaster> predefinedMasterList=new ArrayList<PredefinedMaster>();
		predefinedMasterList = genericDao.findCustomList("from PredefinedMaster" + " where isDelete='N' and entityType = '"+entityType+"' and name = '"+name+"' and subName= '"+subName+"'  ");
		if(predefinedMasterList != null && predefinedMasterList.size() > 0) {
			logger.info("PredefinedServiceImpl getPredefinedByNameAndSubname END");
			return predefinedMasterList.get(0);
		}else {
			logger.info("PredefinedServiceImpl getPredefinedByNameAndSubname END ELSE");
			return null;
		}
	}

	@Override
	public void savePredefinedWithFile(PredefinedMaster predefinedMaster) throws Exception {

		if(predefinedMaster.getDocumentSelectedPath()!=null && !predefinedMaster.getDocumentSelectedPath().equalsIgnoreCase("")) {
			String fileSelectPath = predefinedMaster.getDocumentSelectedPath() + "/"
					+ predefinedMaster.getNewFileName();
				File file = new File(fileSelectPath);
			
				logger.info("PredefinedServiceImpl->>> savePredefinedWithFile file upload Start -->");
				AmazonS3DTO amazonS3DTO = new AmazonS3DTO();
			//	String fileName = file.getOriginalFilename();
				String key = predefinedMaster.getEntityType()+"/"+predefinedMaster.getName()+ UUID.randomUUID().toString()+"-"+predefinedMaster.getNewFileName();		
				amazonS3DTO = awsService.uploadDataToAmazonS3WithFile(file, key);
				
				predefinedMaster.setFileKey(amazonS3DTO.getS3Key());
				predefinedMaster.setFilePath(amazonS3DTO.getFilePath());
				predefinedMaster.setFilePath(amazonS3DTO.getFilePath());
		}
	
		
			logger.info("PredefinedServiceImpl->>> savePredefinedWithFile file upload End--> ");				
	
		
		savePredefinedMaster(predefinedMaster);
			
	}

	@Override
	public List<PredefinedMaster> getPredefinedListByEntityTypeAndType(PredefinedMaster predefinedMaster)
			throws Exception {
		logger.info("PredefinedServiceImpl->>> getPredefinedListByEntityTypeAndType Data type-->"+predefinedMaster.getType());
		return genericDao.findCustomList("from PredefinedMaster where isDelete='N' and entityType='"+predefinedMaster.getEntityType()+"' and type='"+predefinedMaster.getType()+"' ");
	}

	@Override
	public File getDocumentPath(MultipartFile multipartFile) throws Exception {
		logger.info("PredefinedServiceImpl->>> getDocumentPath start -->" );
		File file = null;
		try {
			file = dataUtil.convertMultipartToFile(multipartFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("PredefinedServiceImpl->>> getDocumentPath end -->" );
		return file;
	}

	@Override
	public List<PredefinedMaster> getPredefinedListByTypeOnlyName(PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedServiceImpl->>> getPredefinedListByTypeOnlyName Data type-->"+predefinedMaster.getEntityType());
		
		String query = "select new PredefinedMaster(name) from PredefinedMaster where isDelete='N' and entityType='"+predefinedMaster.getEntityType()+"' ";
		
		if(predefinedMaster.getSortOrder()!=null && !predefinedMaster.getSortOrder().isEmpty()) {
			query+=" order by displayOrder "+predefinedMaster.getSortOrder();
		}
		
		
		return genericDao.findCustomList(query);
	}

	@Override
	public List<PredefinedMaster> getPredefinedListByTypeSearch(PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedServiceImpl->>> getPredefinedListByType Data type-->"+predefinedMaster.getEntityType());
		
		String query = "from PredefinedMaster where isDelete='N' and entityType='"+predefinedMaster.getEntityType()+"' and name like '"+predefinedMaster.getName()+"%' ";
		
		if(predefinedMaster.getSortOrder()!=null && !predefinedMaster.getSortOrder().isEmpty()) {
			query+=" order by displayOrder "+predefinedMaster.getSortOrder();
		}
		
		
		return genericDao.findCustomList(query);
	}
	
	
	
}

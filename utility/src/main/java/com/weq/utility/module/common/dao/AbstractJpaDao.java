package com.weq.utility.module.common.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.annotations.QueryHints;

@Transactional
public abstract class AbstractJpaDao<T> implements GenericDao<T> {

	private Class<T> clazz;

	@PersistenceContext
	EntityManager entityManager;

	public void setClazz(Class<T> clazzToSet) {
		this.clazz = clazzToSet;
	}

	public T findOne(long id) {
		return entityManager.find(clazz, id);
	}

	public List<T> findAll() {
		return entityManager.createQuery("from " + clazz.getName()).getResultList();
	}

	public T save(T entity) {
		entityManager.persist(entity);
		return entity;
	}

	public T update(T entity) {
		return entityManager.merge(entity);
	}

	public void delete(T entity) {
		entityManager.remove(entity);
	}

	public void deleteById(long entityId) {
		T entity = findOne(entityId);
		delete(entity);
	}

	public List<T> findCustomList(String query) {
		return entityManager.createQuery(query).getResultList();
	}

	public List<T> findCustomListPaginate(String query, Integer pageNumber, Integer pageSize) {

		Integer start = 0;
		start = pageNumber * pageSize;

		Query queryHQL = entityManager.createQuery(query);
		queryHQL.setFirstResult(start);
		queryHQL.setMaxResults(pageSize);

		return queryHQL.getResultList();
	}

	public Long countCustomList(final String query) {
		return (Long) entityManager.createQuery(query).getSingleResult();

	}

	public T findOneInt(Integer id, Class<T> className) {
		return entityManager.find(className, id);
	}

	public T findOneLong(Long id, Class<T> className) {
		return entityManager.find(className, id);
	}

	@Override
	public Long maxCount(String query) {
		return (Long) entityManager.createQuery(query).getSingleResult();
	}

	public List<T> findCustomNativeList(String query) {
		return entityManager.createNativeQuery(query).getResultList();
	}

	public T saveAll(T entity) {
		entityManager.persist(entity);
		return entity;
	}

	public void customUpdate(final String query) {
		entityManager.createQuery(query).executeUpdate();
	}

	public List<?> findCustomListUnique(String query) {
		return entityManager.createQuery(query).getResultList();
	}
	
	public List<T> fetchNextRecords(String query,Integer recordSize) {
		Query queryHQL = entityManager.createQuery(query);
		queryHQL.setMaxResults(recordSize);
		return queryHQL.getResultList();
	}
	
	public String findSingleColumnValue(String query) {
		return (String) entityManager.createQuery(query).getSingleResult();
	}
	
}

package com.weq.utility.module.aws;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AmazonS3DTO {
	
	private String bucketName;
	private String s3Key;
	private String filePath;
	private String fileName;
	private String contentType;

	
	private String fileNameThumbnail;
	private String s3KeyThumbnail;
	private String filePathThumbnail;
	
}

package com.weq.utility.module.aws.service;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.UUID;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.weq.utility.configuration.UtilityProperties;
import com.weq.utility.module.aws.AmazonS3DTO;
import com.weq.utility.module.common.dao.GenericDao;
import com.weq.utility.module.common.util.DataConstants;
import com.weq.utility.module.common.util.DataUtil;

@Service
public class AwsServiceImpl implements AwsService{
	
	private static final Logger logger = Logger.getLogger(AwsServiceImpl.class);
	
	@Autowired
	private GenericDao genericDao;
	
	@Autowired
	private DataUtil dataUtil;
	
	@Autowired
	private UtilityProperties utilityProperties;

	@Override
	public AmazonS3DTO uploadDataToAmazonS3(MultipartFile file, String key) throws Exception {
		AWSCredentials credentials = new BasicAWSCredentials(DataConstants.AmazonSecurityCredentials.ACCESS_KEY_ID,
				DataConstants.AmazonSecurityCredentials.SECRET_ACCESS_KEY);

		AmazonS3 s3client = AmazonS3Client.builder()
			    .withRegion("ap-south-1")
			    .withCredentials(new AWSStaticCredentialsProvider(credentials))
			    .build();
		String bucketName = DataConstants.AmazonSecurityCredentials.BUCKETNAME;
		String s3Key = key;
		String fullPath =" ";

		File src = dataUtil.convertMultipartToFile(file);

		String fileName = src.getName();

		File compressedImageFile = null;
		Iterator<ImageWriter> writers = null;
		if (file.getOriginalFilename() != null
				&& (file.getOriginalFilename().contains(".jpg") || file.getOriginalFilename().contains(".JPG"))) {
			BufferedImage image = ImageIO.read(src);

			compressedImageFile = new File(utilityProperties.getDocumentUpload() + fileName);
			writers = ImageIO.getImageWritersByFormatName("jpg");

			OutputStream os = new FileOutputStream(compressedImageFile);

			if (writers != null) {
				ImageWriter writer = (ImageWriter) writers.next();

				ImageOutputStream ios = ImageIO.createImageOutputStream(os);
				writer.setOutput(ios);

				ImageWriteParam param = writer.getDefaultWriteParam();

				param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
				param.setCompressionQuality(0.5f);
				writer.write(null, new IIOImage(image, null, null), param);

				os.close();
				ios.close();
				writer.dispose();
			}

		} else if (file.getOriginalFilename() != null
				&& (file.getOriginalFilename().contains(".png") || file.getOriginalFilename().contains(".PNG"))) {

			try {
				BufferedImage image = ImageIO.read(src);

				if (file.getOriginalFilename().contains(".png")) {
					fileName = fileName.replace(".png", ".jpg");
				} else if (file.getOriginalFilename().contains(".PNG")) {
					fileName = fileName.replace(".PNG", ".jpg");
				}

				compressedImageFile = new File(utilityProperties.getDocumentUpload() + fileName);

				BufferedImage newBufferedImage = new BufferedImage(image.getWidth(), image.getHeight(),
						BufferedImage.TYPE_INT_RGB);
				newBufferedImage.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);

				ImageIO.write(newBufferedImage, "jpg", compressedImageFile);
				
				
				
				
				
			} catch (Exception e) {
				compressedImageFile = new File(utilityProperties.getDocumentUpload() + fileName);
			}

		} else {
			compressedImageFile = src;
		}

		FileInputStream documentFile = new FileInputStream(compressedImageFile);

		URLConnection connection = compressedImageFile.toURI().toURL().openConnection();
		String mimeType = connection.getContentType();
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentType(mimeType);
		metadata.setContentLength(compressedImageFile.length());

		s3client.putObject(new PutObjectRequest(bucketName, s3Key, documentFile, metadata));
		s3client.setObjectAcl(bucketName, s3Key, CannedAccessControlList.PublicRead);

		fullPath = "https://" + bucketName + ".s3.amazonaws.com/" + s3Key;

		AmazonS3DTO amazonS3DTO = new AmazonS3DTO();
		amazonS3DTO.setBucketName(bucketName);
		amazonS3DTO.setS3Key(s3Key);
		amazonS3DTO.setFilePath(fullPath);
		amazonS3DTO.setFileName(src.getName());

		if (mimeType != null && mimeType.contains("image/")) {
			amazonS3DTO.setContentType("image");
			
			
			
			
			BufferedImage bufferedImage = ImageIO.read(compressedImageFile);
			String thumbnailFileName = utilityProperties.getDocumentUpload() + "thumbnail-"
					+ UUID.randomUUID().toString() + ".jpg";
			File thumbnailFile = null;
					
			if(file.getOriginalFilename().contains(".png") || file.getOriginalFilename().contains(".PNG")) {
				thumbnailFile =new File(thumbnailFileName);
				writers = ImageIO.getImageWritersByFormatName("jpg");

				OutputStream os = new FileOutputStream(thumbnailFile);

				if (writers != null) {
					ImageWriter writer = (ImageWriter) writers.next();

					ImageOutputStream ios = ImageIO.createImageOutputStream(os);
					writer.setOutput(ios);

					ImageWriteParam param = writer.getDefaultWriteParam();

					param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
					param.setCompressionQuality(0.4f);
					writer.write(null, new IIOImage(bufferedImage, null, null), param);

					os.close();
					ios.close();
					writer.dispose();
				}
			}else {
				thumbnailFile = compressedImageFile;
			}
			
			
			s3Key += thumbnailFile.getName();
			documentFile = new FileInputStream(thumbnailFile);
			connection = thumbnailFile.toURI().toURL().openConnection();
			mimeType = connection.getContentType();
			metadata = new ObjectMetadata();
			metadata.setContentType(mimeType);
			metadata.setContentLength(thumbnailFile.length());
			s3client.putObject(new PutObjectRequest(bucketName, s3Key, documentFile, metadata));
			s3client.setObjectAcl(bucketName, s3Key, CannedAccessControlList.PublicRead);
			fullPath = "https://" + bucketName + ".s3.amazonaws.com/" + s3Key;

			amazonS3DTO.setFileNameThumbnail(thumbnailFile.getName());
			amazonS3DTO.setFilePathThumbnail(fullPath);
			amazonS3DTO.setS3KeyThumbnail(s3Key);
			
		}else {
			amazonS3DTO.setContentType("video");
		}		
		
		return amazonS3DTO;
		
	}
	

	@Override
	public AmazonS3DTO uploadDataToAmazonS3WithFile(File file, String key) throws Exception {
		AWSCredentials credentials = new BasicAWSCredentials(DataConstants.AmazonSecurityCredentials.ACCESS_KEY_ID,
				DataConstants.AmazonSecurityCredentials.SECRET_ACCESS_KEY);

		AmazonS3 s3client = AmazonS3Client.builder()
			    .withRegion("ap-south-1")
			    .withCredentials(new AWSStaticCredentialsProvider(credentials))
			    .build();
		String bucketName = DataConstants.AmazonSecurityCredentials.BUCKETNAME;
		String s3Key = key;
		String fullPath =" ";
		
		File src = file;
		
		FileInputStream documentFile=new FileInputStream(src);	
		
		URLConnection connection = src.toURI().toURL().openConnection();
	    String mimeType = connection.getContentType();
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentType(mimeType);
		metadata.setContentLength(src.length());
		s3client.putObject(
				new PutObjectRequest(bucketName, s3Key, documentFile, metadata));
		s3client.setObjectAcl(bucketName, s3Key, CannedAccessControlList.PublicRead);
						
		fullPath = "https://" + bucketName + ".s3.amazonaws.com/" + s3Key;
					
		AmazonS3DTO amazonS3DTO = new AmazonS3DTO();
		amazonS3DTO.setBucketName(bucketName);
		amazonS3DTO.setS3Key(s3Key);
		amazonS3DTO.setFilePath(fullPath);
		amazonS3DTO.setFileName(src.getName());

		
		return amazonS3DTO;
	}
	
	@Override
	public AmazonS3DTO uploadDataToOurServer(MultipartFile file) throws IOException {

		AmazonS3DTO amazonS3DTO = new AmazonS3DTO();

		File src = dataUtil.convertMultipartToFile(file);

		File fileNew = new File(src.getPath()); 

		String key = UUID.randomUUID().toString()+"-"+file.getOriginalFilename();
		String newFileFullPath = utilityProperties.getUploadedContentFullPath()+key;
		String newFilePath = utilityProperties.getUploadedContentPath()+key;

		
		
        // renaming the file and moving it to a new location 
        if(fileNew.renameTo 
           (new File(newFileFullPath))) 
        { 
            // if file copied successfully then delete the original file 
        	fileNew.delete(); 
            System.out.println("File moved successfully"); 
        } 
        else
        { 
            System.out.println("Failed to move the file"); 
        } 
		
		amazonS3DTO.setFilePath(newFilePath);
		amazonS3DTO.setFilePathThumbnail(newFilePath);
		
		if( src.getName().contains(".jpg") || src.getName().contains(".JPG") || 
				src.getName().contains(".jpeg") || src.getName().contains(".JPEG") ||
				src.getName().contains(".png") || src.getName().contains(".PNG") || 
				src.getName().contains(".gif") || src.getName().contains(".GIF")) {
			
			amazonS3DTO.setContentType("image");

		} else {

			amazonS3DTO.setContentType("video");
			
		}

		return amazonS3DTO;
	}

}

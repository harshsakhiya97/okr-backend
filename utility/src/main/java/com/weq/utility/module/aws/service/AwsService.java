package com.weq.utility.module.aws.service;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.weq.utility.module.aws.AmazonS3DTO;

public interface AwsService {

	AmazonS3DTO uploadDataToAmazonS3(MultipartFile file,String key) throws Exception;
	
	AmazonS3DTO uploadDataToAmazonS3WithFile(File file,String key) throws Exception;

	AmazonS3DTO uploadDataToOurServer(MultipartFile file) throws IOException;
	
}

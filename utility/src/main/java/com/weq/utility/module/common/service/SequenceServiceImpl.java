package com.weq.utility.module.common.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.weq.utility.module.common.dao.SequenceDao;
import com.weq.utility.module.common.model.SequenceMaster;

@Service("salesSequenceService")
public class SequenceServiceImpl implements SequenceService{
	private static final Logger logger = Logger.getLogger(SequenceServiceImpl.class);
	
	@Autowired
	private SequenceDao sequenceDao;

	@Override
	public SequenceMaster getSequence(String type) throws Exception {
		logger.info("SequenceServiceImpl getSequence");
		return sequenceDao.getSequence(type);
	}

	@Override
	public void updateSequence(String type) throws Exception{
		logger.info("SequenceServiceImpl updateSequence");
		SequenceMaster sequenceMaster=sequenceDao.getSequence(type);
		
		if(sequenceMaster!=null && sequenceMaster.getSequenceId()!=null && sequenceMaster.getSequenceId()!=0) {
			Integer nextVal=sequenceMaster.getNextVal()+1;
			sequenceMaster.setNextVal(nextVal);
			sequenceDao.save(sequenceMaster);
		}
		logger.info("SequenceServiceImpl updateSequence end");
	}
	
	@Override
	public void updateDecrementSequence(String type) throws Exception{
		logger.info("SequenceServiceImpl updateDecrementSequence");
		SequenceMaster sequenceMaster = sequenceDao.getSequence(type);
		
		if(sequenceMaster!=null && sequenceMaster.getSequenceId()!=null && sequenceMaster.getSequenceId()!=0) {
			Integer nextVal = sequenceMaster.getNextVal() - 1;
			sequenceMaster.setNextVal(nextVal);
			sequenceDao.save(sequenceMaster);
		}
		logger.info("SequenceServiceImpl updateDecrementSequence end");
	}
}
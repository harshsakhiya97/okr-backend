package com.weq.utility.module.common.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class PredefinedMaster extends TimestampEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer predefinedId;
	
//	@Size(max = 100)
	private String name;
	
//	@Size(max = 100)
	private String subName;
	
	@Size(max = 50)
	private String entityType;
	
	@Size(max = 50)
	private String type;
	
	@Size(max = 100)
	private String field1;
	
	@Size(max = 100)
	private String field2;
	
	@Size(max = 100)
	private String field3;
	
	@Size(max = 100)
	private String field4;
	
	@Size(max = 100)
	private String field5;
	
	@Size(max = 100)
	private String field6;
	
	@Size(max = 100)
	private String code;
	
	@Size(max = 100)
	private String fileName;
	
	@Size(max = 300)
	private String filePath;
	
	@Size(max = 200)
	private String fileKey;
	
	@Column(name="display_order", columnDefinition="int(10) default '0'")
	private Integer displayOrder;
	
	@Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;
	
	@ManyToOne(targetEntity = PredefinedMaster.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentPredefinedId", nullable = true)
	@JsonBackReference("parentPredefinedMaster")
	private PredefinedMaster parentPredefinedMaster;
	
	@Column(name = "parentPredefinedId", insertable = false, updatable = false)
	private Integer parentPredefinedId;
	
	@OneToMany(fetch = FetchType.EAGER,mappedBy = "parentPredefinedMaster")
	@Where(clause="is_delete='N'")
	private List<PredefinedMaster> childPredefinedMasterList = new ArrayList<PredefinedMaster>();
	
	@Transient
	private String documentSelectedPath;
	
	@Transient
	private String documentPath;

	@Transient
	private String newFileName;
	
	@Transient
	private String nameAndSubname;
	
	@Transient
	private String sortOrder;
	
	@Transient
	private String parentName;
	
	@Transient
	private Integer viewCount;
	
	@Transient
	private Integer downloadCount;
	
	public PredefinedMaster() {
		
	}
	
	public PredefinedMaster(String name) {;
	this.name = name;
}
	
	public PredefinedMaster(Integer predefinedId, String name) {;
		this.predefinedId = predefinedId;
		this.name = name;
	}
	
	public PredefinedMaster(String field1, Long field2) {;
		this.field1 = field1;
		if(field2!=null) {
			this.field2 = String.valueOf(field2);
		}
		
	}
	
	
	public PredefinedMaster(Integer predefinedId, String name, String subName, String field1, String field2) {
		this.predefinedId = predefinedId;
		this.name = name;
		this.subName = subName;
		this.field1 =field1;
		this.field2 =field2;
	}
	
	public PredefinedMaster(Integer predefinedId,  String name,String subName, String field1, String field2,
			 String field3,  String field4) {
		this.predefinedId = predefinedId;
		this.name = name;
		this.subName = subName;
		this.field1 = field1;
		this.field2 = field2;
		this.field3 = field3;
		this.field4 = field4;
	}

	public PredefinedMaster(Integer predefinedId,  String name,String subName,
			 String entityType,  String field1, String field2,
			 String field3,  String field4, String field5,
			 String field6) {
		this.predefinedId = predefinedId;
		this.name = name;
		this.subName = subName;
		this.entityType = entityType;
		this.field1 = field1;
		this.field2 = field2;
		this.field3 = field3;
		this.field4 = field4;
		this.field5 = field5;
		this.field6 = field6;
	}
	
	
	public String getNameAndSubname() {
		
		if(subName!=null && !subName.isEmpty()) {
			nameAndSubname = name + " ("+subName+")";
		}else {
			nameAndSubname = name;
		}
		
		return nameAndSubname;
	}
	

}

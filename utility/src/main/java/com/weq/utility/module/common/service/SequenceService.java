package com.weq.utility.module.common.service;

import com.weq.utility.module.common.model.SequenceMaster;

public interface SequenceService {

	SequenceMaster getSequence(String type) throws Exception;

	void updateSequence(String type) throws Exception;

	void updateDecrementSequence(String type) throws Exception;
}

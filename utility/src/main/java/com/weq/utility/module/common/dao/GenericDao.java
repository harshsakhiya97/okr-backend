package com.weq.utility.module.common.dao;

import java.util.List;

public interface GenericDao<T> {
	  T findOne(final long id);
		 
	   List<T> findAll();
	   
	   List<T> findCustomList(final String query);
	   
	   List<T> findCustomListPaginate(final String query,Integer pageNumber,Integer pageSize);
	   
	   Long countCustomList(final String query);
	 
	   T save(final T entity);
	   
	   T saveAll(final T entity);
	 
	   T update(final T entity);
	 
	   void delete(final T entity);
	 
	   void deleteById(final long entityId);

	   void setClazz(Class<T> clazz);
	   
	   T findOneInt(final Integer id,Class<T> className);
	   
	   T findOneLong(final Long id,Class<T> className);
	   
	   Long maxCount(final String query);
	   
	   List<T> findCustomNativeList(final String query);
	   
	   void customUpdate(final String query);
	   
	   List<?> findCustomListUnique(String lookupQuery);
	   
	   List<T> fetchNextRecords(final String query,Integer recordSize);
	   
	   String findSingleColumnValue(final String query);


}

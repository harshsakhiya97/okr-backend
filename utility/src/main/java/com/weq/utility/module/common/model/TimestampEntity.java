package com.weq.utility.module.common.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

//@Entity
@MappedSuperclass
@Data
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public abstract class TimestampEntity {

	@Column
	@CreationTimestamp
	private Date createdDate;

	@Column
	@UpdateTimestamp
	private Date modifiedDate;
	
}

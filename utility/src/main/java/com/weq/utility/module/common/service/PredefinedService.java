package com.weq.utility.module.common.service;

import java.io.File;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.weq.utility.module.common.model.PredefinedMaster;

public interface PredefinedService {
	
	PredefinedMaster savePredefinedMaster(PredefinedMaster predefinedMaster) throws Exception;
	
	void deletePredefinedMaster(Integer predefinedId) throws Exception;
	
	List<PredefinedMaster> getPredefinedListByType(PredefinedMaster predefinedMaster) throws Exception;

	PredefinedMaster getPredefinedByCode(String locationCode) throws Exception;

	PredefinedMaster getPredefinedByName(String name, String entityType) throws Exception;
	
	Integer getDesignationEntityListLength(PredefinedMaster predefinedMaster) throws Exception;
	
	List<PredefinedMaster> getFilteredDesignationNames(PredefinedMaster predefinedMaster) throws Exception;

	PredefinedMaster getPredefinedByNameAndSubname(String value, String industryData, String entityName) throws Exception;

	void savePredefinedWithFile(PredefinedMaster predefinedMaster) throws Exception;

	List<PredefinedMaster> getPredefinedListByEntityTypeAndType(PredefinedMaster predefinedMaster) throws Exception;

	File getDocumentPath(MultipartFile multipartFile) throws Exception;

	List<PredefinedMaster> getPredefinedListByTypeOnlyName(PredefinedMaster predefinedMaster) throws Exception;

	List<PredefinedMaster> getPredefinedListByTypeSearch(PredefinedMaster predefinedMaster) throws Exception;

	PredefinedMaster getPredefinedWithSaveByName(String name, String entityType) throws Exception;

	PredefinedMaster getPredefinedWithSaveByNameChild(String name, String parentName, String entityType)
			throws Exception;

}

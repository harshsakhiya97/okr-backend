package com.weq.utility.module.common.dao;

import java.io.Serializable;
import java.util.List;

public interface IGenericDao<T> {
	 
	   T findOne(final long id);
	 
	   List<T> findAll();
	   
	   List<T> findCustomList(final String query);
	 
	   T save(final T entity);
	 
	   T update(final T entity);
	 
	   void delete(final T entity);
	 
	   void deleteById(final long entityId);
	   
	   List<T> getLookupList(String queryStr);
	   
}


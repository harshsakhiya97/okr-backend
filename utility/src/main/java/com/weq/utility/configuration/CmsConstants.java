package com.weq.utility.configuration;

public class CmsConstants {
	
	public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5 * 60 * 60000;
	public static final String SIGNING_KEY = "ironoid@myapp";
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String AUTHORITIES_KEY = "scopes";
	public static final String[] IP_HEADER = { 
			"X-Real-IP",
		    "X-Forwarded-For",
		    "Proxy-Client-IP",
		    "WL-Proxy-Client-IP",
		    "HTTP_X_FORWARDED_FOR",
		    "HTTP_X_FORWARDED",
		    "HTTP_X_CLUSTER_CLIENT_IP",
		    "HTTP_CLIENT_IP",
		    "HTTP_FORWARDED_FOR",
		    "HTTP_FORWARDED",
		    "HTTP_VIA",
		    "REMOTE_ADDR" };

}

package com.weq.utility.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;


@Configuration
@Data
public class UtilityProperties {
	

	@Value("${master.documentUpload}")
	public String documentUpload;
	

	@Value("${master.uploadedContentFullPath}")
	public String uploadedContentFullPath;

	@Value("${master.uploadedContentPath}")
	public String uploadedContentPath;

	@Value("${report.fullPathFolder}")
	public String reportFullPathFolder;

	@Value("${report.folder}")
	public String reportFolder;
	
	@Value("${fcm.serverKey}")
	public String fcmServerKey;

	@Value("${max.okrDownloadSize}")
	public Integer maxOkrDownloadSize;
	
	@Value("${spring.mail.username}")
	public String fromEmail;
	
	@Value("${company.logo}")
	public String companyLogo;
	
}

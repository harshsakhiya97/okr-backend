package com.weq.communication.service.template;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.weq.communication.dao.TemplateDao;
import com.weq.communication.model.template.TemplateMaster;

@Service
public class TemplateServiceImpl implements TemplateService{
	private static final Logger logger = Logger.getLogger(TemplateServiceImpl.class);

	@Autowired
	private TemplateDao templateDao;
	
	@Override
	public List<TemplateMaster> getTemplateListByType(String type) throws Exception {
		logger.info("TemplateServiceImpl->>> getTemplateListByType  start-->");
		 List<TemplateMaster> templateList=new ArrayList<TemplateMaster>();
		 templateList=templateDao.getTemplateListByType(type);
		logger.info("TemplateServiceImpl->>> getTemplateListByType  end-->");
		return templateList;
	}

	@Override
	public TemplateMaster saveTemplate(TemplateMaster templateMaster) throws Exception {
		logger.info("TemplateServiceImpl->>> saveTemplate  start-->");
		templateMaster=templateDao.save(templateMaster);	
		logger.info("TemplateServiceImpl->>> saveTemplate  end-->");
		return templateMaster;
	}
	
}

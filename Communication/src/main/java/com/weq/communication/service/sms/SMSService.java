package com.weq.communication.service.sms;

import com.weq.communication.model.sms.SMS;

public interface SMSService {
	void sendSMS(SMS sms) throws Exception;
}

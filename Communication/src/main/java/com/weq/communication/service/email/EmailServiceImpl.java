package com.weq.communication.service.email;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import com.weq.communication.model.email.Mail;

@Service
public class EmailServiceImpl implements EmailService{
	private static final Logger logger = Logger.getLogger(EmailServiceImpl.class);
	
	 @Autowired
	 private JavaMailSender emailSender;

	 @Autowired
	 private SpringTemplateEngine templateEngine;
	
	
	/*@Override
	public void sendTemplateEmail(Mail mail,List<MultipartFile> files) throws Exception {
		logger.info("EmailServiceImpl->>> sendTemplateEmail id start-->");
		MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
		
        Context context = new Context();;
        context.setVariables(mail.getModel());
        
        String html = templateEngine.process(mail.getTemplateCode(), context);

        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());
        
        if(files!=null && !files.isEmpty()) {
        	for(MultipartFile mFile:files) {
        		helper.addAttachment(mFile.getOriginalFilename(), mFile);	
        	}
        	
        }

        emailSender.send(message);
        logger.info("EmailServiceImpl->>> sendTemplateEmail id end-->");
	}
*/
	@Override
	public void sendDirectEmail(Mail mail) throws Exception {
		logger.info("EmailServiceImpl->>> sendDirectEmail id start-->");
		MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
		
        Context context = new Context();
        context.setVariables(mail.getModel());
     
        helper.setTo(mail.getTo());
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());
        
        if(mail.getTemplateCode()!=null && !mail.getTemplateCode().equalsIgnoreCase("")) {
        	  String html = templateEngine.process(mail.getTemplateCode(), context);
        	  helper.setText(html, true);
        }else {
        	 helper.setText(mail.getContent(), true);
        }
        
            
        if(mail.getMfileAttachments()!=null && !mail.getMfileAttachments().isEmpty()) {
        	for(MultipartFile mFile:mail.getMfileAttachments()) {
        		helper.addAttachment(mFile.getOriginalFilename(), mFile);	
        	}
        	
        }
        
        if(mail.getFileAttachments()!=null && !mail.getFileAttachments().isEmpty()) {
        	for(File file:mail.getFileAttachments()) {
        		helper.addAttachment(file.getName(), file);	
        	}
        	
        }
        

        emailSender.send(message);
        logger.info("EmailServiceImpl->>> sendDirectEmail id end-->");
		
	}
	
	/*@Override
	public void sendDirectEmailWithFile(Mail mail,List<File> files) throws Exception {
		logger.info("EmailServiceImpl->>> sendDirectEmailWithFile id start-->");
		MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
		
        Context context = new Context();;
        context.setVariables(mail.getModel());
     
        helper.setTo(mail.getTo());
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());
        
        if(mail.getTemplateCode()!=null && !mail.getTemplateCode().equalsIgnoreCase("")) {
      	  String html = templateEngine.process(mail.getTemplateCode(), context);
      	  helper.setText(html, true);
      }else {
      	 helper.setText(mail.getContent(), true);
      }
      
        
        if(files!=null && !files.isEmpty()) {
        	for(File file:files) {
        		helper.addAttachment(file.getName(), file);	
        	}
        	
        }

        emailSender.send(message);
        logger.info("EmailServiceImpl->>> sendDirectEmailWithFile id end-->");
		
	}*/

}

package com.weq.communication.service.template;

import java.util.List;

import com.weq.communication.model.template.TemplateMaster;

public interface TemplateService {

	List<TemplateMaster> getTemplateListByType(String type) throws Exception;
	TemplateMaster saveTemplate(TemplateMaster templateMaster) throws Exception;
	
}

package com.weq.communication.service.sms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.weq.communication.model.sms.SMS;
import com.weq.communication.service.email.EmailServiceImpl;

@Service
public class SMSServiceImpl implements SMSService {
	private static final Logger logger = Logger.getLogger(SMSServiceImpl.class);
	@Override
	public void sendSMS(SMS sms) throws Exception {
		logger.info("SMSServiceImpl->>> sendSMS start-->");
		String smsURL= sms.getSmsURL();
		/*String apiKey = "apikey=" + URLEncoder.encode(sms.getApiKey(), "UTF-8");
		String sender = "&sender=" + URLEncoder.encode(sms.getSenderCode(), "UTF-8");
		String message = "&message=" + URLEncoder.encode(sms.getMessage(), "UTF-8");
		String numbers = "&numbers=" + URLEncoder.encode(sms.getSmsTo(), "UTF-8");*/

		URL url = new URL(smsURL);
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);
		
		// Get the response
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line;
		String sResult="";
		while ((line = rd.readLine()) != null) {
			sResult=sResult+line+" ";
		}
		rd.close();
		
		logger.info("SMSServiceImpl->>> sendSMS end-->");
	}


}

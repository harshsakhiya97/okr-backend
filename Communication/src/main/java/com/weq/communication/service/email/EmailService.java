package com.weq.communication.service.email;

import java.io.File;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.weq.communication.model.email.Mail;

public interface EmailService {
	
/*	void sendTemplateEmail(Mail mail,List<MultipartFile> files) throws Exception;
	void sendTemplateEmailWithFile(Mail mail,List<File> files) throws Exception;*/
	void sendDirectEmail(Mail mail) throws Exception;
//	void sendDirectEmailWithFile(Mail mail,List<File> files) throws Exception;
	
}

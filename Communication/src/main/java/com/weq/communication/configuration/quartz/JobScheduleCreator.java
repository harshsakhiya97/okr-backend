package com.weq.communication.configuration.quartz;

import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;
/*import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;*/
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.stereotype.Component;


//@Component
public class JobScheduleCreator {
	//private static final Logger logger = Logger.getLogger(JobScheduleCreator.class);
    /**
     * Create Quartz Job.
     *
     * @param jobClass  Class whose executeInternal() method needs to be called.
     * @param isDurable Job needs to be persisted even after completion. if true, job will be persisted, not otherwise.
     * @param context   Spring application context.
     * @param jobName   Job name.
     * @param jobGroup  Job group.
     * @return JobDetail object
     */
   /* public JobDetail createJob(Class<? extends QuartzJobBean> jobClass, boolean isDurable,
                               ApplicationContext context, SchedulerJobInfo schedulerJobInfo) throws Exception{
    	logger.info("JobScheduleCreator->>> createJob start-->");
        JobDetailFactoryBean factoryBean = new JobDetailFactoryBean();
        factoryBean.setJobClass(jobClass);
        factoryBean.setDurability(isDurable);
        factoryBean.setApplicationContext(context);
        factoryBean.setName(schedulerJobInfo.getJobName());
        factoryBean.setGroup(schedulerJobInfo.getJobGroup());

        // set job data map
        JobDataMap jobDataMap = new JobDataMap();
        
        if(schedulerJobInfo.getJobDataMap()!=null && schedulerJobInfo.getJobDataMap().size() > 0 ) {
        	jobDataMap.putAll(schedulerJobInfo.getJobDataMap());
        }        
        
        jobDataMap.put(schedulerJobInfo.getJobName() + schedulerJobInfo.getJobGroup(), jobClass.getName());
        factoryBean.setJobDataMap(jobDataMap);

        factoryBean.afterPropertiesSet();
        logger.info("JobScheduleCreator->>> createJob end-->");
        return factoryBean.getObject();
    }*/

    /**
     * Create cron trigger.
     *
     * @param triggerName        Trigger name.
     * @param startTime          Trigger start time.
     * @param cronExpression     Cron expression.
     * @param misFireInstruction Misfire instruction (what to do in case of misfire happens).
     * @return {@link CronTrigger}
     */
	/* public CronTrigger createCronTrigger(String triggerName, Date startTime, String cronExpression, int misFireInstruction,
    		String timeZone) throws Exception {
    	 logger.info("JobScheduleCreator->>> createCronTrigger start-->");
        CronTriggerFactoryBean factoryBean = new CronTriggerFactoryBean();
        factoryBean.setName(triggerName);
        factoryBean.setStartTime(startTime);
        factoryBean.setCronExpression(cronExpression);
        factoryBean.setMisfireInstruction(misFireInstruction);
        
        //custom time zone
        if(timeZone!=null && !timeZone.equalsIgnoreCase("")) {
        	 factoryBean.setTimeZone(TimeZone.getTimeZone(timeZone));
        }
        
       
        
      //  try {
            factoryBean.afterPropertiesSet();
        } catch (ParseException e) {
          //  log.error(e.getMessage(), e);
        }
	
        logger.info("JobScheduleCreator->>> createCronTrigger end-->");
        return factoryBean.getObject();
    }*/

    /**
     * Create simple trigger.
     *
     * @param triggerName        Trigger name.
     * @param startTime          Trigger start time.
     * @param repeatTime         Job repeat period mills
     * @param misFireInstruction Misfire instruction (what to do in case of misfire happens).
     * @return {@link SimpleTrigger}
     */
    /*public SimpleTrigger createSimpleTrigger(String triggerName, Date startTime, Long repeatTime, int misFireInstruction,
    		String timeZone) throws Exception{
    	logger.info("JobScheduleCreator->>> createSimpleTrigger start-->");
        SimpleTriggerFactoryBean factoryBean = new SimpleTriggerFactoryBean();
        factoryBean.setName(triggerName);
        //factoryBean.setStartTime(startTime);
        factoryBean.setStartTime(getDateByTimeZone(startTime,timeZone));
        factoryBean.setRepeatInterval(repeatTime);
        factoryBean.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);
        factoryBean.setMisfireInstruction(misFireInstruction);
        factoryBean.afterPropertiesSet();
        logger.info("JobScheduleCreator->>> createSimpleTrigger end-->");
        return factoryBean.getObject();
    }*/
    
	/*  public Trigger buildOnceJobTrigger(JobDetail jobDetail, SchedulerJobInfo jobInfo) {
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName(), jobDetail.getKey().getName()+"-triggers")
                .withDescription(jobDetail.getDescription())
                .startAt(jobInfo.getStartDate())
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
                .build();
    }*/
    
   /* public Date getDateByTimeZone(Date startTime,String timeZone) throws Exception{
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(startTime);
    	cal.setTimeZone(TimeZone.getTimeZone(timeZone));
    	return cal.getTime();
      
    }*/
    
}

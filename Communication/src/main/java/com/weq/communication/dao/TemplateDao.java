package com.weq.communication.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.weq.communication.model.template.TemplateMaster;

@Repository
public interface TemplateDao extends JpaRepository<TemplateMaster, Integer> {

	@Query(value="Select t from TemplateMaster t where t.templateType=:templateType and t.isDelete!='Y' ")
	List<TemplateMaster> getTemplateListByType(@Param("templateType") String templateType);

}

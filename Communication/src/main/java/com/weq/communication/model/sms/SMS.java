package com.weq.communication.model.sms;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class SMS {
	private String smsTo;
	private String smsFrom;
	private String message;
	private String templateCode;
	private String apiKey;
	private String senderCode;
	private String username;
	private String password;
	private String smsType;
	private String source;
	private String smsURL;
	
	@JsonFormat(timezone="GMT")
	private Date dateTime;
	
	private String timeZone;
	
	@JsonIgnore
	private Map<String, Object> model = new HashMap<>();
	
}

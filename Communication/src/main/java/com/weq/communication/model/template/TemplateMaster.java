package com.weq.communication.model.template;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
public class TemplateMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer templateMasterId;
	
	@Size(max = 100)
	private String templateName;
	
	@Size(max = 20)
	private String templateType;
	
	@Size(max = 2000)
	private String templateData;
	
	@Column(name="is_delete", columnDefinition="varchar(10) default 'N' ")
	private String isDelete;
	
}

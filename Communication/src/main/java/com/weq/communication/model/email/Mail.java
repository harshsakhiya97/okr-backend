package com.weq.communication.model.email;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class Mail {
	private String from;
	private String to;
	private String subject;
	private String content;
	private String templateCode;
	private String sendingType;
	
	@JsonFormat(timezone="GMT")
	private Date dateTime;
	
	private String timeZone;
	
	@JsonIgnore
	private Map<String, Object> model = new HashMap<>();
	
	@JsonIgnore
	private List<MultipartFile> mfileAttachments;
	
	@JsonIgnore
	private List<File> fileAttachments;
	
	
	public Mail() {
	}

	public Mail(String from, String to, String subject, String content, String templateCode) {
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.content = content;
		this.templateCode = templateCode;
	}
}
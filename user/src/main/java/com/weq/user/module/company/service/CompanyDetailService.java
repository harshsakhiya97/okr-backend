package com.weq.user.module.company.service;

import com.weq.user.module.company.model.CompanyDetailMaster;

public interface CompanyDetailService {

	void saveCompanyDetail(CompanyDetailMaster companyDetailMaster) throws Exception;

	CompanyDetailMaster getCompanyDetail(CompanyDetailMaster companyDetailMaster) throws Exception;

}

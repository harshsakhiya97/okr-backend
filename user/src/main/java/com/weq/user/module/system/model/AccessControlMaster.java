package com.weq.user.module.system.model;

import java.util.Iterator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.weq.user.module.user.model.Role;
import com.weq.user.module.user.model.User;

import lombok.Data;

@Entity
@Data
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class AccessControlMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer accessControlId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "moduleId")
	private ModuleMaster moduleMaster;

	private boolean createAccess;

	private boolean readAccess;

	private boolean updateAccess;

	private boolean deleteAccess;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "roleId")
	@JsonIgnore
	private Role role;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "userId")
	@JsonIgnore
	private User user;

	@Size(max = 100)
	private String staticIpAddress;

	@Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;

	@Transient
	private Integer pageNumber;

	@Transient
	private Integer pageSize;

	@Transient
	private Role roleTemp;
	
	@Transient
	private Integer employeeId;

	public Role getRoleTemp() {

		if (roleTemp == null && role != null) {
			Role roleNew = new Role();
			roleNew.setId(role.getId());
			roleNew.setName(role.getName());
			roleNew.setDescription(role.getDescription());
			roleTemp = roleNew;
		}
		return roleTemp;
	}
	
	public String getModuleName() {
		if(moduleMaster!=null) {
			return moduleMaster.getModuleName();
		}else {
			return "";
		}
	}
	
	public String getRoleName() {
		if(role!=null) {
			return role.getName();
		}else if(user!=null && user.getRoles()!=null && !user.getRoles().isEmpty()) {
			String roleName="";
			Iterator<Role> value = user.getRoles().iterator();
			while (value.hasNext()) 
			{
				roleName=roleName+value.next().getName()+",";
			}
			if(roleName!=null && !roleName.equalsIgnoreCase("")) {
				roleName=roleName.substring(0, roleName.length()-1);
			}
				
			return roleName;
		}else {
			return "";
		}
	}
	
	
	/*public EmployeeMaster getTempEmployee() {
		
		if(tempEmployee == null) {
			EmployeeMaster employeeMaster = new EmployeeMaster();
			employeeMaster.setEmployeeId(tempEmployee.getEmployeeId());
			tempEmployee = employeeMaster;
		}
		
		return tempEmployee;
		
	}*/
	
	
}

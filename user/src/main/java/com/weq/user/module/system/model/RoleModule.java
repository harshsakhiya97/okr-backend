package com.weq.user.module.system.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.weq.user.module.user.model.Role;

import lombok.Data;

@Entity
@Data
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class RoleModule {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer roleModuleId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "moduleId")
	private ModuleMaster moduleMaster;

	private boolean createAccess;

	private boolean readAccess;

	private boolean updateAccess;

	private boolean deleteAccess;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "roleId")
	@JsonIgnore
	private Role role;
	
	@Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;

}

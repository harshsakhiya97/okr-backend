package com.weq.user.module.system.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.weq.utility.module.common.model.TimestampEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
public class FilterModuleMaster extends TimestampEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer filterModuleId;
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "moduleId")
    private ModuleMaster moduleMaster;
	
	@Size(max = 50)
	private String fieldName;

	@Size(max = 20)
	private String fieldType;
	
	@Size(max = 20)
	private String filterType;
	
	@Size(max = 50)
	private String width;
	
	@Size(max = 50)
	private String lookupValues;
	
	@Size(max = 50)
	private String lookupSelect;
	
	@Size(max = 50)
	private String singleSelect;
	
	@Size(max = 50)
	private String lookupLabelValue;
	
	@Size(max = 50)
	private String defaultLabel;
	
	@Size(max = 300)
	private String lookupQuery;
	
	@Size(max = 50)
	private String lookupCriteria1;
	
	@Size(max = 50)
	private String lookupCriteria2;
	
	@Size(max = 50)
	private String lookupCriteria3;
	
	@Size(max = 2000)
	private String lookupJson;
	
	@Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;
	
	private boolean showDefaultFilter;
	
	@Size(max = 10)
	private Integer filterSequenceOrder;
	
	@Transient
	private List<Object> filterLookupList;
	
	@Transient
	private String searchValue1;
	
	@Transient
	private String searchValue2;
	
	@Transient
	private String searchValue3;
	
	
                                                                                                                                           
}

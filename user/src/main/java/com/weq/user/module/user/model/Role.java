package com.weq.user.module.user.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.weq.user.module.system.model.RoleModule;

import lombok.Data;

@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true,
value = {"hibernateLazyInitializer", "handler"})
public class Role {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    @Size(max = 100)
    private String name;
    
    @Size(max = 100)
	private String subName;

    @Size(max = 100)
    private String description;    
 
    @Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;
    
    @Transient
	private List<RoleModule> roleModuleList = new ArrayList<RoleModule>();
    
}
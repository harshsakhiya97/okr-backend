package com.weq.user.module.user.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.weq.user.module.user.model.User;
import com.weq.user.module.user.model.UserLoginLogs;

public interface UserService {
	String changePassword(User user) throws Exception;
	String forgetPassword(User user) throws Exception;
	void userRegistration(User user) throws Exception;
	String sendOtpToUser(User user) throws Exception;
	String checkDuplicateEmailAndMobileNo(User user) throws Exception;
	User validateUser(String username) throws Exception;
	void userProfile(MultipartFile file, Long userId) throws Exception;
	User getUserById(User user) throws Exception;
	List<User> getUserList(User user) throws Exception;
	Integer getUserListLength(User user) throws Exception;
	void activeDeactiveUser(User user) throws Exception;
	String sendNotificationToUser(User user) throws Exception;
	void updateUserDetail(User user) throws Exception;
	UserLoginLogs saveUserLogs(UserLoginLogs userLogs) throws Exception;
	List<UserLoginLogs> getUserLogsList(UserLoginLogs userLogs) throws Exception;
	Integer getUserLogsLength(UserLoginLogs userLogs) throws Exception;
	void updateDownloadCounts(User user) throws Exception;
}
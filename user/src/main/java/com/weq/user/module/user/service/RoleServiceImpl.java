package com.weq.user.module.user.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.weq.user.module.system.model.RoleModule;
import com.weq.user.module.user.model.Role;
import com.weq.utility.module.common.dao.GenericDao;


@Service
public class RoleServiceImpl implements RoleService {
	private static final Logger logger = Logger.getLogger(RoleServiceImpl.class);
	
	@Autowired
	private GenericDao genericDao;

	@Override
	public List<Role> getRoleList() throws Exception {
		logger.info("RoleServiceImpl getRoleList");
		String query="from Role where isDelete='N' ";
		
		return genericDao.findCustomList(query);
	}

	@Override	
	public Role getRoleByName(String name) throws Exception {
		logger.info("RoleServiceImpl getRoleByName Data-->" + name);
		List<Role> roleList=new ArrayList<Role>();
		String query="from Role where isDelete='N' and name='"+name+"' ";
		roleList=genericDao.findCustomList(query);
		
		if(roleList!=null && roleList.size() > 0) {
			return roleList.get(0);
		}else {
			return null;
		}

	}

	@Override
	public Role saveRole(Role role) throws Exception {
		logger.info("RoleServiceImpl saveRole Data-->" + role);
		return (Role) genericDao.save(role);
	}

	@Override
	public void deleteRole(Long id) throws Exception {
		logger.info("RoleServiceImpl deleteRole Data-->" + id);
		Role role = (Role) genericDao.findOneLong(id, Role.class);
		if (role != null) {
			role.setIsDelete("Y");
			genericDao.save(role);
		}
		logger.info("RoleServiceImpl deleteRole End");
	}

	@Override
	public Role getRoleById(Long id) throws Exception {
		logger.info("RoleServiceImpl getRoleById Data-->" + id);
		return (Role) genericDao.findOneLong(id, Role.class);
	}

	@Override
	public List<RoleModule> getRoleModuleListByRoleId(Role role) throws Exception {
		logger.info("RoleServiceImpl getRoleModuleListByRoleId Data-->");
		List<RoleModule> roleModuleList=new ArrayList<>();
		
		if(role!=null && role.getId()!=0) {
			String query="from RoleModule where isDelete='N' and role.id='"+role.getId()+"'";
			roleModuleList=genericDao.findCustomList(query);
		}
		
		
		return roleModuleList;
	}

	@Override
	public List<Role> getUserRoleList() throws Exception {
		logger.info("RoleServiceImpl getUserRoleList Start");
		 List<Role>  roleList = new ArrayList<Role>();
		String query="from Role where isDelete='N' and id not in ('1','2','3') ";
		roleList = genericDao.findCustomList(query);
		logger.info("RoleServiceImpl getUserRoleList End");
		return roleList;
	}
}
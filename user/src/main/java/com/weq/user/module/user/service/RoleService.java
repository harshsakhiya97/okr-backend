package com.weq.user.module.user.service;

import java.util.List;

import com.weq.user.module.system.model.RoleModule;
import com.weq.user.module.user.model.Role;


public interface RoleService {
	void deleteRole(Long id) throws Exception;

	List<Role> getRoleList() throws  Exception;

	Role getRoleByName(String name) throws Exception;

	Role saveRole(Role role) throws Exception;

	Role getRoleById(Long id) throws Exception;

	List<RoleModule> getRoleModuleListByRoleId(Role role) throws Exception;

	List<Role> getUserRoleList() throws Exception;
}
package com.weq.user.module.company.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.weq.user.module.company.model.CompanyDetailMaster;
import com.weq.utility.module.aws.AmazonS3DTO;
import com.weq.utility.module.aws.service.AwsService;
import com.weq.utility.module.common.dao.GenericDao;

@Service
public class CompanyDetailServiceImpl implements CompanyDetailService{

private static final Logger logger = Logger.getLogger(CompanyDetailServiceImpl.class);
	
	@Autowired
	private GenericDao genericDao;
	
	@Autowired
	private AwsService awsService;

	@Override
	public void saveCompanyDetail(CompanyDetailMaster companyDetailMaster) throws Exception {
		logger.info("CompanyDetailServiceImpl->>> saveCompanyDetail Start -->");	
		
		if(companyDetailMaster.getDocumentSelectedPath()!=null && !companyDetailMaster.getDocumentSelectedPath().equalsIgnoreCase("")) {
			String fileSelectPath = companyDetailMaster.getDocumentSelectedPath() + "/"
					+ companyDetailMaster.getNewFileName();
				File file = new File(fileSelectPath);
			
				logger.info("PredefinedServiceImpl->>> savePredefinedWithFile file upload Start -->");
				AmazonS3DTO amazonS3DTO = new AmazonS3DTO();
				String key = "CompanyOwner/"+UUID.randomUUID().toString()+"-"+companyDetailMaster.getNewFileName();		
				amazonS3DTO = awsService.uploadDataToAmazonS3WithFile(file, key);
				
				companyDetailMaster.setFileKey(amazonS3DTO.getS3Key());
				companyDetailMaster.setFilePath(amazonS3DTO.getFilePath());
				companyDetailMaster.setFilePath(amazonS3DTO.getFilePath());
		}
		
		if(companyDetailMaster.getCompanyDetailId() == null || companyDetailMaster.getCompanyDetailId() == 0) {
			companyDetailMaster = (CompanyDetailMaster) genericDao.save(companyDetailMaster);
		}else {
			companyDetailMaster = (CompanyDetailMaster) genericDao.update(companyDetailMaster);
		}
		
		genericDao.save(companyDetailMaster);
		logger.info("CompanyDetailServiceImpl->>> saveCompanyDetail End -->");
	}

	@Override
	public CompanyDetailMaster getCompanyDetail(CompanyDetailMaster companyDetailMaster) throws Exception {
		logger.info("CompanyDetailServiceImpl->>> getCompanyDetail Start -->");		
		
		List<CompanyDetailMaster> companyDetailMasterList = new ArrayList<CompanyDetailMaster>();
		
		String query = "from CompanyDetailMaster";
		
		companyDetailMasterList = genericDao.findCustomList(query);
		
		if(companyDetailMasterList!=null && companyDetailMasterList.size() > 0) {
			companyDetailMaster = companyDetailMasterList.get(0);
		}
			
		logger.info("CompanyDetailServiceImpl->>> getCompanyDetail End -->");		
		return companyDetailMaster;
	}
	
}

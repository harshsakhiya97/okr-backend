package com.weq.user.module.user.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.weq.user.module.user.model.User;
import com.weq.utility.module.common.model.TimestampEntity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString(exclude = { "user" })
@JsonIgnoreProperties(
value = {"hibernateLazyInitializer", "handler"})
public class UserLoginLogs extends TimestampEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Integer userLoginLogsId;
	
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "userId")
	private User user;
	
	@Column(name = "userId", insertable = false, updatable = false)
	private Long userId;
	
	@Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;
	
	@Transient
	private Integer pageSize;
	
	@Transient
	private Integer pageNumber;
	

}

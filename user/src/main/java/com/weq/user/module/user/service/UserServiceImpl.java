package com.weq.user.module.user.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.weq.communication.model.email.Mail;
import com.weq.communication.service.email.EmailService;
import com.weq.user.module.user.model.Role;
import com.weq.user.module.user.model.User;
import com.weq.user.module.user.model.UserLoginLogs;
import com.weq.utility.configuration.NetworkUtil;
import com.weq.utility.configuration.UtilityProperties;
import com.weq.utility.module.aws.AmazonS3DTO;
import com.weq.utility.module.aws.service.AwsService;
import com.weq.utility.module.common.dao.GenericDao;
import com.weq.utility.module.common.model.SequenceMaster;
import com.weq.utility.module.common.service.SequenceService;
import com.weq.utility.module.common.util.DataUtil;

@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserDetailsService, UserService {
	private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	@Autowired
	private RoleService roleService;
	
	@Autowired
	private GenericDao genericDao;

	@Autowired
	private AwsService awsService;

	@Autowired
	private UtilityProperties utilityProperties;

	@Autowired
	private DataUtil dataUtil;

	@Autowired
	private SequenceService sequenceService;

	@Autowired
	private EmailService emailService;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("UserServiceImpl loadUserByUsername Start Data--> " + username);
		// User user = userDao.findByUsername(username);
		User user = null;
		try {
			user = validateUser(username);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		Set<SimpleGrantedAuthority> authoritySet = new HashSet<>();
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		try {
			authoritySet = getAuthority(user);

		} catch (Exception e) {
			logger.error("loadUserByUsername: " + e.getMessage());
		}

		logger.info("UserServiceImpl loadUserByUsername End");

		System.out.println("getClientIpAddress " + NetworkUtil.getClientIpAddress(request));

		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				authoritySet);
	}

	private Set<SimpleGrantedAuthority> getAuthority(User user) throws Exception {
		logger.info("UserServiceImpl getAuthority Start Data-->" + user);
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		user.getRoles().forEach(role -> {
			// authorities.add(new SimpleGrantedAuthority(role.getName()));
			authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
		});
		logger.info("UserServiceImpl getAuthority End");
		return authorities;
		// return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

	@Override
	public User validateUser(String username) throws Exception {
		User user = new User();
		String query = "from User where isDelete='N' and (username='" + username + "' or " + " email='" + username
				+ "' or phone='" + username + "') ";
		List<User> userList = new ArrayList<User>();
		userList = genericDao.findCustomList(query);

		if (userList != null && userList.size() > 0) {
			user = userList.get(0);
		} else {
			user = null;
		}

		return user;
	}

	@Override
	public String changePassword(User user) throws Exception {
		logger.info("UserServiceImpl changePassword Start Data-->" + user);
		String newPassword = "";
		String changeStatus = "";

		User exisitingUser = validateUser(user.getUsername());

		newPassword = bcryptEncoder.encode(user.getNewPassword());
		bcryptEncoder.encode(user.getCurrentPassword());

		if (exisitingUser != null && newPassword != null
				&& bcryptEncoder.matches(user.getCurrentPassword(), exisitingUser.getPassword())) {
			exisitingUser.setPassword(newPassword);
			genericDao.save(exisitingUser);
			changeStatus = "SUCCESS";
		} else {
			changeStatus = "ERROR";
			// throw new CustomException("Current Password wrong!!! ","MISMATCH");
		}
		logger.info("UserServiceImpl changePassword End");
		return changeStatus;
	}

	@Override
	public String forgetPassword(User user) throws Exception {
		logger.info("UserServiceImpl forgetPassword Start Data-->" + user);
		String newPassword = "";
		String changeStatus = "";

		User exisitingUser = validateUser(user.getUsername());

		newPassword = bcryptEncoder.encode(user.getNewPassword());

		if (exisitingUser != null && newPassword != null) {
			exisitingUser.setPassword(newPassword);
			genericDao.save(exisitingUser);
			changeStatus = "SUCCESS";
		} else {
			changeStatus = "NO-USER";
		}
		logger.info("UserServiceImpl forgetPassword End");
		return changeStatus;
	}

	@Override
	public String sendOtpToUser(User user) throws Exception {
		logger.info("UserServiceImpl sendOtpToUser Start -->");
		Random r = new Random();
		String randomPin = (String.format("%04d", r.nextInt(10000)));
		System.out.println("Random number: " + randomPin);

		Mail mail =  new Mail();
		mail.setTo(user.getEmail());
		mail.setSubject("OTP For OKR is - "+randomPin);
		mail.setFrom(utilityProperties.getFromEmail());
	
		Map<String,Object> modelMap = new HashMap<String, Object>();
		modelMap.put("randomPin", randomPin);
		modelMap.put("name", user.getFirstName());
		modelMap.put("companyLogo", utilityProperties.getCompanyLogo());

		mail.setModel(modelMap);
		
		mail.setTemplateCode("otp-template");
		
		final CompletableFuture<String> sendStatus = CompletableFuture.supplyAsync(() -> {
			try {
				emailService.sendDirectEmail(mail);
				return "SUCCESS";
			} catch (Exception e) {
				logger.error("UserServiceImpl sendOtpToUserOTPMail ERROR" + e.getMessage(), e);
				e.printStackTrace();
				return "ERROR";
			}
			
		});
		
		/*SMS sms = new SMS();
		sms.setMessage(randomPin
				+ "%20is%20your%20One%20Time%20Password%20(OTP)%20for%20Ironoid%20User%20Registration.%20Do%20not%20share%20this%20OTP%20with%20anyone%20for%20your%20account%20security.");
		sms.setSmsTo(user.getPhone());
		try {
			if (sms.getSmsTo() != null && !sms.getSmsTo().isEmpty() && sms.getMessage() != null
					&& !sms.getMessage().isEmpty()) {
				sendSingleSMS(sms);
			} else {
				return "";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}*/
		logger.info("UserServiceImpl sendOtpToUser End -->");
		return randomPin;
	}

	@Override
	public String checkDuplicateEmailAndMobileNo(User user) throws Exception {
		logger.info("UserServiceImpl checkDuplicateEmailAndMobileNo Start -->");
		String duplicateCode = "";
		Integer emailCount = 0;
		Integer phoneCount = 0;

		if(user.getEmail() != null && !user.getEmail().equalsIgnoreCase("")) {
			String emailQuery = "Select count(id) from User where isDelete='N' and email='" + user.getEmail() + "' ";
			emailCount = genericDao.countCustomList(emailQuery).intValue();
		}

		if(user.getPhone() != null && !user.getPhone().equalsIgnoreCase("")) {
			String phoneQuery = "Select count(id) from User where isDelete='N' and phone='"+user.getPhone()+"' ";
			phoneCount = genericDao.countCustomList(phoneQuery).intValue();
		}

		logger.info("UserServiceImpl checkDuplicateEmailAndMobileNo End -->");
		if (emailCount > 0 && phoneCount > 0) {
			duplicateCode = "EMAIL-PHONE-DUP";
		} else if (emailCount > 0) {
			duplicateCode = "EMAIL-DUP";
		} else if (phoneCount > 0) {
			duplicateCode = "PHONE-DUP";
		} else {
			duplicateCode = "NO-DUP";
		}

		return duplicateCode;
	}

	@Override
	public String sendNotificationToUser(User user) throws Exception {
		user = getUserById(user);

		if (user.getId() != 0 && user.getFireBaseId() != null && !user.getFireBaseId().isEmpty()) {
			dataUtil.sendFCMNotification(user.getFireBaseId(), user.getTitle(), user.getMessage());
			return "SUCCESS";
		} else {
			return "ERROR";
		}

	}

	
	@Override
	public void userRegistration(User user) throws Exception {
		logger.info("UserServiceImpl userRegistration Start -->");
		user.setIsDeactive("N");
		user.setIsDelete("N");
		Role role = new Role();
		role = roleService.getRoleByName("USER");

		Set<Role> roles = new HashSet<Role>();

		if (role != null) {
			roles.add(role);
			user.setRole(role);
		}

		user.setRoles(roles);
		user.setUsername(user.getPhone());
		String password = user.getCurrentPassword();

		if (password != null && !password.isEmpty()) {
			user.setPassword(bcryptEncoder.encode(password));
		}

		String sequenceType = "OKR-USER";
		SequenceMaster sequenceMaster = sequenceService.getSequence(sequenceType);
		user.setUniqueCustomerId(sequenceType+"-"+sequenceMaster.getNextVal());
		sequenceService.updateSequence(sequenceType);

		user = (User) genericDao.save(user);

		logger.info("UserServiceImpl userRegistration End -->");
	}
	
	@Override
	public void userProfile(MultipartFile file, Long userId) throws Exception {
		logger.info("UserServiceImpl userProfile Start -->");
		User user = new User();
		String query = "from User where id='" + userId + "' ";
		List<User> userList = new ArrayList<User>();
		userList = genericDao.findCustomList(query);

		if (userList != null && userList.size() > 0) {
			AmazonS3DTO amazonS3DTO = new AmazonS3DTO();
			user = userList.get(0);
			String fileName = file.getOriginalFilename();
			String key = "UserProfile/User-ID-" + user.getId() + "/image/" + UUID.randomUUID().toString() + "-"
					+ fileName;
			amazonS3DTO = awsService.uploadDataToAmazonS3(file, key);

			String updateQuery = "Update User set userLogoKey='" + amazonS3DTO.getS3Key() + "', userLogoPath='"
					+ amazonS3DTO.getFilePath() + "'" + " , fileName='" + amazonS3DTO.getFileName() + "' where id = '"
					+ user.getId() + "' ";
			genericDao.customUpdate(updateQuery);

		} else {
			throw new Exception();
		}

		logger.info("UserServiceImpl userProfile End -->");

	}

	@Override
	public void updateUserDetail(User user) throws Exception {
		
		User prevUserDetails = getUserById(user);
		String updateQuery = "Update User set modifiedDate=CURRENT_TIMESTAMP ";
		
		if(user.getFireBaseId() != null && !user.getFireBaseId().equalsIgnoreCase("") && 
				!user.getFireBaseId().equalsIgnoreCase(prevUserDetails.getFireBaseId()) ) {
			updateQuery += " , fireBaseId='"+user.getFireBaseId()+"' ";
		}

		if(user.getFirstName() != null && !user.getFirstName().equalsIgnoreCase("") && 
				!user.getFirstName().equalsIgnoreCase(prevUserDetails.getFirstName()) ) {
			updateQuery += " , firstName='"+user.getFirstName()+"' ";
		}

		if(user.getMiddleName() != null && !user.getMiddleName().equalsIgnoreCase("") && 
				!user.getMiddleName().equalsIgnoreCase(prevUserDetails.getMiddleName()) ) {
			updateQuery += " , middleName='"+user.getMiddleName()+"' ";
		}

		if(user.getLastName() != null && !user.getLastName().equalsIgnoreCase("") && 
				!user.getLastName().equalsIgnoreCase(prevUserDetails.getLastName()) ) {
			updateQuery += " , lastName='"+user.getLastName()+"' ";
		}

		if(user.getIndustry() != null && 
				(prevUserDetails.getIndustry() == null || 
				user.getIndustry().getPredefinedId() != prevUserDetails.getIndustry().getPredefinedId()) ) {
			updateQuery += " , industryId='"+user.getIndustry().getPredefinedId()+"' ";
		}

		if(user.getDesignation() != null && 
				(prevUserDetails.getDesignation() == null || 
				user.getDesignation().getPredefinedId() != prevUserDetails.getDesignation().getPredefinedId()) ) {
			updateQuery += " , designationId='"+user.getDesignation().getPredefinedId()+"' ";
		}

		if(user.getDesignationLevel() != null && 
				(prevUserDetails.getDesignationLevel() == null || 
				user.getDesignationLevel().getPredefinedId() != prevUserDetails.getDesignationLevel().getPredefinedId()) ) {
			updateQuery += " , designationLevelId='"+user.getDesignationLevel().getPredefinedId()+"' ";
		}

		if(user.getDepartment() != null && 
				(prevUserDetails.getDepartment() == null || 
				user.getDepartment().getPredefinedId() != prevUserDetails.getDepartment().getPredefinedId()) ) {
			updateQuery += " , departmentId='"+user.getDepartment().getPredefinedId()+"' ";
		}

		if(user.getCompanyName() != null && !user.getCompanyName().equalsIgnoreCase("") && 
				!user.getCompanyName().equalsIgnoreCase(prevUserDetails.getCompanyName()) ) {
			updateQuery += " , companyName='"+user.getCompanyName()+"' ";
		}

		if(user.getCompanyWebsite() != null && !user.getCompanyWebsite().equalsIgnoreCase("") && 
				!user.getCompanyWebsite().equalsIgnoreCase(prevUserDetails.getCompanyWebsite()) ) {
			updateQuery += " , companyWebsite='"+user.getCompanyWebsite()+"' ";
		}

		if(user.getLocation() != null && !user.getLocation().equalsIgnoreCase("") && 
				!user.getLocation().equalsIgnoreCase(prevUserDetails.getLocation()) ) {
			updateQuery += " , location='"+user.getLocation()+"' ";
		}

		if(user.getRegion() != null && 
				(prevUserDetails.getRegion() == null || 
				user.getRegion().getPredefinedId() != prevUserDetails.getRegion().getPredefinedId()) ) {
			updateQuery += " , regionId='"+user.getRegion().getPredefinedId()+"' ";
		}
		
		updateQuery += " where id = '"+user.getId()+"' ";

		genericDao.customUpdate(updateQuery);

	}
	
	@Override
	public void updateDownloadCounts(User user) throws Exception {
		User prevUserDetails = getUserById(user);

		String updateQuery = "Update User set modifiedDate=CURRENT_TIMESTAMP, "
				+ "totalDownloads='"+(prevUserDetails.getTotalDownloads()+1)+"' ";
		genericDao.customUpdate(updateQuery);

	}


	@Override
	public void activeDeactiveUser(User user) throws Exception {
		logger.info("UserServiceImpl->>> activeDeactiveUser Start-->");
		String query = "Update User set isDeactive='" + user.getIsDeactive()
				+ "',modifiedDate=CURRENT_TIMESTAMP where id='" + user.getId() + "'  ";
		genericDao.customUpdate(query);
		logger.info("UserServiceImpl->>> activeDeactiveUser End-->");
	}

	
	@Override
	public List<User> getUserList(User user) throws Exception {
		logger.info("UserServiceImpl->>> getUserList Start-->");

		List<User> userList = new ArrayList<User>();
		String query = "from User u where u.isDelete='N' ";
		query = getQueryByCriteria(query, user);

		logger.info("UserServiceImpl->>> getUserList End-->");

		userList = genericDao.findCustomListPaginate(query, user.getPageNumber() != null ? user.getPageNumber() : 0,
				user.getPageSize() != null ? user.getPageSize() : 10);
		

		return userList;
	}

	@Override
	public Integer getUserListLength(User user) throws Exception {
		logger.info("UserServiceImpl->>> getUserListLength start");

		String query = "Select count(u.id) from User u where u.isDelete='N'  ";

		query = getQueryByCriteria(query, user);
		logger.info("UserServiceImpl->>> getUserListLength end");

		return genericDao.countCustomList(query).intValue();
	}

	private String getQueryByCriteria(String query, User user) {

		query = query + " and u.role = (Select id from Role where name = 'USER' ) ";
		
		if (user.getFirstName() != null && !user.getFirstName().isEmpty()) {
			query = query + " and u.firstName like '" + user.getFirstName() + "%' ";
		}

		if (user.getPhone() != null && !user.getPhone().isEmpty()) {
			query = query + " and u.phone like '" + user.getPhone() + "%' ";
		}

		if (user.getEmail() != null && !user.getEmail().isEmpty()) {
			query = query + " and u.email like '" + user.getEmail() + "%' ";
		}

		return query;
	}

	
	@Override
	public UserLoginLogs saveUserLogs(UserLoginLogs userLogs) throws Exception {
		logger.info("UserServiceImpl->>> saveUserLogs Start-->");
		userLogs.setIsDelete("N");
		userLogs = (UserLoginLogs) genericDao.save(userLogs);
		logger.info("UserServiceImpl->>> saveUserLogs End-->");
		
		return userLogs;

	}

	@Override
	public List<UserLoginLogs> getUserLogsList(UserLoginLogs userLogs) throws Exception {
		logger.info("UserServiceImpl->>> getUserLogsList Start-->");
		List<UserLoginLogs> userLogList = new ArrayList<UserLoginLogs>();
		
		String query = " from UserLoginLogs where isDelete='N' ";
		query = getUserLogsListFilter(userLogs,query);
		
		userLogList = genericDao.findCustomListPaginate(query, userLogs.getPageNumber() != null ? userLogs.getPageNumber() : 0,
				userLogs.getPageSize() != null ? userLogs.getPageSize() : 10);
		logger.info("UserServiceImpl->>> getUserLogsList End-->");
		
		return userLogList;

	}
	
	@Override
	public Integer getUserLogsLength(UserLoginLogs userLogs) {
		logger.info("UserServiceImpl->>> getUserLogsLength Start-->");
		String query = "Select count(userLoginLogsId) from UserLoginLogs where isDelete='N' ";
		query = getUserLogsListFilter(userLogs,query);
		logger.info("UserServiceImpl->>> getUserLogsLength Start-->");
		
		return genericDao.countCustomList(query).intValue();
	}
	
	public String getUserLogsListFilter(UserLoginLogs userLogs,String query) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		if(userLogs.getUser() != null) {
			query += " and userId='"+userLogs.getUser().getId()+"'";
		}
		
		if(userLogs.getCreatedDate() != null) {
			String date1 = formatter.format(userLogs.getCreatedDate());
			query=query+" and createdDate like '"+date1+"%'";
			
		}
		
		return query;
		
	}



	@Override
	public User getUserById(User user) throws Exception {
		return (User) genericDao.findOneLong(user.getId(), User.class);
	}




}
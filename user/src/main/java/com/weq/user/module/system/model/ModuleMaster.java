package com.weq.user.module.system.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Entity
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(
	    uniqueConstraints=
	        @UniqueConstraint(columnNames={"moduleCode","moduleUrl"})
	)
public class ModuleMaster {

	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer moduleId;
    
    @Size(max=100)
    private String moduleName;
    
    @Size(max=20)
    private String moduleCode;
    
    @Size(max=100)
    private String moduleUrl;
    
    @Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;
    
    @Transient
	private Integer pageNumber;

	@Transient
	private Integer pageSize;
	
}

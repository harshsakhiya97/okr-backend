package com.weq.user.module.user.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.weq.user.module.system.model.AccessControlMaster;
import com.weq.utility.module.common.model.PredefinedMaster;
import com.weq.utility.module.common.model.TimestampEntity;

import lombok.Data;

@Entity
@Data
public class User extends TimestampEntity {

	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private long id;

	@Size(max = 100)
	private String uniqueCustomerId;

	@Size(max = 100)
	private String firstName;
	
	@Size(max = 100)
	private String middleName;
	
	@Size(max = 100)
	private String lastName;

	@Size(max = 20)
	private String phone;
	
	@Size(max = 150)
	private String email;
	
	@Size(max = 200)
	private String fireBaseId;
	
	@Column
	private String username;

	@Column
	@JsonIgnore
	private String password;
	
	@Size(max = 250)
	private String userProfilePath;
	
	@Size(max = 150)
	private String fileName;
	

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "USER_ROLES", joinColumns = { @JoinColumn(name = "USER_ID") }, inverseJoinColumns = {
	@JoinColumn(name = "ROLE_ID") })
	@JsonBackReference("roles")
	private Set<Role> roles;
	
	@ManyToOne(targetEntity = Role.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "role")
	@JsonBackReference("role")
	private Role role;
	
	@Column(name = "role", insertable = false, updatable = false)
	private Long roleId;

	@ManyToOne(targetEntity = PredefinedMaster.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "industryId")
	private PredefinedMaster industry;
	
	@Column(name = "industryId", insertable = false, updatable = false)
	private Integer industryId;

	@ManyToOne(targetEntity = PredefinedMaster.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "designationId")
	private PredefinedMaster designation;
	
	@Column(name = "designationId", insertable = false, updatable = false)
	private Integer designationId;

	@ManyToOne(targetEntity = PredefinedMaster.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "designationLevelId")
	private PredefinedMaster designationLevel;
	
	@Column(name = "designationLevelId", insertable = false, updatable = false)
	private Integer designationLevelId;

	@ManyToOne(targetEntity = PredefinedMaster.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "departmentId")
	private PredefinedMaster department;
	
	@Column(name = "departmentId", insertable = false, updatable = false)
	private Integer departmentId;

	@Size(max = 200)
	private String companyName;
	
	@Size(max = 200)
	private String location;

	@ManyToOne(targetEntity = PredefinedMaster.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "regionId")
	private PredefinedMaster region;
	
	@Column(name = "regionId", insertable = false, updatable = false)
	private Integer regionId;

	@Size(max = 200)
	private String companyWebsite;
	
	@Column(name="total_downloads", columnDefinition="int(10) default '0'")
	private Integer totalDownloads;
	
	@Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;
	
	@Column(name = "is_deactive", columnDefinition = "varchar(10) default 'N' ")
	private String isDeactive;
	
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "createdBy")
	private User createdBy;
	
	@Column(name = "createdBy", insertable = false, updatable = false)
	private Long createdById;
	
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "modifiedBy")
	private User modifiedBy;
	
	@Column(name = "modifiedBy", insertable = false, updatable = false)
	private Long modifiedById;
	
	@Transient
	private String currentOTP;
		
	@Transient
	private String roleName;
	
	@Transient
	private String currentPassword;

	@Transient
	private String newPassword;

	@Transient
	private String confirmNewPassword;
	
	@Transient
	private String fullName;

	@Transient
	private String title;

	@Transient
	private String message;

	@Transient
	private String searchUser;
	
	@Transient
	private Integer lastUserId;
	
	@Transient
	private Integer recordSize;
	
	@Transient
	private Integer pageSize;
	
	@Transient
	private Integer pageNumber;
	
	public String getRoleName() {
		
		if((roleName==null || roleName.isEmpty()) && role!=null && role.getId()!=0) {
			roleName = role.getName();
		}
		
		return roleName;
	}
	
	public String getFullName() {
		
		if(fullName==null || fullName.isEmpty()) {
			fullName = firstName!=null?firstName:" "+middleName!=null?middleName:" "+lastName!=null?lastName:"";
			
			try {
				String[] splited = fullName.split("\\s+"); 
				String newStr = "";
				String name = "";
				for (String string : splited) {
					//System.out.println(String.valueOf(Character.toUpperCase(string.charAt(0))));
					newStr = string.replaceFirst(String.valueOf(string.charAt(0)), String.valueOf(Character.toUpperCase(string.charAt(0))));
					name = name.concat(newStr);
					name = name + " ";
				}
				fullName = name;
			}catch(Exception e) {
				
			}
			
		}
		
		return fullName;
	}

	@Transient
	private List<AccessControlMaster> accessControlMasterList=new ArrayList<AccessControlMaster>();
	
	
	
	public String getIndustryName() {
		
		if(industry != null && industry.getName() != null) {
			return industry.getName();
		} else {
			return "";
		}
		
	}

	public String getDesignationName() {
		
		if(designation != null && designation.getName() != null) {
			return designation.getName();
		} else {
			return "";
		}
		
	}

	public String getDesignationLevelName() {
		
		if(designationLevel != null && designationLevel.getName() != null) {
			return designationLevel.getName();
		} else {
			return "";
		}
		
	}

	public String getDepartmentName() {
		
		if(department != null && department.getName() != null) {
			return department.getName();
		} else {
			return "";
		}
		
	}

	public String getRegionName() {
		
		if(region != null && region.getName() != null) {
			return region.getName();
		} else {
			return "";
		}
		
	}


	
	
}
package com.weq.user.module.company.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.weq.utility.module.common.model.TimestampEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class CompanyDetailMaster extends TimestampEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer companyDetailId;
	
	@Size(max = 100)
	private String name;
	
	@Size(max = 100000)
	private String description;
	
	@Size(max = 150)
	private String email;
	
	@Size(max = 100)
	private String phone;
	
	@Size(max = 300)
	private String address;
	
	@Size(max = 300)
	private String facebookPage;
	
	@Size(max = 300)
	private String instagramPage;
	
	@Size(max = 300)
	private String twitterPage;
	
	@Size(max = 100)
	private String fileName;
	
	@Size(max = 300)
	private String filePath;
	
	@Size(max = 200)
	private String fileKey;
	
	@Transient
	private String documentSelectedPath;
	
	@Transient
	private String documentPath;

	@Transient
	private String newFileName;
	
	
	
}

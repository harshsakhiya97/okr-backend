package com.weq.cms.dao.system;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.weq.user.module.system.model.ModuleMaster;

@Repository
public interface ModuleMasterDao extends JpaRepository<ModuleMaster, Integer>,JpaSpecificationExecutor<ModuleMaster>{

	@Query(value = "Select m from ModuleMaster m where m.moduleId=:moduleId and m.isDelete='N' ")
	ModuleMaster getModuleById(@Param("moduleId") Integer moduleId);

	@Query(value = "Select count(m.moduleId) from ModuleMaster m where m.isDelete='N' ")
	Integer getModuleListLength(@Param("isDelete") String isDelete);

}

package com.weq.cms.dao.field;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.weq.cms.model.field.FieldConfiguration;

@Repository
public interface FieldConfigurationDao extends JpaRepository<FieldConfiguration, Integer>, JpaSpecificationExecutor<FieldConfiguration> {

	@Query(value="Select fc from FieldConfiguration fc where fc.entityServer=:entityServer and fc.isDelete!='Y' order by fc.orderNumber ")
	List<FieldConfiguration> getFieldConfigurationListByEntity(@Param("entityServer") String entityServer);

	@Query(value = "Select f from FieldConfiguration f where f.fieldConfigurationId=:fieldConfigurationId and f.isDelete='N' ")
	FieldConfiguration getFieldConfigurationById(@Param("fieldConfigurationId") Integer fieldConfigurationId);

	@Query(value = "Select count(f.fieldConfigurationId) from FieldConfiguration f where f.isDelete='N' ")
	Integer getFieldConfigurationListLength(@Param("fieldConfigurationId")  String isDelete);

	@Query(value="Select fc from FieldConfiguration fc where fc.entityServer=:entityServer and fc.sectionName=:sectionName and fc.isDelete!='Y' order by fc.orderNumber ")
	List<FieldConfiguration> getFieldConfigurationListByEntityAnSection(@Param("entityServer") String entityServer,@Param("sectionName") String sectionName);

}

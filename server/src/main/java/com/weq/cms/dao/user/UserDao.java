package com.weq.cms.dao.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.weq.user.module.user.model.User;

@Repository
public interface UserDao extends CrudRepository<User, Long> {
    User findByUsername(String username);
}
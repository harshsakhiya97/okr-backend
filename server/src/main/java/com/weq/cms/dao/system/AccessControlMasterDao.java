package com.weq.cms.dao.system;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.weq.user.module.system.model.AccessControlMaster;

@Repository
public interface AccessControlMasterDao extends JpaRepository<AccessControlMaster, Integer>,JpaSpecificationExecutor<AccessControlMaster>{

	@Query(value = "Select a from AccessControlMaster a where a.accessControlId=:accessControlId and a.isDelete='N' ")
	AccessControlMaster getAccessControlMasterById(@Param("accessControlId") Integer accessControlId);

	@Query(value = "Select count(a.accessControlId) from AccessControlMaster a where a.isDelete='N' ")
	Integer getAcessControlAllListLength(@Param("isDelete") String isDelete);

}

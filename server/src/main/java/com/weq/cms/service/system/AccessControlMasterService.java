package com.weq.cms.service.system;

import java.util.List;

import com.weq.user.module.system.model.AccessControlMaster;
import com.weq.user.module.system.model.ModuleMaster;
import com.weq.user.module.user.model.Role;
import com.weq.user.module.user.model.User;

public interface AccessControlMasterService {

	AccessControlMaster saveAccessControl(AccessControlMaster accessControlMaster) throws Exception;

	List<AccessControlMaster> getAccessControlList(User user) throws Exception;

	List<AccessControlMaster> getAcessControlAllList(AccessControlMaster accessControlMaster) throws Exception;

	Integer getAcessControlAllListLength(AccessControlMaster accessControlMaster) throws Exception;

	void deleteAccessControl(Integer accessControlId) throws Exception;
	
	Integer getRoleModuleListCount (Role roleMaster, ModuleMaster moduleMaster) throws Exception;

	Integer getUserModuleCount(User userOld, ModuleMaster module) throws Exception;

	List<AccessControlMaster> getAcessControlListByRole(User user) throws Exception;

}

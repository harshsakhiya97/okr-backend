package com.weq.cms.service.system;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.weq.cms.dao.system.AccessControlMasterDao;
import com.weq.user.module.system.model.AccessControlMaster;
import com.weq.user.module.system.model.ModuleMaster;
import com.weq.user.module.user.model.Role;
import com.weq.user.module.user.model.User;
import com.weq.utility.module.common.dao.GenericDao;

@Service
public class AccessControlMasterServiceImpl implements AccessControlMasterService {
	private static final Logger logger = Logger.getLogger(AccessControlMasterServiceImpl.class);

	@Autowired
	private AccessControlMasterDao accessControlMasterDao;
	
	@Autowired
	private GenericDao genericDao;

	@Override
	public List<AccessControlMaster> getAccessControlList(User user) throws Exception {
		logger.info("AccessControlMasterServiceImpl getAccessControlList start");
		List<AccessControlMaster> accessControlMasterList = new ArrayList<AccessControlMaster>();

		Specification<AccessControlMaster> userSpec = Specification.where((root, query, cb) -> {
			return cb.and(cb.equal(root.get("user"), user.getId()), cb.equal(root.get("isDelete"), "N"));
		});
		accessControlMasterList = accessControlMasterDao.findAll(userSpec);
		logger.info("AccessControlMasterServiceImpl getAccessControlList end");
		return accessControlMasterList;
		
/*
		if (accessControlMasterList == null || accessControlMasterList.size() == 0) {

			List<Role> roleList = new ArrayList<Role>();
			
			if(user.getRoles()!=null && !user.getRoles().isEmpty()) {
				roleList.addAll(user.getRoles());
			}
			
			

			if (roleList != null && roleList.size() > 0) {
				Specification<AccessControlMaster> roleSpec = Specification.where((root, query, cb) -> {
					return cb.and(root.get("role").in(roleList), cb.equal(root.get("isDelete"), "N"));

				});
				accessControlMasterList = accessControlMasterDao.findAll(roleSpec);

				if (accessControlMasterList != null && accessControlMasterList.size() > 0) {
					List<AccessControlMaster> accessControlMasterOuterList = new ArrayList<AccessControlMaster>();
					accessControlMasterOuterList.addAll(accessControlMasterList);
					List<AccessControlMaster> accessControlMasterInnerList = new ArrayList<AccessControlMaster>();
					accessControlMasterInnerList.addAll(accessControlMasterList);

					for (AccessControlMaster accessControlOuter : accessControlMasterOuterList) {

						for (AccessControlMaster accessControlInner : accessControlMasterInnerList) {

							if (accessControlInner!=null && accessControlOuter!=null && accessControlOuter.getAccessControlId() != accessControlInner.getAccessControlId()) {

								if (accessControlOuter.getModuleMaster().getModuleId()
										.equals(accessControlInner.getModuleMaster().getModuleId())) {
									AccessControlMaster accessControlMasterNew = new AccessControlMaster();
									boolean checkOverride = false;

									accessControlMasterNew.setCreateAccess(accessControlOuter.isCreateAccess());
									accessControlMasterNew.setReadAccess(accessControlOuter.isReadAccess());
									accessControlMasterNew.setUpdateAccess(accessControlOuter.isUpdateAccess());
									accessControlMasterNew.setDeleteAccess(accessControlOuter.isDeleteAccess());
									accessControlMasterNew.setModuleMaster(accessControlOuter.getModuleMaster());
									accessControlMasterNew.setIsDelete("N");
									// accessControlMasterNew.setRole(accessControlOuter.getRole());
									// accessControlMasterNew.setRoleTemp(accessControlOuter.getRole());

									if (!accessControlOuter.isCreateAccess() && accessControlInner.isCreateAccess()) {
										accessControlInner.setCreateAccess(true);
										accessControlMasterNew.setCreateAccess(true);
										checkOverride = true;
									}

									if (!accessControlOuter.isReadAccess() && accessControlInner.isReadAccess()) {
										accessControlInner.setReadAccess(true);
										accessControlMasterNew.setReadAccess(true);
										checkOverride = true;
									}

									if (!accessControlOuter.isUpdateAccess() && accessControlInner.isUpdateAccess()) {
										accessControlInner.setUpdateAccess(true);
										accessControlMasterNew.setUpdateAccess(true);
										checkOverride = true;
									}

									if (!accessControlOuter.isDeleteAccess() && accessControlInner.isDeleteAccess()) {
										accessControlInner.setDeleteAccess(true);
										accessControlMasterNew.setDeleteAccess(true);
										checkOverride = true;
									}

									accessControlMasterList = (List<AccessControlMaster>) accessControlMasterList
											.stream()
											.filter(accessControlMasterTemp -> !accessControlMasterTemp
													.getModuleMaster().getModuleId()
													.equals(accessControlInner.getModuleMaster().getModuleId()))
											.collect(Collectors.toList());

									accessControlMasterOuterList = (List<AccessControlMaster>) accessControlMasterOuterList
											.stream()
											.filter(accessControlMasterTemp -> !accessControlMasterTemp
													.getModuleMaster().getModuleId()
													.equals(accessControlInner.getModuleMaster().getModuleId()))
											.collect(Collectors.toList());

									accessControlMasterInnerList = (List<AccessControlMaster>) accessControlMasterInnerList
											.stream()
											.filter(accessControlMasterTemp -> !accessControlMasterTemp
													.getModuleMaster().getModuleId()
													.equals(accessControlInner.getModuleMaster().getModuleId()))
											.collect(Collectors.toList());

									if (checkOverride) {
										accessControlMasterNew
												.setAccessControlId(accessControlOuter.getAccessControlId());
									}

									accessControlMasterList.add(accessControlMasterNew);

								}

							} else {
								accessControlMasterList = (List<AccessControlMaster>) accessControlMasterList.stream()
										.filter(accessControlMasterTemp -> !accessControlMasterTemp.getAccessControlId()
												.equals(accessControlInner.getAccessControlId()!=null?accessControlInner.getAccessControlId():0))
										.collect(Collectors.toList());

								accessControlMasterOuterList = (List<AccessControlMaster>) accessControlMasterOuterList
										.stream()
										.filter(accessControlMasterTemp -> !accessControlMasterTemp.getAccessControlId()
												.equals(accessControlInner.getAccessControlId()!=null?accessControlInner.getAccessControlId():0))
										.collect(Collectors.toList());

								accessControlMasterInnerList = (List<AccessControlMaster>) accessControlMasterInnerList
										.stream()
										.filter(accessControlMasterTemp -> !accessControlMasterTemp.getAccessControlId()
												.equals(accessControlInner.getAccessControlId()!=null?accessControlInner.getAccessControlId():0))
										.collect(Collectors.toList());

								accessControlMasterList.add(accessControlInner);

							}

						}
					}

				}

			}

		}

		logger.info("AccessControlMasterServiceImpl getAccessControlList end");
		return accessControlMasterList;*/
	}

	@Override
	public AccessControlMaster saveAccessControl(AccessControlMaster accessControlMaster) throws Exception {
		logger.info("AccessControlMasterServiceImpl getAccessControlList start");
		accessControlMaster = accessControlMasterDao.save(accessControlMaster);
		logger.info("AccessControlMasterServiceImpl getAccessControlList end");
		return accessControlMaster;
	}

	@Override
	public List<AccessControlMaster> getAcessControlAllList(AccessControlMaster accessControlMaster) throws Exception {
		logger.info("AccessControlMasterServiceImpl getAcessControlAllList start");
		Specification<AccessControlMaster> spec =Specification.where((root, query, cb) -> {
		    return cb.and(
			        cb.equal(root.get("isDelete"), "N"));
			});
		logger.info("AccessControlMasterServiceImpl getAcessControlAllList end ");
		return accessControlMasterDao.findAll( Specification.where(spec),PageRequest.of(accessControlMaster.getPageNumber(), accessControlMaster.getPageSize()))
				.getContent();
	}

	@Override
	public Integer getAcessControlAllListLength(AccessControlMaster accessControlMaster) throws Exception {
		logger.info("AccessControlMasterServiceImpl getAcessControlAllListLength ");
		return accessControlMasterDao.getAcessControlAllListLength(accessControlMaster.getIsDelete());
	}

	@Override
	public void deleteAccessControl(Integer accessControlId) throws Exception {
		logger.info("AccessControlMasterServiceImpl deleteAccessControl Start Data-->" + accessControlId);
		AccessControlMaster accessControlMaster=accessControlMasterDao.getAccessControlMasterById(accessControlId);
		 if(accessControlMaster != null){
			 accessControlMaster.setIsDelete("Y");
			 accessControlMasterDao.save(accessControlMaster);
	        }	
		 logger.info("AccessControlMasterServiceImpl deleteAccessControl End");

	}

	@Override
	public Integer getRoleModuleListCount(Role roleMaster, ModuleMaster moduleMaster) throws Exception {
		logger.info("AccessControlMasterServiceImpl getRoleModuleListCount Start");
		
		String query = "Select count(accessControlId) from AccessControlMaster where isDelete = 'N' and "
				       + "role.id = "+ roleMaster.getId() +" and moduleMaster.moduleId = " 
					   + moduleMaster.getModuleId()+"";
		
		logger.info("AccessControlMasterServiceImpl getRoleModuleListCount End");
		return genericDao.countCustomList(query).intValue();
	}

	@Override
	public Integer getUserModuleCount(User userOld, ModuleMaster moduleMaster) throws Exception {
	logger.info("AccessControlMasterServiceImpl getUserModuleCount Start");
		
		String query = "Select count(accessControlId) from AccessControlMaster where isDelete = 'N' and "
				       + "user.id = "+ userOld.getId() +" and moduleMaster.moduleId = " 
					   + moduleMaster.getModuleId()+"";
		
		logger.info("AccessControlMasterServiceImpl getUserModuleCount End");
		return genericDao.countCustomList(query).intValue();
	}

	@Override
	public List<AccessControlMaster> getAcessControlListByRole(User user) throws Exception {
		logger.info("AccessControlMasterServiceImpl getAcessControlListByRole start");
		List<AccessControlMaster> accessControlMasterList = new ArrayList<AccessControlMaster>();
		
		if(user.getRoleId()!=null && user.getRoleId()!=0) {
			String query = "from AccessControlMaster where role='"+user.getRoleId()+"' ";
			accessControlMasterList = genericDao.findCustomList(query);			
		}
						
		logger.info("AccessControlMasterServiceImpl getAcessControlListByRole end");
		return accessControlMasterList;
	}

}

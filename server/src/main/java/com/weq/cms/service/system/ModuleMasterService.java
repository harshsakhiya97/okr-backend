package com.weq.cms.service.system;

import java.util.List;

import com.weq.user.module.system.model.ModuleMaster;

public interface ModuleMasterService {

	ModuleMaster saveModule(ModuleMaster moduleMaster) throws Exception;

	List<ModuleMaster> getModuleList(ModuleMaster moduleMaster) throws Exception;

	Integer getModuleListLength(ModuleMaster moduleMaster) throws Exception;

	void deleteModule(Integer moduleId) throws Exception;

}

package com.weq.cms.service.system;

import java.util.List;

import com.weq.user.module.system.model.FilterModuleMaster;
import com.weq.user.module.system.model.ModuleMaster;

public interface LookupService {

	List<FilterModuleMaster> getFilterListByModuleAndType(FilterModuleMaster filterModuleMaster) throws Exception;

	List<?> filterLookupDataBySearchValue(FilterModuleMaster filterModuleMaster) throws Exception;

	List<?> getModuleLookupList(ModuleMaster moduleMaster) throws Exception;

}

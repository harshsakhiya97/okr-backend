package com.weq.cms.service.field;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weq.cms.dao.field.FieldConfigurationDao;
import com.weq.cms.model.field.FieldConfiguration;
import com.weq.utility.module.common.dao.GenericDao;
import com.weq.utility.module.common.model.LookupData;

@Service
public class FieldConfigurationServiceImpl implements FieldConfigurationService{
	private static final Logger logger = Logger.getLogger(FieldConfigurationServiceImpl.class);
	
	@Autowired
	private FieldConfigurationDao fieldConfigurationDao;
	
	/*
	 * @Autowired private CommonDao commonDao;
	 */
	
	GenericDao< FieldConfiguration > commonDao;
	GenericDao< LookupData > lookupDao;
	 
	   @Autowired
	   public void setCommanDao( GenericDao< FieldConfiguration > daoToSet ){
		   commonDao = daoToSet;
		   commonDao.setClazz( FieldConfiguration.class );
	   }
	   
	   @Autowired
	   public void setLookupDao( GenericDao< LookupData > daoToSet ){
		   lookupDao = daoToSet;
		   lookupDao.setClazz( LookupData.class );
	   }   
	   
	
	@Override
	public List<FieldConfiguration> getFieldConfigurationListByEntity(FieldConfiguration fieldConfigurationFilter) throws Exception {
		logger.info("FieldConfigurationServiceImpl->>> getFieldConfigurationListByEntity  start-->");
		 List<FieldConfiguration> fieldConfigurationList=new ArrayList<FieldConfiguration>();
		 
		 if(fieldConfigurationFilter.getSectionName()!=null && !fieldConfigurationFilter.getSectionName().equalsIgnoreCase("")) { 
			 fieldConfigurationList=fieldConfigurationDao.getFieldConfigurationListByEntityAnSection(fieldConfigurationFilter.getEntityServer(),fieldConfigurationFilter.getSectionName());
		 }else {
			 fieldConfigurationList=fieldConfigurationDao.getFieldConfigurationListByEntity(fieldConfigurationFilter.getEntityServer());
		 }
		 
		
		logger.info("FieldConfigurationServiceImpl->>> getFieldConfigurationListByEntity  end-->");
		
		for(FieldConfiguration fieldConfiguration:fieldConfigurationList) {
			
			if(fieldConfiguration.getFieldType()!=null && (
					fieldConfiguration.getFieldType().equalsIgnoreCase("dropdown") || fieldConfiguration.getFieldType().equalsIgnoreCase("radio"))
					) {
				
				
				if(fieldConfiguration.getLookupQuery()!=null && !fieldConfiguration.getLookupQuery().equalsIgnoreCase("")) {
					
					String lookupQuery=fieldConfiguration.getLookupQuery();
					List<LookupData> lookupDataList=(List<LookupData>) lookupDao.findCustomList(lookupQuery);
					fieldConfiguration.setLookupDataList(lookupDataList);	 
					
				}else if(fieldConfiguration.getLookupJson()!=null && !fieldConfiguration.getLookupJson().equalsIgnoreCase("")) {
					
					ObjectMapper mapper = new ObjectMapper();
					List<LookupData> lookupDataList=mapper.readValue(fieldConfiguration.getLookupJson(), new TypeReference<List<LookupData>>(){});
					fieldConfiguration.setLookupDataList(lookupDataList);	 
					fieldConfiguration.setLookupDataList(lookupDataList);	 
					
				}
				
				
				
				 /*for(LookupData lookupData:lookupdataList){
				        	 logger.info("Lookup list id "+lookupData.getLookupId());
				        	 logger.info("Lookup list value "+lookupData.getLookupValue());
				  }*/
				
			}
			
		}
		
		
	
		        
		 
		return fieldConfigurationList;
	}

	@Override
	public FieldConfiguration saveFieldConfiguration(FieldConfiguration fieldConfiguration) throws Exception {
		logger.info("FieldConfigurationServiceImpl->>> saveFieldConfiguration  start-->");
		fieldConfiguration=fieldConfigurationDao.save(fieldConfiguration);	
		logger.info("FieldConfigurationServiceImpl->>> saveFieldConfiguration  end-->");
		return fieldConfiguration;
	}

	@Override
	public List<FieldConfiguration> getFieldConfigurationList(FieldConfiguration fieldConfiguration) throws Exception {
		logger.info("FieldConfigurationServiceImpl getFieldConfigurationList start");
		Specification<FieldConfiguration> spec =Specification.where((root, query, cb) -> {
		    return cb.and(
			        cb.equal(root.get("isDelete"), "N"));
			});
		logger.info("FieldConfigurationServiceImpl getFieldConfigurationList end ");
		return fieldConfigurationDao.findAll( Specification.where(spec),PageRequest.of(fieldConfiguration.getPageNumber(), fieldConfiguration.getPageSize()))
				.getContent();
	}

	@Override
	public Integer getFieldConfigurationListLength(FieldConfiguration fieldConfiguration) throws Exception {
		logger.info("FieldConfigurationServiceImpl getFieldConfigurationListLength ");
		return fieldConfigurationDao.getFieldConfigurationListLength(fieldConfiguration.getIsDelete());
	}

	@Override
	public void deleteFieldConfiguration(Integer fieldConfigurationId) throws Exception {
		logger.info("FieldConfigurationServiceImpl deleteFieldConfiguration Start Data-->" + fieldConfigurationId);
		FieldConfiguration fieldConfiguration=fieldConfigurationDao.getFieldConfigurationById(fieldConfigurationId);
		 if(fieldConfiguration != null){
			 fieldConfiguration.setIsDelete("Y");
			 fieldConfigurationDao.save(fieldConfiguration);
	        }	
		 logger.info("FieldConfigurationServiceImpl deleteFieldConfiguration End");
		
	}

}

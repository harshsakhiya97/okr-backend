package com.weq.cms.service.field;

import java.util.List;

import com.weq.cms.model.field.FieldConfiguration;

public interface FieldConfigurationService {

	List<FieldConfiguration> getFieldConfigurationListByEntity(FieldConfiguration fieldConfiguration) throws Exception;

	FieldConfiguration saveFieldConfiguration(FieldConfiguration fieldConfiguration) throws Exception;

	List<FieldConfiguration> getFieldConfigurationList(FieldConfiguration fieldConfiguration) throws Exception;

	Integer getFieldConfigurationListLength(FieldConfiguration fieldConfiguration) throws Exception;

	void deleteFieldConfiguration(Integer fieldConfigurationId) throws Exception;
}

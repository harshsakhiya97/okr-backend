package com.weq.cms.service.system;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weq.user.module.system.model.FilterModuleMaster;
import com.weq.user.module.system.model.ModuleMaster;
import com.weq.utility.module.common.dao.GenericDao;
import com.weq.utility.module.common.model.LookupData;

@Service
public class LookupServiceImpl implements LookupService {

	@Autowired
	private GenericDao genericDao;

	private static final Logger logger = Logger.getLogger(LookupServiceImpl.class);

	@Override
	public List<FilterModuleMaster> getFilterListByModuleAndType(FilterModuleMaster filterModuleMaster)
			throws Exception {
		logger.info("LookupServiceImpl->>> getFilterListByModuleAndType Data-->");
		List<FilterModuleMaster> filterModuleMasterList = new ArrayList<FilterModuleMaster>();

		String query = " from FilterModuleMaster where moduleMaster=(select m.moduleId From ModuleMaster m"
				+ " where m.moduleCode='" + filterModuleMaster.getModuleMaster().getModuleCode()
				+ "' and isDelete='N' ) " + " and filterType='" + filterModuleMaster.getFilterType()
				+ "' and isDelete='N' order by filterSequenceOrder asc";

		filterModuleMasterList = (List<FilterModuleMaster>) genericDao.findCustomList(query);
		// genericDaoImpl.f

		for (FilterModuleMaster filterModuleMasterNew : filterModuleMasterList) {
			List<Object> lookupDataList=null;
			if (filterModuleMasterNew.getLookupJson() != null) {
				String lookupJson = filterModuleMasterNew.getLookupJson();

				ObjectMapper mapper = new ObjectMapper();
				lookupDataList = mapper.readValue(lookupJson, new TypeReference<List<LookupData>>() {
				});
				filterModuleMasterNew.setFilterLookupList(lookupDataList);
			}else if(filterModuleMasterNew.getLookupQuery() != null && !filterModuleMasterNew.getLookupQuery().equalsIgnoreCase("")) {
				String lookupQuery = filterModuleMasterNew.getLookupQuery();
				
				if (filterModuleMasterNew.getLookupCriteria1() != null
						&& !filterModuleMasterNew.getLookupCriteria1().equalsIgnoreCase("")) {
					
					filterModuleMasterNew.setFilterLookupList(null);
				}else {
					lookupDataList = genericDao.findCustomListUnique(lookupQuery);
					filterModuleMasterNew.setFilterLookupList(lookupDataList);
				}
				
			}

		}

		// genericDaoImpl.findCustomList(query)

		logger.info("LookupServiceImpl->>> getFilterListByModuleAndType Data end -->");

		return filterModuleMasterList;
	}

	@Override
	public List<?> filterLookupDataBySearchValue(FilterModuleMaster filterModuleMaster) throws Exception {
		logger.info("LookupServiceImpl->>> filterLookupDataBySearchValue Data start -->");
		List<?> lookupList = null;

		if (filterModuleMaster.getLookupQuery() != null) {
			String lookupQuery = filterModuleMaster.getLookupQuery();

			if (filterModuleMaster.getLookupCriteria1() != null
					&& !filterModuleMaster.getLookupCriteria1().equalsIgnoreCase("")
					&& filterModuleMaster.getSearchValue1() != null
					&& !filterModuleMaster.getSearchValue1().equalsIgnoreCase("")) {
				lookupQuery = lookupQuery + " and " + filterModuleMaster.getLookupCriteria1();
				lookupQuery = lookupQuery.replace("?", filterModuleMaster.getSearchValue1());
			}
			lookupList = genericDao.findCustomListUnique(lookupQuery);

		}
		logger.info("LookupServiceImpl->>> filterLookupDataBySearchValue Data end -->");
		return lookupList;
	}

	@Override
	public List<?> getModuleLookupList(ModuleMaster moduleMaster) throws Exception {
		logger.info("LookupServiceImpl getModuleLookupList ");
		List<?> lookupList = null;
		String lookupQuery="from ModuleMaster where isDelete='N'";
	
		lookupList = genericDao.findCustomList(lookupQuery);

		logger.info("LookupServiceImpl->>> getModuleLookupList Data end -->");
		return lookupList;
	}
	

}

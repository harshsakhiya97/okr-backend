package com.weq.cms.service.system;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.weq.cms.dao.system.ModuleMasterDao;
import com.weq.user.module.system.model.ModuleMaster;

@Service
public class ModuleMasterServiceImpl implements ModuleMasterService {
	private static final Logger logger = Logger.getLogger(ModuleMasterServiceImpl.class);

	@Autowired
	private ModuleMasterDao moduleMasterDao;

	@Override
	public ModuleMaster saveModule(ModuleMaster moduleMaster) throws Exception {
		logger.info("ModuleMasterServiceImpl saveModule start " + moduleMaster);
		moduleMaster = moduleMasterDao.save(moduleMaster);
		logger.info("ModuleMasterServiceImpl saveModule end ");
		return moduleMaster;
	}

	@Override
	public List<ModuleMaster> getModuleList(ModuleMaster moduleMaster) throws Exception {
		logger.info("ModuleMasterServiceImpl getModuleList ");
		Specification<ModuleMaster> spec =Specification.where((root, query, cb) -> {
		    return cb.and(
			        cb.equal(root.get("isDelete"), "N"));
			});
		
		return moduleMasterDao.findAll( Specification.where(spec),PageRequest.of(moduleMaster.getPageNumber(), moduleMaster.getPageSize()))
				.getContent();
	}

	@Override
	public Integer getModuleListLength(ModuleMaster moduleMaster) throws Exception {
		logger.info("ModuleMasterServiceImpl getModuleList ");
		return moduleMasterDao.getModuleListLength(moduleMaster.getIsDelete());
	}

	@Override
	public void deleteModule(Integer moduleId) throws Exception {
		logger.info("ModuleMasterServiceImpl deleteModule Start Data-->" + moduleId);
		ModuleMaster moduleMaster=moduleMasterDao.getModuleById(moduleId);
		 if(moduleMaster != null){
			 moduleMaster.setIsDelete("Y");
			 moduleMasterDao.save(moduleMaster);
	        }	
		 logger.info("ModuleMasterServiceImpl deleteModule End");
		
	}

}

package com.weq.cms.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weq.utility.module.common.dao.GenericDao;

@Component("DataUtil")
public class DataUtil {
	private static final Logger logger = Logger.getLogger(DataUtil.class);

	@Autowired
	private GenericDao genericDao;
	
	@Autowired
    private ModelMapper modelMapper;

	public  String convertListToCommaSeparated(List<?> objectList,String fieldName) throws Exception {
		String dataList="";
		
		if(objectList!=null && objectList.size() > 0) {
			for(Object obj :objectList) {
				if(obj instanceof String) {
					dataList=dataList+obj+",";
				}else {
					 Class<?> clazz = obj.getClass();
					 Field field = clazz.getDeclaredField(fieldName);
					 field.setAccessible(true);
					 Object fieldValue = field.get(obj);
					 dataList=dataList+fieldValue+",";
				}
				
			}
			if(dataList!=null && !dataList.equalsIgnoreCase("")) {
				dataList=dataList.substring(0, dataList.length()-1);
			}
		}
			
		return dataList;	
	}
	
	
	public  <T> List<T> convertCommaSeparatedToList(String fieldValue,Class<T> className,String fieldName,boolean fetchMaster) throws Exception {
		List<T> resultList =new ArrayList<T>();
		
		if(fieldValue==null || fieldValue.equalsIgnoreCase("")) {
			return resultList;
		}
		
		
		List<String> dataList = Arrays.asList(fieldValue.split("\\s*,\\s*"));
		
		
		
		for(String value:dataList) {
			if(value!=null && ! value.equalsIgnoreCase("")) {
				Class<T> classObj = className;
				T obj = classObj.getConstructor().newInstance();
				Field field = classObj.getDeclaredField(fieldName);
				field.setAccessible(true);
							
				if(obj instanceof String) {
					field.set(obj, value);
				}else {
					if(fetchMaster) {
						field.set(obj, Integer.parseInt(value));
						obj=(T) genericDao.findOneInt(Integer.parseInt(value), classObj);	
					}else {
						field.set(obj, value);
					}
					
					
				}
				
				
				if(obj!=null) {
					resultList.add(obj);
				}
				
				
			}
		
			
			
		}
		
		
		return resultList;	
	}
	

	public void sendFCMNotification(String tokenId, String title, String message) {
		String server_key = "AIzaSyBMmwdulhk3gxwuNRQI_gZ_si6ic6ebYuU";
		System.out.println("server_key :" + server_key);
		System.out.println("tokenId :" + tokenId);
		String FCM_URL = "https://fcm.googleapis.com/fcm/send";
		try {
			// Create URL instance.
			URL url = new URL(FCM_URL);
			// create connection.
			HttpURLConnection conn;
			conn = (HttpURLConnection) url.openConnection();
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			// set method as POST or GET
			conn.setRequestMethod("POST");
			// pass FCM server key
			conn.setRequestProperty("Authorization", "key=" + server_key);
			// Specify Message Format
			conn.setRequestProperty("Content-Type", "application/json");
			// Create JSON Object & pass value
			JSONObject infoJson = new JSONObject();

			infoJson.put("title", title);
			infoJson.put("body", message);

			JSONObject json = new JSONObject();
			json.put("to", tokenId.trim());
			// json.put("registration_ids",tokenId);
			json.put("data", infoJson);

			System.out.println("json :" + json.toString());
			System.out.println("infoJson :" + infoJson.toString());
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(json.toString());
			wr.flush();
			int status = 0;
			if (null != conn) {
				status = conn.getResponseCode();
			}
			System.out.println("status=" + status);
			if (status != 0) {

				if (status == 200) {
					// SUCCESS message
					BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					System.out.println("Android Notification Response : " + reader.readLine());
				} else if (status == 400) {
					// client side error
					System.out.println(
							"Notification Response : [ Syntax Error ]TokenId : " + tokenId + " Error occurred :");
				} else if (status == 401) {
					// client side error
					System.out.println("Notification Response : TokenId : " + tokenId + " Error occurred :");
				} else if (status == 501) {
					// server side error
					System.out.println("Notification Response : [ errorCode=ServerError ] TokenId : " + tokenId);
				} else if (status == 503) {
					// server side error
					System.out.println("Notification Response : FCM Service is Unavailable TokenId : " + tokenId);
				}
			}
		} catch (MalformedURLException mlfexception) {
			// Prototcal Error
			System.out.println("Error occurred while sending push Notification!.." + mlfexception.getMessage());
		} catch (Exception mlfexception) {
			// URL problem
			System.out.println(
					"Reading URL, Error occurred while sending push Notification!.." + mlfexception.getMessage());
		}
	}
		

	public <T> String objectToJson( T object) throws Exception {
		ObjectMapper mapper = new ObjectMapper(); 
		String json = mapper.writeValueAsString(object); 		
		return json;
	}
	
	public <T> T jsonToObject(String json,Class<T> className) throws Exception {
		ObjectMapper mapper = new ObjectMapper(); 
		Class<T> classObj = className;
		T obj = classObj.getConstructor().newInstance();
		
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		obj = mapper.readValue(json, classObj);	
		
		return obj;
	}
	
	public String newLineToCommaSeparated(String textValue) throws Exception {
		 String commaSeparatedValue="";
	 	if(textValue!=null)
	 	{
		    String [] textValueList=textValue.split("\n");
		    
		    for(String separatedValue : textValueList)
		    {
		    	if(separatedValue!="" || separatedValue!=null || separatedValue!=" ")
		    	{
		    		commaSeparatedValue=commaSeparatedValue+"'"+separatedValue+"',";
		
		    	}
		    }
		    
		    if(commaSeparatedValue!=null && !commaSeparatedValue.equalsIgnoreCase("")) {
		    	commaSeparatedValue=commaSeparatedValue.substring(0, commaSeparatedValue.length()-1);
			}
			
	 	}
	 	
	 	return commaSeparatedValue;
	}
	
	public  List<String> newLineToStringList(String textValue) throws Exception {
		 List<String> stringList=new ArrayList<String>();
	 	if(textValue!=null)
	 	{
		    String [] textValueList=textValue.split("\n");
		    
		    for(String separatedValue : textValueList)
		    {
		    	if(separatedValue!="" || separatedValue!=null || separatedValue!=" ")
		    	{
		    		stringList.add(separatedValue);
		
		    	}
		    }
		    
		
			
	 	}
	 	
	 	return stringList;
	}
		
}

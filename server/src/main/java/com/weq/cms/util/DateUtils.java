package com.weq.cms.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {

	
	
	
	
	 public static Date getFirstDay(Date d) throws Exception
	    {
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(d);
	        calendar.set(Calendar.DAY_OF_MONTH, 1);
	        Date firstDay = calendar.getTime();
	   /*     SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd");
	         sdf1.format(dddd);*/
	         return firstDay;
	    }
	 
	    public static Date getLastDay(Date d) throws Exception
	    {
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(d);
	        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	        Date lastDate = calendar.getTime();
	    //    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd");
	        return lastDate;
	    }
	 
	    public static String getDate(Date d)
	    {
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM");
	        return sdf.format(d);
	    }
	 
	    public static int getLastDayOfMonth(Date date)
	    {
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(date);
	        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	    }
	    
	    // getting current month from a date
	    public static int getCurrentMonth(Date date) {
	    	Calendar calendar = Calendar.getInstance();
	    	calendar.setTime(date);
	    	return calendar.get(Calendar.MONTH);
	    }
	
	
}

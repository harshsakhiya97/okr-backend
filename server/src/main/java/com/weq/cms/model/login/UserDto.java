package com.weq.cms.model.login;

import lombok.Data;

@Data
public class UserDto {

    private String username;
    private String password;
}

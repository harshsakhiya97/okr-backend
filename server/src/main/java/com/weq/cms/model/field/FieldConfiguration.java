package com.weq.cms.model.field;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.weq.utility.module.common.model.LookupData;
import com.weq.utility.module.common.model.TimestampEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
public class FieldConfiguration extends TimestampEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer fieldConfigurationId;

	@Size(max = 50)
	private String entityClient;

	@Size(max = 50)
	private String entityServer;

	@Size(max = 50)
	private String fieldName;

	@Size(max = 50)
	private String fieldType;
	
	@Size(max = 50)
	private String sectionName;
	
	@Size(max = 50)
	private String sectionSubName;

	@Size(max = 50)
	private String label;

	private Integer orderNumber;

	@Size(max = 50)
	private String fieldCondition1;

	@Size(max = 50)
	private String fieldCondition2;

	@Size(max = 300)
	private String lookupQuery;
	
	@Size(max = 50)
	private String columnClass;

	@Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;

	@OneToOne(mappedBy = "fieldConfiguration", cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = true)
	private FieldValidation fieldValidation;

	@Transient
	private Integer pageNumber;

	@Transient
	private Integer pageSize;

	@Size(max = 1000)
	private String lookupJson;
	
	@Transient
	private List<LookupData> lookupDataList = new ArrayList<LookupData>();

}

package com.weq.cms.model.common;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TableDTO {

	private Integer individualBranchId;
	
	private String companyCode;
	
	private String companyName;
	
	private String companyAsappName;
	
	private String companyTrade;
	
	private String companyGroupName;
	
	private String companyForeignEquity;
	
	private String companyTurnOver;
	
	private String companyTurnYear;
	
	private String companyEmployeeNumber;
	
	private String companyLineOfBusiness;
	
	private String companyCategory;
	
	private String companyProductName;
	
	private String companyMNC;
	
	private String companyEquityStatu;
	
	private String companyEmail;
	
	private String companyWebsite;
	
	private String branchCode;
	
	private String branchType;
	
	private String branchAddress;

	private String branchCity;
	
	private String branchState;
	
	private String branchCountry;
	
	private String branchCircle;
	
	private String companySubIndustry;
	
	private String companySector;
	
	private String branchZone;
	
	private String branchTelephone;
	
	private String branchMobile;
	
	private String branchFax;
	
	private String individualCode;
	
	private String decisionMaker;
	
	private String individualFirstName;
	
	private String individualMiddleName;
	
	private String individualLastName;
	
	private String officeContact;
	
	private String officeEmail;
	
	private String individualBranchAsapp;
	
	private String individualBranchData;
	
	private String individualBranchMaster;
	
	private Date createdDate;
	
	private String createdByName;
	
	private Date modifiedDate;
	
	private String modifiedByName;
	
}

	
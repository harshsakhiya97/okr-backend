package com.weq.cms.model.properties;

import lombok.Data;

@Data
public class PropertiesDTO {

	private MasterPropertiesDTO masterPropertiesDTO;
	
}

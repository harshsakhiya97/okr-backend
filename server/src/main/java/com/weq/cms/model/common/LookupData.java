package com.weq.cms.model.common;

import lombok.Data;

@Data
public class LookupData {
	private String lookupId;
	private String lookupValue;
	
	public LookupData() {
		
	}
	
	public LookupData(Integer lookupId, String lookupValue) {
		this.lookupId = String.valueOf(lookupId);
		this.lookupValue = lookupValue;
	}
	
	public LookupData(Long lookupId, String lookupValue) {
		this.lookupId = String.valueOf(lookupId);
		this.lookupValue = lookupValue;
	}
	
}

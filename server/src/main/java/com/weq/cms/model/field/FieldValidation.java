package com.weq.cms.model.field;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.weq.utility.module.common.model.TimestampEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper=false)
public class FieldValidation extends TimestampEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer fieldValidationId;
	
	private boolean mandatoryField;
	
	private boolean checkRegex;
	
	@Size(max = 100)
	private String checkRegexMsg;
	
	@Size(max = 100)
	private String regexPattern;
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fieldConfigurationId")
	@JsonIgnore
    private FieldConfiguration fieldConfiguration;
	
	/*
	private boolean checkNull; 
	
	@Size(max = 100)
	private String checkNullMsg;
	
	private boolean checkUndefined;
	
	@Size(max = 100)
	private String checkUndefinedMsg;
	
	 */
	
	
	
	
	
}

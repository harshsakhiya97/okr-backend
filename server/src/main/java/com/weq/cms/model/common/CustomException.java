package com.weq.cms.model.common;

import lombok.Data;

@Data
public class CustomException {
	private String message;
	private String code;
	private Boolean checkValidation;

	public CustomException() {
	}

	public CustomException(String message,String code) {
		this.message = message;
		this.code = code;
	}

}

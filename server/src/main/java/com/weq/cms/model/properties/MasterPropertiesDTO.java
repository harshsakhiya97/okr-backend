package com.weq.cms.model.properties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MasterPropertiesDTO {
	
	public Boolean checkIpAddress;
	public String numberFormat;
	public Double numberFormatValue;
	public Boolean checkPrefix;
	public String companyPrefix;
	public String branchPrefix;
	public String personPrefix;
	public String employeePrefix;
	

}

package com.weq.cms.controller.system;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weq.cms.model.common.CustomException;
import com.weq.cms.service.system.AccessControlMasterService;
import com.weq.user.module.system.model.AccessControlMaster;
import com.weq.user.module.user.model.User;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/accessControl")
public class AccessControlMasterController {
	private static final Logger logger = Logger.getLogger(AccessControlMasterController.class);

	@Autowired
	private AccessControlMasterService accessControlMasterService;

	@RequestMapping(value = "/saveAccessControl", method = RequestMethod.POST)
	public ResponseEntity<?> saveAccessControl(@RequestBody AccessControlMaster accessControlMaster) throws Exception {
		logger.info("AccessControlMasterController->>> saveAccessControl Data Start-->" + accessControlMaster);

		try {
			
			User user =  new User();
			user.setId(accessControlMaster.getEmployeeId());
			accessControlMaster.setUser(user);
			
			accessControlMasterService.saveAccessControl(accessControlMaster);

		} catch (Exception e) {

			logger.error("AccessControlMasterController saveAccessControl " + e.getMessage(), e);

			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("AccessControlMasterController->>> saveAccessControl Data End-->");
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/getAcessControlAllList", method = RequestMethod.POST)
	public ResponseEntity<?> getAcessControlAllList(@RequestBody AccessControlMaster accessControlMaster)
			throws Exception {
		logger.info("AccessControlMasterController->>> getAcessControlAllList Data Start-->" + accessControlMaster);
		List<AccessControlMaster> accessControlList = new ArrayList<AccessControlMaster>();
		try {
			accessControlList = accessControlMasterService.getAcessControlAllList(accessControlMaster);

		} catch (Exception e) {

			logger.error("AccessControlMasterController getAcessControlAllList " + e.getMessage(), e);

			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("AccessControlMasterController->>> getAcessControlAllList Data End-->");
		return ResponseEntity.ok(accessControlList);
	}

	@RequestMapping(value = "/getAcessControlAllListLength", method = RequestMethod.POST)
	public ResponseEntity<?> getAcessControlAllListLength(@RequestBody AccessControlMaster accessControlMaster)
			throws Exception {
		logger.info("ModuleMasterController->>> getAcessControlAllListLength Data Start-->" + accessControlMaster);
		Integer count = 0;
		try {
			count = accessControlMasterService.getAcessControlAllListLength(accessControlMaster);

		} catch (Exception e) {

			logger.error("AccessControlMasterController getAcessControlAllListLength " + e.getMessage(), e);

			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("AccessControlMasterController->>> getAcessControlAllListLength Data End-->");
		return ResponseEntity.ok(count);
	}

	@RequestMapping(value = "/deleteAccessControl/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteAccessControl(@PathVariable("id") Integer accessControlId) throws Exception {
		logger.info("AccessControlMasterController->>> deleteAccessControl Data--> " + accessControlId);
		try {
			accessControlMasterService.deleteAccessControl(accessControlId);
		} catch (Exception e) {
			logger.error("AccessControlMasterController deleteAccessControl" + e.getMessage(), e);

			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("AccessControlMasterController->>> deleteAccessControl end--> ");
		return ResponseEntity.ok().build();
	}

	
	@RequestMapping(value = "/getAcessControlListByRole", method = RequestMethod.POST)
	public ResponseEntity<?> getAcessControlListByRole(@RequestBody User user)
			throws Exception {
		logger.info("AccessControlMasterController->>> getAcessControlListByRole Start-->" );
		List<AccessControlMaster> accessControlList = new ArrayList<AccessControlMaster>();
		try {
			accessControlList = accessControlMasterService.getAcessControlListByRole(user);

		} catch (Exception e) {

			logger.error("AccessControlMasterController getAcessControlListByRole " + e.getMessage(), e);

			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("AccessControlMasterController->>> getAcessControlListByRole End-->");
		return ResponseEntity.ok(accessControlList);
	}
	
}

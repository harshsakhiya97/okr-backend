/**
 * 
 */
package com.weq.cms.controller.login;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weq.user.module.user.model.Role;
import com.weq.user.module.user.service.RoleService;

/**
 * @author PC
 *
 */

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/role")
public class RoleController {

	private static final Logger logger = Logger.getLogger(RoleController.class);

	@Autowired
	private RoleService roleService;

	@RequestMapping(value = "/getRoleList", method = RequestMethod.GET)
	public ResponseEntity<?> getRoleList() throws Exception {
		logger.info("RoleController->>> getRoleList Data Start ");
		List<Role> roleMasterList = new ArrayList<Role>();
		try {
			roleMasterList = roleService.getRoleList();
		} catch (Exception e) {
			logger.error("RoleController getRoleList " + e.getMessage(), e);
			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("RoleController->>> getRoleList Data End-->");
		return ResponseEntity.ok(roleMasterList);
	}
	
	@RequestMapping(value = "/getUserRoleList", method = RequestMethod.GET)
	public ResponseEntity<?> getUserRoleList() throws Exception {
		logger.info("RoleController->>> getUserRoleList Data Start ");
		List<Role> roleMasterList = new ArrayList<Role>();
		try {
			roleMasterList = roleService.getUserRoleList();
		} catch (Exception e) {
			logger.error("RoleController getUserRoleList " + e.getMessage(), e);
			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("RoleController->>> getUserRoleList Data End-->");
		return ResponseEntity.ok(roleMasterList);
	}

}

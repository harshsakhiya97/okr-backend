package com.weq.cms.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weq.cms.model.common.CustomException;
import com.weq.cms.model.field.FieldConfiguration;
import com.weq.cms.service.field.FieldConfigurationService;
//import com.weq.communication.model.email.Mail;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/fieldConfiguration")
public class FieldConfigurationController {

	private static final Logger logger = Logger.getLogger(FieldConfigurationController.class);

	@Autowired
	private FieldConfigurationService fieldConfigurationService;

	@RequestMapping(value = "/getFieldConfigurationListByEntity", method = RequestMethod.POST)
	public ResponseEntity<?> getFieldConfigurationListByEntity(@RequestBody FieldConfiguration fieldConfiguration)
			throws Exception {
		logger.info("FieldConfigurationController->>> getFieldConfigurationListByEntity Data start-->");
		List<FieldConfiguration> fieldConfigurationList = new ArrayList<FieldConfiguration>();

		try {
			fieldConfigurationList = fieldConfigurationService
					.getFieldConfigurationListByEntity(fieldConfiguration);
		} catch (Exception e) {
			logger.error("FieldConfigurationController ->>> getFieldConfigurationListByEntity " + e.getMessage(), e);
			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		logger.info("FieldConfigurationController->>> getFieldConfigurationListByEntity Data end-->");
		return ResponseEntity.ok(fieldConfigurationList);
	}

	@RequestMapping(value = "/saveFieldConfiguration", method = RequestMethod.POST)
	public ResponseEntity<?> saveFieldConfiguration(@RequestBody FieldConfiguration fieldConfiguration)
			throws Exception {
		logger.info("FieldConfigurationController->>> saveFieldConfiguration Data Start-->" + fieldConfiguration);

		try {
			fieldConfigurationService.saveFieldConfiguration(fieldConfiguration);

		} catch (Exception e) {

			logger.error("FieldConfigurationController saveFieldConfiguration " + e.getMessage(), e);

			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("FieldConfigurationController->>> saveFieldConfiguration Data End-->");
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/getFieldConfigurationList", method = RequestMethod.POST)
	public ResponseEntity<?> getFieldConfigurationList(@RequestBody FieldConfiguration fieldConfiguration)
			throws Exception {
		logger.info("FieldConfigurationController->>> getFieldConfigurationList Data Start-->" );
		List<FieldConfiguration> fieldConfigurationList = new ArrayList<FieldConfiguration>();
		try {
			fieldConfigurationList = fieldConfigurationService.getFieldConfigurationList(fieldConfiguration);

		} catch (Exception e) {

			logger.error("FieldConfigurationController getFieldConfigurationList " + e.getMessage(), e);

			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("FieldConfigurationController->>> getFieldConfigurationList Data End-->");
		return ResponseEntity.ok(fieldConfigurationList);
	}

	@RequestMapping(value = "/getFieldConfigurationListLength", method = RequestMethod.POST)
	public ResponseEntity<?> getFieldConfigurationListLength(@RequestBody FieldConfiguration fieldConfiguration)
			throws Exception {
		logger.info("ModuleMasterController->>> getFieldConfigurationListLength Data Start-->" );
		Integer count = 0;
		try {
			count = fieldConfigurationService.getFieldConfigurationListLength(fieldConfiguration);

		} catch (Exception e) {

			logger.error("AccessControlMasterController getFieldConfigurationListLength " + e.getMessage(), e);

			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("AccessControlMasterController->>> getFieldConfigurationListLength Data End-->");
		return ResponseEntity.ok(count);
	}

	@RequestMapping(value = "/deleteFieldConfiguration/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteFieldConfiguration(@PathVariable("id") Integer fieldConfigurationId) throws Exception {
		logger.info("AccessControlMasterController->>> deleteFieldConfiguration Data--> " + fieldConfigurationId);
		try {
			fieldConfigurationService.deleteFieldConfiguration(fieldConfigurationId);
		} catch (Exception e) {
			logger.error("AccessControlMasterController deleteFieldConfiguration" + e.getMessage(), e);

			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("AccessControlMasterController->>> deleteFieldConfiguration end--> ");
		return ResponseEntity.ok().build();
	}

}

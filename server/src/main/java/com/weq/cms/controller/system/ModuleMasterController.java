package com.weq.cms.controller.system;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weq.cms.model.common.CustomException;
import com.weq.cms.service.system.ModuleMasterService;
import com.weq.user.module.system.model.ModuleMaster;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/module")
public class ModuleMasterController {
	private static final Logger logger = Logger.getLogger(ModuleMasterController.class);

	@Autowired
	private ModuleMasterService moduleMasterService;

	@RequestMapping(value = "/saveModule", method = RequestMethod.POST)
	public ResponseEntity<?> saveModule(@RequestBody ModuleMaster moduleMaster) throws Exception {
		logger.info("ModuleMasterController->>> saveModule Data Start-->" + moduleMaster);

		try {
			moduleMasterService.saveModule(moduleMaster);

		} catch (Exception e) {

			logger.error("ModuleMasterController saveModule " + e.getMessage(), e);

			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("ModuleMasterController->>> saveModule Data End-->");
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/getModuleList", method = RequestMethod.POST)
	public ResponseEntity<?> getModuleList(@RequestBody ModuleMaster moduleMaster) throws Exception {
		logger.info("ModuleMasterController->>> getModuleList Data Start-->" + moduleMaster);
		List<ModuleMaster> moduleList = new ArrayList<ModuleMaster>();
		try {
			moduleList = moduleMasterService.getModuleList(moduleMaster);

		} catch (Exception e) {

			logger.error("ModuleMasterController getModuleList " + e.getMessage(), e);

			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("ModuleMasterController->>> getModuleList Data End-->");
		return ResponseEntity.ok(moduleList);
	}

	@RequestMapping(value = "/getModuleListLength", method = RequestMethod.POST)
	public ResponseEntity<?> getModuleListLength(@RequestBody ModuleMaster moduleMaster) throws Exception {
		logger.info("ModuleMasterController->>> getModuleListLength Data Start-->" + moduleMaster);
		Integer count = 0;
		try {
			count = moduleMasterService.getModuleListLength(moduleMaster);

		} catch (Exception e) {

			logger.error("ModuleMasterController getModuleListLength " + e.getMessage(), e);

			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("ModuleMasterController->>> getModuleListLength Data End-->");
		return ResponseEntity.ok(count);
	}

	@RequestMapping(value = "/deleteModule/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteModule(@PathVariable("id") Integer moduleId) throws Exception {
		logger.info("ModuleMasterController->>> deleteModule Data--> " + moduleId);
		try {
			moduleMasterService.deleteModule(moduleId);
		} catch (Exception e) {
			logger.error("ModuleMasterController deleteModule" + e.getMessage(), e);

			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("ModuleMasterController->>> deleteModule end--> ");
		return ResponseEntity.ok().build();
	}
}

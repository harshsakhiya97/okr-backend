package com.weq.cms.controller.system;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weq.cms.model.common.CustomException;
import com.weq.cms.service.system.LookupService;
import com.weq.user.module.system.model.FilterModuleMaster;
import com.weq.user.module.system.model.ModuleMaster;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/lookup")
public class LookupController {

	private static final Logger logger = Logger.getLogger(LookupController.class);
	
	@Autowired
	private LookupService lookupService;
	
	@RequestMapping(value = "/getFilterListByModuleAndType", method = RequestMethod.POST)
	public ResponseEntity<?> getFilterListByModuleAndType(@RequestBody FilterModuleMaster filterModuleMaster)
			throws Exception {
		logger.info("LookupController->>> getFilterListByModuleAndType Data start-->");
		List<FilterModuleMaster> filterModuleList = new ArrayList<FilterModuleMaster>();

		try {
			filterModuleList = lookupService
					.getFilterListByModuleAndType(filterModuleMaster);
		} catch (Exception e) {
			logger.error("LookupController ->>> getFilterListByModuleAndType " + e.getMessage(), e);
			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		logger.info("LookupController->>> getFilterListByModuleAndType Data end-->");
		return ResponseEntity.ok(filterModuleList);
	}
	
	
	@RequestMapping(value = "/filterLookupDataBySearchValue", method = RequestMethod.POST)
	public ResponseEntity<?> filterLookupDataBySearchValue(@RequestBody FilterModuleMaster filterModuleMaster)
			throws Exception {
		logger.info("LookupController->>> filterLookupDataBySearchValue Data start-->");
		List<?> lookupDataList = null;

		try {
			lookupDataList = lookupService
					.filterLookupDataBySearchValue(filterModuleMaster);
		} catch (Exception e) {
			logger.error("LookupController ->>> filterLookupDataBySearchValue " + e.getMessage(), e);
			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		logger.info("LookupController->>> filterLookupDataBySearchValue Data end-->");
		return ResponseEntity.ok(lookupDataList);
	}
		
	
	@RequestMapping(value = "/getModuleLookupList", method = RequestMethod.POST)
	public ResponseEntity<?> getModuleLookupList(@RequestBody ModuleMaster moduleMaster)
			throws Exception {
		logger.info("LookupController->>> getModuleLookupList Data start-->");
		List<?> lookupDataList = null;

		try {
			lookupDataList = lookupService
					.getModuleLookupList(moduleMaster);
		} catch (Exception e) {
			logger.error("LookupController ->>> getModuleLookupList " + e.getMessage(), e);
			return new ResponseEntity<>(new CustomException("Something Went Wrong", "ERROR"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		logger.info("LookupController->>> getModuleLookupList Data end-->");
		return ResponseEntity.ok(lookupDataList);
	}
	
	

	
}

package com.weq.cms.controller.login;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.weq.cms.configuration.properties.MasterProperties;
import com.weq.cms.model.properties.MasterPropertiesDTO;
import com.weq.cms.model.properties.PropertiesDTO;
import com.weq.cms.service.system.AccessControlMasterService;
import com.weq.communication.service.email.EmailService;
import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.user.module.system.model.AccessControlMaster;
import com.weq.user.module.user.model.User;
import com.weq.user.module.user.service.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {
	private static final Logger logger = Logger.getLogger(UserController.class);

	@Autowired
	private UserService userService;
	
	@Autowired
	private AccessControlMasterService accessControlMasterService;
	
	
	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;
	
	@Autowired
	private MasterProperties masterProperties;
	
	@Autowired
	private EmailService emailService ;

	@RequestMapping(value = "/systemProperties", method = RequestMethod.POST)
	public ResponseEntity<?> systemProperties(@RequestBody User user) throws Exception {
		logger.info("UserController->>> systemProperties start-->");
		PropertiesDTO propertiesDTO=new PropertiesDTO();
		try {
			//set Master Properties
			MasterPropertiesDTO masterPropertiesDTO=new MasterPropertiesDTO();
			masterPropertiesDTO.setCheckIpAddress(masterProperties.getCheckIpAddress());
			masterPropertiesDTO.setNumberFormat(masterProperties.getNumberFormat());
			masterPropertiesDTO.setCheckPrefix(masterProperties.getCheckPrefix());
			masterPropertiesDTO.setCompanyPrefix(masterProperties.getCompanyPrefix());
			propertiesDTO.setMasterPropertiesDTO(masterPropertiesDTO);
		} catch (Exception e) {
			if (logger.isDebugEnabled() || logger.isTraceEnabled()) {
				logger.error("systemProperties" + e.getMessage(),e);
			}
			
			return new ResponseEntity<>("Something Went Wrong",HttpStatus.INTERNAL_SERVER_ERROR);
		} 
		
		logger.info("UserController->>> systemProperties end-->");
		return ResponseEntity.ok(propertiesDTO);
	}
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.PUT)
	public ResponseEntity<?> changePassword(@RequestBody User user) {
		logger.info("UserController->>> changePassword id-->" + user);
		String changeStatus = null;
		try {
			changeStatus = userService.changePassword(user);
			
			if (changeStatus != null && changeStatus.equalsIgnoreCase("SUCCESS")) {
				logger.info("UserController->>> changePassword End");
				return ResponseEntity.ok(changeStatus);
			} else {
				logger.info("UserController->>> changePassword End");
				changeStatus = "INCORRECT";
			//	return new ResponseEntity<>(changeStatus, HttpStatus.FORBIDDEN);
				return ResponseEntity.ok(changeStatus);
			}
			
		} catch (Exception e) {
				logger.error("changePassword" + e.getMessage(),e);
				return new ResponseEntity<>("ERROR",HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	
	@RequestMapping(value = "/userRegistration", method = RequestMethod.POST)
	public ResponseEntity<?> userRegistration(@RequestBody User user) throws Exception {
		logger.info("UserController->>> userRegistration  Start-->" );
		try {
			userService.userRegistration(user);	
		} catch (Exception e) {

			logger.error("UserController userRegistration Error " + e.getMessage(), e);

			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("UserController->>> userRegistration  End-->");
		return ResponseEntity.ok(user);
	}
	
	@RequestMapping(value = "/sendOtpToUser", method = RequestMethod.POST)
	public ResponseEntity<?> sendOtpToUser(@RequestBody User user) throws Exception {
		logger.info("UserController->>> sendOtpToUser  Start-->" );
		String otp="";
		try {
			otp = userService.sendOtpToUser(user);
		} catch (Exception e) {
			logger.error("UserController sendOtpToUser Error " + e.getMessage(), e);

			return new ResponseEntity<>(otp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("UserController->>> sendOtpToUser  End-->");
		return ResponseEntity.ok(otp);
	}
	
	
	@RequestMapping(value = "/checkDuplicateEmailAndMobileNo", method = RequestMethod.POST)
	public ResponseEntity<?> checkDuplicateEmailAndMobileNo(@RequestBody User user) throws Exception {
		logger.info("UserController->>> checkDuplicateEmailAndMobileNo  Start-->" );
		String duplicateCode="";
		try {
			duplicateCode = userService.checkDuplicateEmailAndMobileNo(user);	
		} catch (Exception e) {
			logger.error("UserController checkDuplicateEmailAndMobileNo Error " + e.getMessage(), e);

			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("UserController->>> checkDuplicateEmailAndMobileNo  End-->");
		return ResponseEntity.ok(duplicateCode);
	}
	
	@RequestMapping(value = "/forgetPassword", method = RequestMethod.POST)
	public ResponseEntity<?> forgetPassword(@RequestBody User user) {
		logger.info("UserController->>> forgetPassword id-->" + user);
		String changeStatus = null;
		try {
			changeStatus = userService.forgetPassword(user);
			return ResponseEntity.ok(changeStatus);
			
		/*	if (changeStatus != null && changeStatus.equalsIgnoreCase("SUCCESS")) {
				logger.info("UserController->>> forgetPassword End");
				return ResponseEntity.ok(changeStatus);
			} else {
				logger.info("UserController->>> forgetPassword End");
				changeStatus = "INCORRECT";
				return new ResponseEntity<>(changeStatus, HttpStatus.FORBIDDEN);
			}*/
			
		} catch (Exception e) {
				logger.error("forgetPassword" + e.getMessage(),e);
				return new ResponseEntity<>("ERROR",HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value = "/userProfileUpload", method = RequestMethod.POST)
	public ResponseEntity<?> userProfile(@RequestParam("file") MultipartFile file,
			@RequestParam("id") Long userId) throws Exception {
		logger.info("UserController->>> userProfile  Start-->" );
		try {
			userService.userProfile(file,userId);	
		} catch (Exception e) {
			logger.error("UserController userProfile Error " + e.getMessage(), e);

			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("UserController->>> userProfile  End-->");
		return ResponseEntity.ok("SUCCESS");
	}
	
	@RequestMapping(value = "/getUserDetailsByUsername", method = RequestMethod.POST)
	public ResponseEntity<?> getUserDetailsByUsername(@RequestBody User user) {
		logger.info("UserController->>> getUserDetailsByUsername id-->" + user);
		try {
			user = userService.validateUser(user.getUsername());
			user.setAccessControlMasterList(accessControlMasterService.getAccessControlList(user));
			
		} catch (Exception e) {
				logger.error("forgetPassword" + e.getMessage(),e);
				return new ResponseEntity<>("ERROR",HttpStatus.INTERNAL_SERVER_ERROR);
		} 
		
		return ResponseEntity.ok(user);
	}
	
	@RequestMapping(value = "/getUserList", method = RequestMethod.POST)
	public ResponseEntity<?> getUserList(@RequestBody User user) throws Exception {
		List<User> userList = new ArrayList<User>();
		logger.info("UserController->>> getUserList Start-->" );
		try {		
			userList = userService.getUserList(user);
		} catch (Exception e) {
			logger.error("UserController getUserList " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("UserController->>> getUserList End-->");
		return ResponseEntity.ok(userList);
	}
	
	@RequestMapping(value = "/getUserListLength", method = RequestMethod.POST)
	public ResponseEntity<?> getUserListLength(@RequestBody User user) throws Exception {
		logger.info("UserController->>> getUserListLength Start-->" );
		Integer count=0;
		try {		
			count = userService.getUserListLength(user);
		} catch (Exception e) {
			logger.error("UserController getUserListLength " + e.getMessage(), e);
			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("UserController->>> getUserListLength Data End-->");
		return ResponseEntity.ok(count);
	}
	
	@RequestMapping(value = "/activeDeactiveUser", method = RequestMethod.POST)
	public ResponseEntity<?> activeDeactiveUser(@RequestBody  User user) throws Exception {
		logger.info("UserController->>> activeDeactiveUser Start-->" );
		try {		
			userService.activeDeactiveUser(user);
		} catch (Exception e) {
			logger.error("UserController activeDeactiveUser " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("UserController->>> activeDeactiveUser End-->");
		return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/sendNotificationToUser", method = RequestMethod.POST) 
	public ResponseEntity<?> sendNotificationToUser(@RequestBody User user) throws Exception {
		logger.info("UserController->>> sendNotificationToUser Start-->" );
		String status="";
		try {		
			status = userService.sendNotificationToUser(user);

		} catch (Exception e) {

			logger.error("UserController sendNotificationToUser " + e.getMessage(), e);

			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("UserController->>> sendNotificationToUser End-->");
		return new ResponseEntity<>(status, HttpStatus.OK);
	}	
	
	@RequestMapping(value = "/updateUserDetail", method = RequestMethod.POST)
	public ResponseEntity<?> updateUserDetail(@RequestBody  User user) throws Exception {
		logger.info("UserController->>> updateUserDetail Start-->" );
		try {		
			userService.updateUserDetail(user);
		} catch (Exception e) {
			logger.error("UserController updateUserDetail " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("UserController->>> updateUserDetail End-->");
		return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getUserById", method = RequestMethod.POST)
	public ResponseEntity<?> getUserById(@RequestBody User user) throws Exception {
		logger.info("UserController->>> getUserById Start-->" );
		try {		
			user = userService.getUserById(user);
		} catch (Exception e) {
			logger.error("UserController getUserById " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("UserController->>> getUserById End-->");
		return ResponseEntity.ok(user);
	}

	
}
package com.weq.cms.custom.module.common.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.weq.cms.util.DataUtil;
import com.weq.utility.module.common.model.PredefinedMaster;
import com.weq.utility.module.common.service.PredefinedService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/predefined")
public class PredefinedController {
	private static final Logger logger = Logger.getLogger(PredefinedController.class);

	@Autowired
	private PredefinedService predefinedService;
	
	@Autowired
	private DataUtil dataUtil;
	
	
	@RequestMapping(value = "/savePredefined", method = RequestMethod.POST)
	public ResponseEntity<?> savePredefined(@RequestBody PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedController->>> savePredefined Data Start-->" + predefinedMaster);
		try {
			predefinedService.savePredefinedMaster(predefinedMaster);
			} catch (Exception e) {
				logger.error("PredefinedController savePredefined " + e.getMessage(), e);
				return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
				}
		logger.info("PredefinedController->>> savePredefined Data End-->");
		return ResponseEntity.ok(predefinedMaster);
	}

	@RequestMapping(value = "/deletePredefinedMaster/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deletePredefinedMaster(@PathVariable("id") Integer predefinedId) throws Exception {
		logger.info("PredefinedController->>> deletePredefinedMaster Start Id--> " + predefinedId);
		try {
			predefinedService.deletePredefinedMaster(predefinedId);
			} catch (Exception e) {
				if (logger.isDebugEnabled() || logger.isTraceEnabled()) {
					logger.error("PredefinedController deletePredefinedMaster" + e.getMessage(), e);
					}
				return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
				}
		logger.info("PredefinedController->>> deletePredefinedMaster End--> ");
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/getPredefinedListByType", method = RequestMethod.POST)
	public ResponseEntity<?> getPredefinedListByType(@RequestBody PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedController->>> getPredefinedListByType Data Start-->" + predefinedMaster);
		List<PredefinedMaster> predefinedMasterList = new ArrayList<PredefinedMaster>();
		try {
			predefinedMasterList = predefinedService.getPredefinedListByType(predefinedMaster);

		} catch (Exception e) {

			logger.error("PredefinedController getPredefinedListByType " + e.getMessage(), e);

			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("PredefinedController->>> getPredefinedListByType Data End-->");
		return ResponseEntity.ok(predefinedMasterList);
	}
	
	@RequestMapping(value = "/getPredefinedListByTypeOnlyName", method = RequestMethod.POST)
	public ResponseEntity<?> getPredefinedListByTypeOnlyName(@RequestBody PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedController->>> getPredefinedListByTypeOnlyName Data Start-->" + predefinedMaster);
		List<PredefinedMaster> predefinedMasterList = new ArrayList<PredefinedMaster>();
		try {
			predefinedMasterList = predefinedService.getPredefinedListByTypeOnlyName(predefinedMaster);

		} catch (Exception e) {

			logger.error("PredefinedController getPredefinedListByTypeOnlyName " + e.getMessage(), e);

			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("PredefinedController->>> getPredefinedListByTypeOnlyName Data End-->");
		return ResponseEntity.ok(predefinedMasterList);
	}
	
	@RequestMapping(value = "/getPredefinedByNameAndSubname", method = RequestMethod.POST)
	public ResponseEntity<?> getPredefinedByNameAndSubname(@RequestBody PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedController->>> getPredefinedByNameAndSubname Data Start-->" + predefinedMaster);
		PredefinedMaster predefinedMasterList = new PredefinedMaster();
		try {
			predefinedMasterList = predefinedService.getPredefinedByNameAndSubname(predefinedMaster.getName(),predefinedMaster.getSubName(),predefinedMaster.getEntityType());

		} catch (Exception e) {

			logger.error("PredefinedController getPredefinedByNameAndSubname " + e.getMessage(), e);

			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("PredefinedController->>> getPredefinedByNameAndSubname Data End-->");
		return ResponseEntity.ok(predefinedMasterList);
	}
	
	
	@RequestMapping(value = "/getDesignationEntityListLength", method = RequestMethod.POST)
	public ResponseEntity<?> getDesignationEntityListLength(@RequestBody PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedController->>> getDesignationEntityListLength Data Start--> " + predefinedMaster);
		Integer count = 0;
		try {
			count = predefinedService.getDesignationEntityListLength(predefinedMaster);
		} catch (Exception e) {
			logger.error("PredefinedController getDesignationEntityListLength " + e.getMessage(), e);
			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("PredefinedController->>> getDesignationEntityListLength Data End-->");
		return ResponseEntity.ok(count);
	}
	
	@RequestMapping(value = "/getFilteredDesignationNames", method = RequestMethod.POST)
	public ResponseEntity<?> getFilteredDesignationNames(@RequestBody PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedController->>> getFilteredDesignationNames Data Start--> " + predefinedMaster);
		List<PredefinedMaster> filteredPredefinedList = new ArrayList<PredefinedMaster>();
		try {
			filteredPredefinedList = predefinedService.getFilteredDesignationNames(predefinedMaster);
		} catch (Exception e) {
			logger.error("PredefinedController getFilteredDesignationNames " + e.getMessage(), e);
			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("PredefinedController->>> getFilteredDesignationNames Data End-->");
		return ResponseEntity.ok(filteredPredefinedList);
	}
	
	@RequestMapping(value = "/savePredefinedWithFile", method = RequestMethod.POST)
	public ResponseEntity<?> savePredefinedWithFile(@RequestBody PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedController->>> savePredefinedWithFile Start-->" );
	//	PredefinedMaster predefinedMaster = new PredefinedMaster();
	//	predefinedMaster = dataUtil.jsonToObject(predefinedJson, PredefinedMaster.class);
		try {		
			predefinedService.savePredefinedWithFile(predefinedMaster);
		} catch (Exception e) {
			logger.error("PredefinedController savePredefinedWithFile " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("PredefinedController->>> savePredefinedWithFile End-->");
		return ResponseEntity.ok("SUCCESS");
	}
	
	@RequestMapping(value = "/getPredefinedListByEntityTypeAndType", method = RequestMethod.POST)
	public ResponseEntity<?> getPredefinedListByEntityTypeAndType(@RequestBody PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedController->>> getPredefinedListByEntityTypeAndType Data Start-->" + predefinedMaster);
		List<PredefinedMaster> predefinedMasterList = new ArrayList<PredefinedMaster>();
		try {
			predefinedMasterList = predefinedService.getPredefinedListByEntityTypeAndType(predefinedMaster);

		} catch (Exception e) {

			logger.error("PredefinedController getPredefinedListByEntityTypeAndType " + e.getMessage(), e);

			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("PredefinedController->>> getPredefinedListByEntityTypeAndType Data End-->");
		return ResponseEntity.ok(predefinedMasterList);
	}
	
	@RequestMapping(value = "/getPredefinedListByTypeSearch", method = RequestMethod.POST)
	public ResponseEntity<?> getPredefinedListByTypeSearch(@RequestBody PredefinedMaster predefinedMaster) throws Exception {
		logger.info("PredefinedController->>> getPredefinedListByTypeSearch Data Start-->" + predefinedMaster);
		List<PredefinedMaster> predefinedMasterList = new ArrayList<PredefinedMaster>();
		try {
			predefinedMasterList = predefinedService.getPredefinedListByTypeSearch(predefinedMaster);

		} catch (Exception e) {

			logger.error("PredefinedController getPredefinedListByTypeSearch " + e.getMessage(), e);

			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("PredefinedController->>> getPredefinedListByTypeSearch Data End-->");
		return ResponseEntity.ok(predefinedMasterList);
	}
	
	@RequestMapping(value = "/getDocumentPath", method = RequestMethod.POST)
	public ResponseEntity<?> getDocumentPath(@RequestParam("file") MultipartFile multipartFile) {
		logger.info("PredefinedController->>> getDocumentPath  start-->");
		PredefinedMaster predefinedMaster = new PredefinedMaster();
		try {
			File file = predefinedService.getDocumentPath(multipartFile);
			
			if(file!=null) {
				String absolutePath="";
				String filePath="";
				absolutePath = file.getAbsolutePath();
				filePath = absolutePath.substring(0, absolutePath.lastIndexOf(File.separator));
				predefinedMaster.setDocumentSelectedPath(filePath);
				predefinedMaster.setNewFileName(file.getName());
				predefinedMaster.setFileName(multipartFile.getOriginalFilename());
			}
					
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(predefinedMaster);

		}
		logger.info("PredefinedController->>> getDocumentPath  end-->");
		return ResponseEntity.ok(predefinedMaster);
	}
	

}
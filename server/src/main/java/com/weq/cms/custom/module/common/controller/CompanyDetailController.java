package com.weq.cms.custom.module.common.controller;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.user.module.company.model.CompanyDetailMaster;
import com.weq.user.module.company.service.CompanyDetailService;
import com.weq.utility.module.common.service.PredefinedService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/companyDetail")
public class CompanyDetailController {
	
	private static final Logger logger = Logger.getLogger(CompanyDetailController.class);
	
	@Autowired
	private CompanyDetailService companyDetailService;
	
	@Autowired
	private PredefinedService predefinedService;
	
	@RequestMapping(value = "/saveCompanyDetail", method = RequestMethod.POST)
	public ResponseEntity<?> saveCompanyDetail(@RequestPart(value = "files", required=false) MultipartFile multipartFile,
			@RequestPart("companyDetailMaster") CompanyDetailMaster companyDetailMaster) throws Exception {
		logger.info("PredefinedController->>> saveCompanyDetail Start-->" );
		try {
			File file = predefinedService.getDocumentPath(multipartFile);
			
			if(file!=null) {
				String absolutePath="";
				String filePath="";
				absolutePath = file.getAbsolutePath();
				filePath = absolutePath.substring(0, absolutePath.lastIndexOf(File.separator));
				companyDetailMaster.setDocumentSelectedPath(filePath);
				companyDetailMaster.setNewFileName(file.getName());
				companyDetailMaster.setFileName(multipartFile.getOriginalFilename());
			}
			companyDetailService.saveCompanyDetail(companyDetailMaster);
			} catch (Exception e) {
				logger.error("PredefinedController saveCompanyDetail " + e.getMessage(), e);
				return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
				}
		logger.info("PredefinedController->>> saveCompanyDetail End-->");
		return ResponseEntity.ok(companyDetailMaster);
	}
	
	@RequestMapping(value = "/getCompanyDetail", method = RequestMethod.POST)
	public ResponseEntity<?> getCompanyDetail(@RequestBody CompanyDetailMaster companyDetailMaster) throws Exception {
		logger.info("PredefinedController->>> getCompanyDetail Start-->" );
		try {
			companyDetailMaster = companyDetailService.getCompanyDetail(companyDetailMaster);
			} catch (Exception e) {
				logger.error("PredefinedController getCompanyDetail " + e.getMessage(), e);
				return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
				}
		logger.info("PredefinedController->>> getCompanyDetail End-->");
		return ResponseEntity.ok(companyDetailMaster);
	}

}

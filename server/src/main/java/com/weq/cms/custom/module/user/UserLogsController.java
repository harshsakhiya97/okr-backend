package com.weq.cms.custom.module.user;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.user.module.user.model.UserLoginLogs;
import com.weq.user.module.user.service.UserService;
import com.weq.utility.module.common.model.PredefinedMaster;


@CrossOrigin(origins = "*", maxAge = 360000)
@RestController
@RequestMapping("/userlog")
public class UserLogsController {
	private static final Logger logger = Logger.getLogger(UserLogsController.class);
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/saveUserLogs", method = RequestMethod.POST)
	public ResponseEntity<?> saveUserLogs(@RequestBody UserLoginLogs userLogs) throws Exception {
		logger.info("UserLogsController->>> saveUserLogs Start-->" );
		try {		
			userLogs = userService.saveUserLogs(userLogs);
		} catch (Exception e) {
			logger.error("UserLogsController saveUserLogs " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("UserLogsController->>> saveUserLogs End-->");
		return ResponseEntity.ok(userLogs);
	}
	
	@RequestMapping(value = "/getUserLogsList", method = RequestMethod.POST)
	public ResponseEntity<?> getUserLogsList(@RequestBody UserLoginLogs userLogs) throws Exception {
		logger.info("UserLogsController->>> getUserLogsList Start-->" );
		List<UserLoginLogs> userLogsList = new ArrayList<UserLoginLogs>();
		try {		
			userLogsList = userService.getUserLogsList(userLogs);
		} catch (Exception e) {
			logger.error("UserLogsController getUserLogsList " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("UserLogsController->>> getUserLogsList End-->");
		return ResponseEntity.ok(userLogsList);
	}
	
	@RequestMapping(value = "/getUserLogsLength", method = RequestMethod.POST)
	public ResponseEntity<?> getUserLogsLength(@RequestBody UserLoginLogs userLogs) throws Exception {
		logger.info("UserLogsController->>> getUserLogsLength Start-->" );
		Integer count = 0;
		try {		
			count = userService.getUserLogsLength(userLogs);
		} catch (Exception e) {
			logger.error("UserLogsController getUserLogsLength " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("UserLogsController->>> getUserLogsLength End-->");
		return ResponseEntity.ok(count);
	}

}
	
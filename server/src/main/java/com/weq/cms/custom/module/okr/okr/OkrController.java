package com.weq.cms.custom.module.okr.okr;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.okr.module.okr.service.OkrService;
import com.weq.utility.module.common.util.DataUtil;

@CrossOrigin(origins = "*", maxAge = 360000)
@RestController
@RequestMapping("/okr")
public class OkrController {
	private static final Logger logger = Logger.getLogger(OkrController.class);
	
	@Autowired
	private OkrService okrService;

	@Autowired
	private DataUtil dataUtil;

	@RequestMapping(value = "/saveOkr", method = RequestMethod.POST)
	public ResponseEntity<?> saveOkr(@RequestPart(value = "okrReportFileList", required=false) List<MultipartFile> okrReportFileList,
			@RequestPart("okrMasterJson") String okrMasterJson) throws Exception {
		logger.info("OkrController->>> saveOkr Start-->" );
		
		OkrMaster okrMaster = new OkrMaster();
		okrMaster = dataUtil.jsonToObject(okrMasterJson, OkrMaster.class);

		try {
			okrMaster = okrService.saveOkr(okrMaster,okrReportFileList);
		} catch (Exception e) {
			logger.error("OkrController saveOkr " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("OkrController->>> saveOkr End-->");
		return ResponseEntity.ok(okrMaster);
	}
	
	@RequestMapping(value = "/saveImportOkrMaster", method = RequestMethod.POST)
	public ResponseEntity<?> saveImportOkrMaster(@RequestBody List<OkrMaster> okrMasterList) throws Exception {
		logger.info("OkrController->>> saveImportOkrMaster Start-->" );
		try {
			okrMasterList = okrService.saveImportOkrMaster(okrMasterList);
		} catch (Exception e) {
			logger.error("OkrController saveImportOkrMaster " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("OkrController->>> saveImportOkrMaster End-->");
		return ResponseEntity.ok(okrMasterList);
	}

	
	@RequestMapping(value = "/deleteOkr", method = RequestMethod.POST)
	public ResponseEntity<?> deleteOkr(@RequestBody OkrMaster okrMaster) throws Exception {
		logger.info("OkrController->>> deleteOkr Start-->" );
		try {					
			if(okrMaster==null || okrMaster.getOkrMasterId()==null || okrMaster.getOkrMasterId()==0) {
				return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
			}		
			okrService.deleteOkr(okrMaster);
		} catch (Exception e) {
			logger.error("OkrController deleteOkr " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("OkrController->>> deleteOkr End-->");
		return ResponseEntity.ok("SUCCESS");
	}
	
	@RequestMapping(value = "/getOkrMasterList", method = RequestMethod.POST)
	public ResponseEntity<?> getOkrMasterList(@RequestBody OkrMaster okrMaster) throws Exception {
		List<OkrMaster> okrMasterList = new ArrayList<OkrMaster>();
		logger.info("OkrController->>> getOkrMasterList Start-->" );
		try {		
			okrMasterList = okrService.getOkrMasterList(okrMaster);
		} catch (Exception e) {
			logger.error("OkrController getOkrMasterList " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("OkrController->>> getOkrMasterList End-->");
		return ResponseEntity.ok(okrMasterList);
	}
	
	@RequestMapping(value = "/getOkrMasterListLength", method = RequestMethod.POST)
	public ResponseEntity<?> getOkrMasterListLength(@RequestBody OkrMaster okrMaster) throws Exception {
		logger.info("OkrController->>> getOkrMasterListLength Start-->" );
		Integer count=0;
		try {		
			count = okrService.getOkrMasterListLength(okrMaster);
		} catch (Exception e) {
			logger.error("OkrController getOkrMasterListLength " + e.getMessage(), e);
			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("OkrController->>> getOkrMasterListLength Data End-->");
		return ResponseEntity.ok(count);
	}
	
	@RequestMapping(value = "/activeDeactiveOkr", method = RequestMethod.POST)
	public ResponseEntity<?> activeDeactiveOkr(@RequestBody OkrMaster okrMaster) throws Exception {
		logger.info("OkrController->>> activeDeactiveOkr Start-->" );
		try {		
			okrService.activeDeactiveOkr(okrMaster);
		} catch (Exception e) {
			logger.error("OkrController activeDeactiveOkr " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("OkrController->>> activeDeactiveOkr End-->");
		return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
	}

	@RequestMapping(value = "/getOkrById", method = RequestMethod.POST)
	public ResponseEntity<?> getOkrById(@RequestBody OkrMaster okrMaster) throws Exception {
		OkrMaster okrMasterData = new OkrMaster();
		logger.info("OkrController->>> getOkrById Start-->" );
		try {		
			okrMasterData = okrService.getOkrById(okrMaster);
		} catch (Exception e) {
			logger.error("OkrController getOkrById " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("OkrController->>> getOkrById End-->");
		return ResponseEntity.ok(okrMasterData);
	}
	
}

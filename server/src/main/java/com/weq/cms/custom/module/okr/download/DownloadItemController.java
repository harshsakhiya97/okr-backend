package com.weq.cms.custom.module.okr.download;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.weq.okr.module.download.model.DownloadItem;
import com.weq.okr.module.download.service.DownloadService;

@CrossOrigin(origins = "*", maxAge = 360000)
@RestController
@RequestMapping("/downloadItem")
public class DownloadItemController {
	private static final Logger logger = Logger.getLogger(DownloadItemController.class);
	
	@Autowired
	private DownloadService downloadService;


	@RequestMapping(value = "/saveMultipleDownloadItem", method = RequestMethod.POST)
	public ResponseEntity<?> saveMultipleDownloadItem(@RequestBody DownloadItem downloadItem) throws Exception {
		logger.info("DownloadItemController->>> saveDownloadItem Start-->" );
		try {
			downloadItem = downloadService.saveMultipleDownloadItem(downloadItem);
		} catch (Exception e) {
			logger.error("DownloadItemController saveMultipleDownloadItem " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("DownloadItemController->>> saveMultipleDownloadItem End-->");
		return ResponseEntity.ok(downloadItem);
	}

	@RequestMapping(value = "/saveDownloadItem", method = RequestMethod.POST)
	public ResponseEntity<?> saveDownloadItem(@RequestBody DownloadItem downloadItem) throws Exception {
		logger.info("DownloadItemController->>> saveDownloadItem Start-->" );
		
		try {
			downloadItem = downloadService.saveDownloadItem(downloadItem);
		} catch (Exception e) {
			logger.error("DownloadItemController saveDownloadItem " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("DownloadItemController->>> saveDownloadItem End-->");
		return ResponseEntity.ok(downloadItem);
	}
	
	@RequestMapping(value = "/deleteDownloadItem", method = RequestMethod.POST)
	public ResponseEntity<?> deleteDownloadItem(@RequestBody DownloadItem downloadItem) throws Exception {
		logger.info("DownloadItemController->>> deleteDownloadItem Start-->" );
		try {					
			if(downloadItem==null || downloadItem.getDownloadItemId()==null || downloadItem.getDownloadItemId()==0) {
				return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
			}		
			downloadService.deleteDownloadItem(downloadItem);
		} catch (Exception e) {
			logger.error("DownloadItemController deleteDownloadItem " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("DownloadItemController->>> deleteDownloadItem End-->");
		return ResponseEntity.ok("SUCCESS");
	}
	
	@RequestMapping(value = "/getDownloadItemList", method = RequestMethod.POST)
	public ResponseEntity<?> getDownloadItemList(@RequestBody DownloadItem downloadItem) throws Exception {
		List<DownloadItem> downloadItemList = new ArrayList<DownloadItem>();
		logger.info("DownloadItemController->>> getDownloadItemList Start-->" );
		try {		
			downloadItemList = downloadService.getDownloadItemList(downloadItem);
		} catch (Exception e) {
			logger.error("DownloadItemController getDownloadItemList " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("DownloadItemController->>> getDownloadItemList End-->");
		return ResponseEntity.ok(downloadItemList);
	}
	
	@RequestMapping(value = "/getDownloadItemListLength", method = RequestMethod.POST)
	public ResponseEntity<?> getDownloadItemListLength(@RequestBody DownloadItem downloadItem) throws Exception {
		logger.info("DownloadItemController->>> getDownloadItemListLength Start-->" );
		Integer count=0;
		try {		
			count = downloadService.getDownloadItemListLength(downloadItem);
		} catch (Exception e) {
			logger.error("DownloadItemController getDownloadItemListLength " + e.getMessage(), e);
			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("DownloadItemController->>> getDownloadItemListLength Data End-->");
		return ResponseEntity.ok(count);
	}
	
	@RequestMapping(value = "/getDownloadItemById", method = RequestMethod.POST)
	public ResponseEntity<?> getDownloadItemById(@RequestBody DownloadItem downloadItem) throws Exception {
		DownloadItem downloadItemData = new DownloadItem();
		logger.info("DownloadItemController->>> getDownloadItemById Start-->" );
		try {		
			downloadItemData = downloadService.getDownloadItemById(downloadItem);
		} catch (Exception e) {
			logger.error("DownloadItemController getDownloadItemById " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("DownloadItemController->>> getDownloadItemById End-->");
		return ResponseEntity.ok(downloadItemData);
	}
}

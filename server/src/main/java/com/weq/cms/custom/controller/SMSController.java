package com.weq.cms.custom.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weq.communication.model.sms.SMS;
import com.weq.communication.service.sms.SMSService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/sms")
public class SMSController {
private static final Logger logger = Logger.getLogger(SMSController.class);
	
	@Autowired
	private SMSService smsService;
	
	//@Autowired
	//private SchedulerService schedulerService;

	@RequestMapping(value = "/sendDirecSMS",method = RequestMethod.POST)
	public ResponseEntity<?> sendDirecSMS(@RequestBody SMS sms) throws Exception {
		logger.info("SMSController->>> sendDirecSMS Data start-->");
    	smsService.sendSMS(sms);
		logger.info("SMSController->>> sendDirecSMS Data end-->");
		return ResponseEntity.ok(sms);
	}
	
/*	@RequestMapping(value = "/scheduleSMS",method = RequestMethod.POST)
	 public ResponseEntity<?> scheduleSMS(@RequestBody SMS sms) {
	        try {	            	            
	        	SchedulerJobInfo schedulerJobInfo=  schedulerService.scheduleNewSMSJob(sms);
	            ScheduleResponse scheduleResponse = new ScheduleResponse(true,
	            		schedulerJobInfo.getJobName(), schedulerJobInfo.getJobGroup(), "SMS Scheduled Successfully!");
	            return ResponseEntity.ok(scheduleResponse);
	        } catch (Exception ex) {
	            logger.error("Error scheduling sms", ex);

	            ScheduleResponse scheduleResponse = new ScheduleResponse(false,
	                    "Error scheduling email. Please try later!");
	            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(scheduleResponse);
	        }
	    }*/
	
}

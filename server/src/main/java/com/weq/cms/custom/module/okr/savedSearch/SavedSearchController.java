package com.weq.cms.custom.module.okr.savedSearch;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.okr.module.savedSearch.model.SavedSearchMaster;
import com.weq.okr.module.savedSearch.service.SavedSearchService;

@CrossOrigin(origins = "*", maxAge = 360000)
@RestController
@RequestMapping("/savedSearch")
public class SavedSearchController {
	private static final Logger logger = Logger.getLogger(SavedSearchController.class);
	
	@Autowired
	private SavedSearchService savedSearchService;


	@RequestMapping(value = "/saveSavedSearch", method = RequestMethod.POST)
	public ResponseEntity<?> saveSavedSearch(@RequestBody SavedSearchMaster savedSearchMaster) throws Exception {
		logger.info("SavedSearchController->>> saveSavedSearch Start-->" );
		
		try {
			savedSearchMaster = savedSearchService.saveSavedSearch(savedSearchMaster);
		} catch (Exception e) {
			logger.error("SavedSearchController saveSavedSearch " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("SavedSearchController->>> saveSavedSearch End-->");
		return ResponseEntity.ok(savedSearchMaster);
	}
	
	@RequestMapping(value = "/deleteSavedSearch", method = RequestMethod.POST)
	public ResponseEntity<?> deleteSavedSearch(@RequestBody SavedSearchMaster savedSearchMaster) throws Exception {
		logger.info("SavedSearchController->>> deleteSavedSearch Start-->" );
		try {					
			if(savedSearchMaster==null || savedSearchMaster.getSavedSearchMasterId()==null || savedSearchMaster.getSavedSearchMasterId()==0) {
				return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
			}		
			savedSearchService.deleteSavedSearch(savedSearchMaster);
		} catch (Exception e) {
			logger.error("SavedSearchController deleteSavedSearch " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("SavedSearchController->>> deleteSavedSearch End-->");
		return ResponseEntity.ok("SUCCESS");
	}
	

	@RequestMapping(value = "/getSavedSearchMasterList", method = RequestMethod.POST)
	public ResponseEntity<?> getSavedSearchMasterList(@RequestBody SavedSearchMaster savedSearchMaster) throws Exception {
		List<SavedSearchMaster> savedSearchMasterList = new ArrayList<SavedSearchMaster>();
		logger.info("SavedSearchController->>> getSavedSearchMasterList Start-->" );
		try {		
			savedSearchMasterList = savedSearchService.getSavedSearchMasterList(savedSearchMaster);
		} catch (Exception e) {
			logger.error("SavedSearchController getSavedSearchMasterList " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("SavedSearchController->>> getSavedSearchMasterList End-->");
		return ResponseEntity.ok(savedSearchMasterList);
	}

	@RequestMapping(value = "/getSavedSearchMasterListLength", method = RequestMethod.POST)
	public ResponseEntity<?> getSavedSearchMasterListLength(@RequestBody SavedSearchMaster savedSearchMaster) throws Exception {
		logger.info("SavedSearchController->>> getSavedSearchMasterListLength Start-->" );
		Integer count=0;
		try {		
			count = savedSearchService.getSavedSearchMasterListLength(savedSearchMaster);
		} catch (Exception e) {
			logger.error("SavedSearchController getSavedSearchMasterListLength " + e.getMessage(), e);
			return new ResponseEntity<>("Something Went Wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("SavedSearchController->>> getSavedSearchMasterListLength Data End-->");
		return ResponseEntity.ok(count);
	}
}

package com.weq.cms.custom.module.okr.bookmark;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weq.okr.module.bookmark.model.BookmarkMaster;
import com.weq.okr.module.bookmark.service.BookmarkService;

@CrossOrigin(origins = "*", maxAge = 360000)
@RestController
@RequestMapping("/bookmark")
public class BookmarkController {
	private static final Logger logger = Logger.getLogger(BookmarkController.class);
	
	@Autowired
	private BookmarkService bookmarkService;
	
	@RequestMapping(value = "/saveBookmark", method = RequestMethod.POST)
	public ResponseEntity<?> saveLikes(@RequestBody BookmarkMaster bookmarkMaster) throws Exception {
		logger.info("BookmarkController->>> saveBookmark Start-->" );
		try {		
			bookmarkMaster = bookmarkService.saveBookmark(bookmarkMaster);
		} catch (Exception e) {
			logger.error("BookmarkController saveBookmark " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("BookmarkController->>> saveBookmark End-->");
		return ResponseEntity.ok(bookmarkMaster);
	}
	
	@RequestMapping(value = "/deleteBookmark", method = RequestMethod.POST)
	public ResponseEntity<?> deleteBookmark(@RequestBody BookmarkMaster bookmarkMaster) throws Exception {
		logger.info("BookmarkController->>> deleteBookmark Start-->" );
		try {					
			if(bookmarkMaster==null || bookmarkMaster.getBookmarkId()==null || bookmarkMaster.getBookmarkId()==0) {
				return new ResponseEntity<>("UNSUCCESS", HttpStatus.INTERNAL_SERVER_ERROR);
			}		
			bookmarkService.deleteBookmark(bookmarkMaster);
		} catch (Exception e) {
			logger.error("BookmarkController deleteBookmark " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("BookmarkController->>> deleteBookmark End-->");
		return ResponseEntity.ok("SUCCESS");
	}
	
	@RequestMapping(value = "/getBookmarkListByUser", method = RequestMethod.POST)
	public ResponseEntity<?> getBookmarkListByUser(@RequestBody BookmarkMaster bookmarkMaster) throws Exception {
		List<BookmarkMaster> bookmarkMasterList = new ArrayList<BookmarkMaster>();
		logger.info("BookmarkController->>> getBookmarkListByUser Start-->" );
		try {		
			bookmarkMasterList = bookmarkService.getBookmarkListByUser(bookmarkMaster);
		} catch (Exception e) {
			logger.error("BookmarkController getBookmarkListByUser " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("BookmarkController->>> getBookmarkListByUser End-->");
		return ResponseEntity.ok(bookmarkMasterList);
	}

}

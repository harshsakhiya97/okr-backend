package com.weq.cms.custom.module.okr.notification;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weq.okr.module.notification.model.NotificationMaster;
import com.weq.okr.module.notification.service.NotificationService;

@CrossOrigin(origins = "*", maxAge = 360000)
@RestController
@RequestMapping("/notification")
public class NotificationController {
	
	private static final Logger logger = Logger.getLogger(NotificationController.class);
	
	@Autowired
	private NotificationService notificationService;
	
	@RequestMapping(value = "/saveNotification", method = RequestMethod.POST)
	public ResponseEntity<?> saveNotification(@RequestBody NotificationMaster notificationMaster) throws Exception {
		logger.info("NotificationController->>> saveNotification Start-->" );
		try {	
			notificationMaster = notificationService.saveNotification(notificationMaster);
		} catch (Exception e) {
			logger.error("NotificationController saveNotification " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("NotificationController->>> saveNotification End-->");
		return ResponseEntity.ok(notificationMaster);
	}
	
	@RequestMapping(value = "/getNotificationListByUser", method = RequestMethod.POST)
	public ResponseEntity<?> getNotificationListByUser(@RequestBody NotificationMaster notificationMaster) throws Exception {
		List<NotificationMaster> notificationMasterList = new ArrayList<NotificationMaster>();
		logger.info("NotificationController->>> getNotificationListByUser Start-->" );
		try {		
			notificationMasterList = notificationService.getNotificationListByUser(notificationMaster);
		} catch (Exception e) {
			logger.error("NotificationController getNotificationListByUser " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("NotificationController->>> getNotificationListByUser End-->");
		return ResponseEntity.ok(notificationMasterList);
	}
	
	@RequestMapping(value = "/updateNotification", method = RequestMethod.POST)
	public ResponseEntity<?> updateNotification(@RequestBody NotificationMaster notificationMaster) throws Exception {
		logger.info("NotificationController->>> updateNotification Start-->" );
		String success = "failure";
		try {		
			notificationService.updateNotificationListByUser(notificationMaster);
			success = "success";
		} catch (Exception e) {
			logger.error("NotificationController updateNotification " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("NotificationController->>> updateNotification End-->");
		return ResponseEntity.ok(success);
	}
	
	@RequestMapping(value = "/getUnseenNotificationCountByUser", method = RequestMethod.POST)
	public ResponseEntity<?> getUnseenNotificationCountByUser(@RequestBody NotificationMaster notificationMaster) throws Exception {
		//List<NotificationMaster> notificationMasterList = new ArrayList<NotificationMaster>();
		Integer count;
		logger.info("NotificationController->>> getUnseenNotificationCountByUser Start-->" );
		try {		
			count = notificationService.getUnseenNotificationCountByUser(notificationMaster);
		} catch (Exception e) {
			logger.error("NotificationController getUnseenNotificationCountByUser " + e.getMessage(), e);
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("NotificationController->>> getUnseenNotificationCountByUser End-->");
		return ResponseEntity.ok(count);
	}
	
}

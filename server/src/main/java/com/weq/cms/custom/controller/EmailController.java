package com.weq.cms.custom.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/email")
public class EmailController {
	private static final Logger logger = Logger.getLogger(EmailController.class);
	
	//@Autowired
	//private EmailService emailService;
	
	//@Autowired
	//private SchedulerService schedulerService;
/*	
	@RequestMapping(value = "/sendDirectEmail",method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<?> sendDirectEmail(@RequestPart(value = "mail", required=false) String mailJson,
			@RequestPart(value = "attachments", required=false) List<MultipartFile> mfileAttachments) throws Exception {
		logger.info("EmailController->>> sendDirectEmail Data start-->");
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		UserMail mail=mapper.readValue(mailJson, UserMail.class);
		mail.setMfileAttachments(mfileAttachments);
		mail.setFrom("ruchit@weqtechnologies.com");
		emailService.sendDirectEmail(mail);
		logger.info("EmailController->>> sendDirectEmail Data end-->");
		return ResponseEntity.ok(mail);
	}
	
	
	/*@RequestMapping(value = "/sendTemplateEmail",method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<?> sendTemplateEmail(@RequestPart(value = "mail", required=false) Mail mail,
			@RequestPart(value = "attachments", required=false) List<MultipartFile> attachments) throws Exception {
		logger.info("EmailController->>> sendTemplateEmail Data start-->");
		emailService.sendTemplateEmail(mail, attachments);
		logger.info("EmailController->>> sendTemplateEmail Data end-->");
		return ResponseEntity.ok(mail);
	}*/
	
	/*@RequestMapping(value = "/scheduleEmail",method = RequestMethod.POST, consumes = "multipart/form-data")
	 public ResponseEntity<?> scheduleEmail(@RequestPart(value = "mail", required=false) String mailJson,
				@RequestPart(value = "attachments", required=false) List<MultipartFile> attachments) {
	        try {
	        	 ObjectMapper mapper = new ObjectMapper();
	        		
	        	 Mail mail=mapper.readValue(mailJson, Mail.class);
	        	mail.setMfileAttachments(attachments);
	            	            
	        	SchedulerJobInfo schedulerJobInfo=  schedulerService.scheduleNewEmailJob(mail);
	            ScheduleResponse scheduleResponse = new ScheduleResponse(true,
	            		schedulerJobInfo.getJobName(), schedulerJobInfo.getJobGroup(), "Email Scheduled Successfully!");
	            return ResponseEntity.ok(scheduleResponse);
	        } catch (Exception ex) {
	            logger.error("Error scheduling email", ex);

	            ScheduleResponse scheduleResponse = new ScheduleResponse(false,
	                    "Error scheduling email. Please try later!");
	            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(scheduleResponse);
	        }
	    }
*/
	
}

package com.weq.cms.configuration.properties;

import java.text.DecimalFormat;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;


@Configuration
@Data
public class MasterProperties {
	
	@Value("${master.checkIpAddress}")
	public Boolean checkIpAddress;
	
	@Value("${master.numberFormat}")
	public String numberFormat;
	
	@Value("${master.checkPrefix}")
	public Boolean checkPrefix;
	
	@Value("${master.companyPrefix}")
	public String companyPrefix;
	
	@Value("${master.branchPrefix}")
	public String branchPrefix;
	
	@Value("${master.personPrefix}")
	public String personPrefix;
	
	@Value("${master.employeePrefix}")
	public String employeePrefix;
	
	public Double numberFormatValue;
	
	
	public Double getNumberFormatValue(Double number) {	
		DecimalFormat decimalFormat= new DecimalFormat("#####.00");
		return Double.parseDouble(decimalFormat.format(number));
	}


	
	
	
	
}

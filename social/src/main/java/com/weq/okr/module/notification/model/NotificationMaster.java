package com.weq.okr.module.notification.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.weq.user.module.user.model.User;
import com.weq.utility.module.common.model.TimestampEntity;

import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
@JsonIgnoreProperties(
value = {"hibernateLazyInitializer", "handler"})
public class NotificationMaster extends TimestampEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Integer notificationId;
		
	@Size(max = 200)
	private String title;
	
	@Size(max = 1500)
	private String message;
	
	@Size(max = 20)
	private String entityType;
	
	private Integer entityId;
	
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "createdBy")
	private User createdBy;
	
	@Column(name = "createdBy", insertable = false, updatable = false)
	private Long createdById;
	  
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "createdFor")
	private User createdFor;
	
	@Column(name = "createdFor", insertable = false, updatable = false)
	private Long createdForId;
	 
	@Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;
	 
	@Column(name = "is_seen", columnDefinition = "varchar(10) default 'N' ")
	private String isSeen;
	
	@Column(name = "is_deactive", columnDefinition = "varchar(10) default 'N' ")
	private String isDeactive;
	
	@Transient
	private String createdByName;
	
	@Transient
	private String createdForName;
	
	@Transient
	private Integer lastNotificationId;
	
	@Transient
	private Integer recordSize;
	
	public String getCreatedByName() {
		
		if(createdBy!=null && createdBy.getId()!=0) {
			createdByName = createdBy.getFirstName()!=null?createdBy.getFirstName():""+createdBy.getMiddleName()!=null?createdBy.getMiddleName():""+
					createdBy.getLastName()!=null?createdBy.getLastName():"";
		}
		
		return createdByName;
	}

	public String getCreatedForName() {
	
	if(createdFor!=null && createdFor.getId()!=0) {
		createdForName = createdFor.getFirstName()!=null?createdFor.getFirstName():""+createdFor.getMiddleName()!=null?createdFor.getMiddleName():""+
				createdFor.getLastName()!=null?createdFor.getLastName():"";
	}
	
		return createdForName;
    }

}

package com.weq.okr.module.bookmark.service;

import java.util.List;

import com.weq.okr.module.bookmark.model.BookmarkMaster;

public interface BookmarkService {

	BookmarkMaster saveBookmark(BookmarkMaster bookmarkMaster) throws Exception;

	void deleteBookmark(BookmarkMaster bookmarkMaster) throws Exception;

	List<BookmarkMaster> getBookmarkListByUser(BookmarkMaster bookmarkMaster) throws Exception;

	BookmarkMaster getBookmarkIdByUserAndOkr(BookmarkMaster bookmarkMaster) throws Exception;
}

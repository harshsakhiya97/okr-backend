package com.weq.okr.module.okr.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.weq.okr.module.content.model.ContentMaster;
import com.weq.user.module.user.model.User;
import com.weq.utility.module.common.model.LookupData;
import com.weq.utility.module.common.model.PredefinedMaster;
import com.weq.utility.module.common.model.TimestampEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@JsonIgnoreProperties(
value = {"hibernateLazyInitializer", "handler"})
@ToString(exclude = { "industry","designation","designationLevel","department","subDepartment" })
public class OkrMaster extends TimestampEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Integer okrMasterId;

	@ManyToOne(targetEntity = PredefinedMaster.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "industryId")
	private PredefinedMaster industry;
	
	@Column(name = "industryId", insertable = false, updatable = false)
	private Integer industryId;

	@ManyToOne(targetEntity = PredefinedMaster.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "designationId")
	private PredefinedMaster designation;
	
	@Column(name = "designationId", insertable = false, updatable = false)
	private Integer designationId;

	@ManyToOne(targetEntity = PredefinedMaster.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "designationLevelId")
	private PredefinedMaster designationLevel;
	
	@Column(name = "designationLevelId", insertable = false, updatable = false)
	private Integer designationLevelId;

	@ManyToOne(targetEntity = PredefinedMaster.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "departmentId")
	private PredefinedMaster department;
	
	@Column(name = "departmentId", insertable = false, updatable = false)
	private Integer departmentId;

	@ManyToOne(targetEntity = PredefinedMaster.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "subDepartmentId")
	private PredefinedMaster subDepartment;
	
	@Column(name = "subDepartmentId", insertable = false, updatable = false)
	private Integer subDepartmentId;

	@OneToOne(fetch = FetchType.EAGER,optional=true,targetEntity = ContentMaster.class)
	@JoinColumn(name = "okrReportFileId",nullable = true)
	private ContentMaster okrReportFile;

	@Column(name = "okrReportFileId", insertable = false, updatable = false)
	private Integer okrReportFileId;
	
	@Size(max = 200000)
	private String objective;
	
	@Size(max = 200000)
	private String keyResult;

	@Column(name="total_downloads", columnDefinition="int(10) default '0'")
	private Integer totalDownloads;

	@Column(name="total_accessed", columnDefinition="int(10) default '0'")
	private Integer totalAccessed;

	
	@Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;
	
	@Column(name = "is_deactive", columnDefinition = "varchar(10) default 'N' ")
	private String isDeactive;
	
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "createdBy")
	private User createdBy;
	
	@Column(name = "createdBy", insertable = false, updatable = false)
	private Long createdById;
	
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "modifiedBy")
	private User modifiedBy;
	
	@Column(name = "modifiedBy", insertable = false, updatable = false)
	private Long modifiedById;

	@Transient
	private String searchValue;

	@Transient
	private Integer lastOkrMasterId;
	
	@Transient
	private Integer recordSize;
	
	@Transient
	private Integer pageSize;
	
	@Transient
	private Integer pageNumber;

	@Transient
	private User currentUser;

	@Transient
	private Integer bookmarkId;
	
	@Transient
	private String industryNameTemp;

	@Transient
	private String designationNameTemp;

	@Transient
	private String designationLevelNameTemp;

	@Transient
	private String departmentNameTemp;

	@Transient
	private String subDepartmentNameTemp;

	@Transient
	private Integer sortOrder;

	
	public String getIndustryName() {
		
		if(industry != null && industry.getName() != null) {
			return industry.getName();
		} else {
			return "";
		}
		
	}

	public String getDesignationName() {
		
		if(designation != null && designation.getName() != null) {
			return designation.getName();
		} else {
			return "";
		}
		
	}

	public String getDesignationLevelName() {
		
		if(designationLevel != null && designationLevel.getName() != null) {
			return designationLevel.getName();
		} else {
			return "";
		}
		
	}

	public String getDepartmentName() {
		
		if(department != null && department.getName() != null) {
			return department.getName();
		} else {
			return "";
		}
		
	}

	public String getSubDepartmentName() {
		
		if(subDepartment != null && subDepartment.getName() != null) {
			return subDepartment.getName();
		} else {
			return "";
		}
		
	}


}

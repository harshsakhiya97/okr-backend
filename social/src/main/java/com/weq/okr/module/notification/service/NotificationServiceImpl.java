package com.weq.okr.module.notification.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.weq.okr.module.notification.model.NotificationMaster;
import com.weq.utility.module.common.dao.GenericDao;

@Service
@Transactional
public class NotificationServiceImpl implements NotificationService{
	private static final Logger logger = Logger.getLogger(NotificationServiceImpl.class);
	
	@Autowired
	private GenericDao genericDao;

	@Override
	public NotificationMaster saveNotification(NotificationMaster notificationMaster) throws Exception {
		logger.info("FollowerServiceImpl->>> saveFollower Start-->");
		notificationMaster.setIsDelete("N");
		notificationMaster.setIsDeactive("N");
		if(notificationMaster.getNotificationId()==null || notificationMaster.getNotificationId()==0) {
			notificationMaster = (NotificationMaster) genericDao.save(notificationMaster);
		}else {
			notificationMaster = (NotificationMaster) genericDao.update(notificationMaster);
		}		
		logger.info("FollowerServiceImpl->>> saveFollower End-->");
		return notificationMaster;
	}
	
	@Override
	public List<NotificationMaster> getNotificationListByUser(NotificationMaster notificationMaster) throws Exception {
		List<NotificationMaster> notificationMasterList = new ArrayList<NotificationMaster>();
		logger.info("PostServiceImpl->>> getPostListByUser Start-->");
		String query = "from NotificationMaster where isDelete='N' and isDeactive='N' ";
		query = getQueryByCriteria(query,notificationMaster);
		notificationMasterList = genericDao.fetchNextRecords(query,notificationMaster.getRecordSize()!=null?notificationMaster.getRecordSize():10);
		
			
		logger.info("PostServiceImpl->>> getPostListByUser End-->");
		return notificationMasterList;
	}
	
	@Override
	public void updateNotificationListByUser(NotificationMaster notificationMaster) throws Exception {
		List<NotificationMaster> notificationMasterList = new ArrayList<NotificationMaster>();
		logger.info("notifictionService->>> updateNOtificationByUSER Start-->");
		String query = "update NotificationMaster set isSeen = 'Y' where isDelete='N' and createdFor='"+notificationMaster.getCreatedFor().getId()+"' and isDeactive='N' ";
		//query = getQueryByCriteria(query,notificationMaster);
		genericDao.customUpdate(query);
		
		logger.info("notifictionService->>> updateNOtificationByUSER End-->");
		
	}
	
	
	private String getQueryByCriteria(String query, NotificationMaster notificationMaster) {
		logger.info("FollowerServiceImpl->>> getQueryByCriteria Start-->");

		
		if(notificationMaster.getCreatedFor()!=null && notificationMaster.getCreatedFor().getId()!=0) {
			query = query+ " and createdFor='"+notificationMaster.getCreatedFor().getId()+"'";
		}

		
		if(notificationMaster.getLastNotificationId()!=null && notificationMaster.getLastNotificationId()!=0) {
			query = query+ " and notificationId < "+notificationMaster.getLastNotificationId();
		}
		
		query = query+ " order by notificationId desc ";
		
		logger.info("FollowerServiceImpl->>> getQueryByCriteria End-->");
		return query;
	}
	
	@Override
	public Integer getUnseenNotificationCountByUser(NotificationMaster notificationMaster) throws Exception {
		
		logger.info("NotificationServiceImpl->>> getUnseenNotificationCountByUser Start-->");
		String query = "Select count(notificationId) from NotificationMaster where createdFor='"+notificationMaster.getCreatedFor().getId()+"' and isDelete='N' and isDeactive='N' and isSeen='N'";		
		logger.info("NotificationServiceImpl->>> getUnseenNotificationCountByUser End-->");
		
		return genericDao.countCustomList(query).intValue();

	}

}

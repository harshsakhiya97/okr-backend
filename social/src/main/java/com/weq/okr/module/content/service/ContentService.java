package com.weq.okr.module.content.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.weq.okr.module.content.model.ContentMaster;
import com.weq.okr.module.okr.model.OkrMaster;

public interface ContentService {

	String getContentMasterOptimizeQuery();

	ContentMaster getContentMasterById(ContentMaster contentMaster) throws Exception;

	List<ContentMaster> getContentMasterList(ContentMaster contentMaster) throws Exception;

	Integer getContentMasterListLength(ContentMaster contentMaster) throws Exception;

	ContentMaster saveContentMaster(ContentMaster contentMaster) throws Exception;

	void deleteContentMaster(Integer contentMasterId) throws Exception;

	ContentMaster saveContentMasterWithSingleFile(MultipartFile multipartFile, ContentMaster contentMaster) throws Exception;

	ContentMaster uploadContentMasterFile(MultipartFile file) throws Exception;

}

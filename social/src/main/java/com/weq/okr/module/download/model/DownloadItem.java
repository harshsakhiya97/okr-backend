package com.weq.okr.module.download.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.weq.okr.module.content.model.ContentMaster;
import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.user.module.user.model.User;
import com.weq.utility.module.common.model.PredefinedMaster;
import com.weq.utility.module.common.model.TimestampEntity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString(exclude = { "okrMaster" })
@JsonIgnoreProperties(
value = {"hibernateLazyInitializer", "handler"})
public class DownloadItem extends TimestampEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Integer downloadItemId;
	
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "userId",nullable=true)
	private User user;
	
	@Column(name = "userId", insertable = false, updatable = false)
	private Long userId;

	@ManyToOne(targetEntity = OkrMaster.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "okrMasterId",nullable=true)
	private OkrMaster okrMaster;
	
	@Column(name = "okrMasterId", insertable = false, updatable = false)
	private Integer okrMasterId;

	@Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;

	@Transient
	private List<OkrMaster> okrMasterListTemp = new ArrayList<OkrMaster>();

	@Transient
	private String errorMessage;

	@Transient
	private Integer pageSize;
	
	@Transient
	private Integer pageNumber;
	
	@Transient
	private Integer lastDownloadItemId;
	
	@Transient
	private Integer recordSize;
	
}

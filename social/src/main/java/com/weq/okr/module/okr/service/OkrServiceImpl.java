package com.weq.okr.module.okr.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.weq.okr.module.bookmark.model.BookmarkMaster;
import com.weq.okr.module.bookmark.service.BookmarkService;
import com.weq.okr.module.content.model.ContentMaster;
import com.weq.okr.module.content.service.ContentService;
import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.user.module.user.model.User;
import com.weq.utility.module.common.dao.GenericDao;
import com.weq.utility.module.common.service.PredefinedService;

@Service
@Transactional
public class OkrServiceImpl implements OkrService{
	private static final Logger logger = Logger.getLogger(OkrServiceImpl.class);
	
	@Autowired
	private GenericDao genericDao;

	@Autowired
	private ContentService contentService;

	@Autowired
	private BookmarkService bookmarkService;

	@Autowired
	private PredefinedService predefinedService;

	@Override
	public OkrMaster saveOkr(OkrMaster okrMaster,List<MultipartFile> okrReportFileList) throws Exception {
		logger.info("OkrServiceImpl->>> saveOkr Start-->");
		OkrMaster okrMaster2 = new OkrMaster();
		
		if(okrMaster.getOkrMasterId()==null || okrMaster.getOkrMasterId()==0) {

			okrMaster.setIsDeactive("N");
			okrMaster.setIsDelete("N");
			okrMaster.setTotalAccessed(0);
			okrMaster.setTotalDownloads(0);
			
			okrMaster2=(OkrMaster) genericDao.save(okrMaster);
		}else {
			okrMaster2=(OkrMaster) genericDao.update(okrMaster);
		}
		

		if(okrReportFileList != null && okrReportFileList.size() > 0) {

			ContentMaster contentMaster = new ContentMaster();
			contentMaster = okrMaster2.getOkrReportFile();
			
			if(contentMaster == null) {
				contentMaster = new ContentMaster();
			}
			contentMaster.setEntityType("excel");
			contentMaster = contentService.saveContentMasterWithSingleFile(okrReportFileList.get(0), contentMaster);

			okrMaster2.setOkrReportFile(contentMaster);
			okrMaster2 = (OkrMaster) genericDao.update(okrMaster2);

		}
		
		logger.info("OkrServiceImpl->>> saveOkr End-->");
		return okrMaster2;
	}
	
	@Override
	public List<OkrMaster> saveImportOkrMaster(List<OkrMaster> okrMasterList) throws Exception {
		logger.info("OkrServiceImpl->>> saveImportOkrMaster Start-->");
		
		List<OkrMaster> okrMasterListTemp = new ArrayList<OkrMaster>();
		for (OkrMaster okrMaster : okrMasterList) {
			
			OkrMaster okrMasterTemp = new OkrMaster();
			okrMasterTemp.setObjective(okrMaster.getObjective());
			okrMasterTemp.setKeyResult(okrMaster.getKeyResult());
			okrMasterTemp.setIndustry(predefinedService.getPredefinedWithSaveByName(okrMaster.getIndustryNameTemp(), "INDUSTRY"));
			okrMasterTemp.setDesignation(predefinedService.getPredefinedWithSaveByName(okrMaster.getDesignationNameTemp(), "DESIGNATION"));
			okrMasterTemp.setDesignationLevel(predefinedService.getPredefinedWithSaveByName(okrMaster.getDesignationLevelNameTemp(), "DESIGNATION-LEVEL"));
			okrMasterTemp.setDepartment(predefinedService.getPredefinedWithSaveByName(okrMaster.getDepartmentNameTemp(), "DEPARTMENT"));
			okrMasterTemp.setSubDepartment(predefinedService.getPredefinedWithSaveByNameChild(okrMaster.getSubDepartmentNameTemp(), okrMaster.getDepartmentNameTemp(), "SUB-DEPARTMENT"));

			okrMasterTemp.setIsDeactive("N");
			okrMasterTemp.setIsDelete("N");
			okrMasterTemp.setTotalAccessed(0);
			okrMasterTemp.setTotalDownloads(0);
			
			okrMasterTemp=(OkrMaster) genericDao.save(okrMasterTemp);
			
			okrMasterListTemp.add(okrMasterTemp);
			
		}
		
		logger.info("OkrServiceImpl->>> saveImportOkrMaster End-->");
		return okrMasterListTemp;
	}
	
	@Override
	public void deleteOkr(OkrMaster okrMaster) throws Exception {
		logger.info("OkrServiceImpl->>> deleteOkr Start-->");
		String query = "Update OkrMaster set isDelete='Y',modifiedDate=CURRENT_TIMESTAMP where okrMasterId='"+okrMaster.getOkrMasterId()+"'  ";
		genericDao.customUpdate(query);
		logger.info("OkrServiceImpl->>> deleteOkr End-->");
	}

	@Override
	public List<OkrMaster> getOkrMasterList(OkrMaster okrMaster) throws Exception {
		logger.info("OkrServiceImpl->>> getOkrMasterList Start-->");
		
		List<OkrMaster> okrMasterList = new ArrayList<OkrMaster>();
		String query = " from OkrMaster om  where om.isDelete='N' ";
				
		query = getQueryByCriteria(query,okrMaster);

		if(okrMaster.getPageNumber()!=null) {
			okrMasterList = genericDao.findCustomListPaginate(query,okrMaster.getPageNumber()!=null?okrMaster.getPageNumber():0,okrMaster.getPageSize()!=null?okrMaster.getPageSize():10);
		} else if (okrMaster.getRecordSize() != null) {
			okrMasterList = genericDao.fetchNextRecords(query,okrMaster.getRecordSize()!=null?okrMaster.getRecordSize():10);
		} else {
			okrMasterList = genericDao.findCustomList(query);
		}
		
		if(okrMaster.getCurrentUser() != null) {
			
			setOkrMasterIsBookmarked(okrMaster.getCurrentUser(),okrMasterList);
			
		}

		logger.info("OkrServiceImpl->>> getOkrMasterList End-->");
		return okrMasterList;
	}
	
	public void setOkrMasterIsBookmarked(User user, List<OkrMaster> okrMasterList) throws Exception {
		
		for (OkrMaster okrMaster : okrMasterList) {
			

			BookmarkMaster bookmarkMaster = new BookmarkMaster();
			OkrMaster okrMasterTemp = new OkrMaster();
			okrMasterTemp.setOkrMasterId(okrMaster.getOkrMasterId());
			bookmarkMaster.setOkrMaster(okrMasterTemp);
			User createdBy = new User();
			createdBy.setId(user.getId());
			bookmarkMaster.setCreatedBy(createdBy);
			bookmarkMaster = bookmarkService.getBookmarkIdByUserAndOkr(bookmarkMaster);
			
			if(bookmarkMaster != null && bookmarkMaster.getBookmarkId() != null) {
				okrMaster.setBookmarkId(bookmarkMaster.getBookmarkId());
			} else {
				okrMaster.setBookmarkId(0);
			}
			
			String query = "Update OkrMaster set totalAccessed=totalAccessed+1,modifiedDate=CURRENT_TIMESTAMP where okrMasterId='"+okrMaster.getOkrMasterId()+"'  ";
			genericDao.customUpdate(query);

		}
		
		
	}
	
	
	@Override
	public Integer getOkrMasterListLength(OkrMaster okrMaster) throws Exception {
		logger.info("OkrServiceImpl->>> getOkrMasterListLength start");
		
		String query="Select count(om.okrMasterId) from OkrMaster om  where om.isDelete='N' ";	
		
		query = getQueryByCriteria(query,okrMaster);
		logger.info("OkrServiceImpl->>> getOkrMasterListLength end");	
		return genericDao.countCustomList(query).intValue();
		
	}

	private String getQueryByCriteria(String query, OkrMaster okrMaster) {
		logger.info("OkrServiceImpl->>> getQueryByCriteria Start-->");
		
		if(okrMaster.getIndustry() != null && okrMaster.getIndustry().getPredefinedId() != null ) {
			query += " and om.industryId = '"+okrMaster.getIndustry().getPredefinedId()+"' ";
		}

		if(okrMaster.getDesignation() != null && okrMaster.getDesignation().getPredefinedId() != null ) {
			query += " and om.designationId = '"+okrMaster.getDesignation().getPredefinedId()+"' ";
		}

		if(okrMaster.getDesignationLevel() != null && okrMaster.getDesignationLevel().getPredefinedId() != null ) {
			query += " and om.designationLevelId = '"+okrMaster.getDesignationLevel().getPredefinedId()+"' ";
		}

		if(okrMaster.getDepartment() != null && okrMaster.getDepartment().getPredefinedId() != null ) {
			query += " and om.departmentId = '"+okrMaster.getDepartment().getPredefinedId()+"' ";
		}

		if(okrMaster.getSubDepartment() != null && okrMaster.getSubDepartment().getPredefinedId() != null ) {
			query += " and om.subDepartmentId = '"+okrMaster.getSubDepartment().getPredefinedId()+"' ";
		}

		if(okrMaster.getIsDeactive() != null && okrMaster.getIsDeactive() != "") {
			query += " and om.isDeactive = '"+okrMaster.getIsDeactive()+"' ";
		}

		if(okrMaster.getObjective() != null && okrMaster.getObjective() != "") {
			query += " and om.objective like '%"+okrMaster.getObjective()+"%' ";
		}

		if(okrMaster.getSearchValue() != null && okrMaster.getSearchValue() != "") {
			query += " and ( om.objective like '%"+okrMaster.getSearchValue()+"%' or om.keyResult like '%"+okrMaster.getSearchValue()+"%' ) ";
		}
		
		if(okrMaster.getLastOkrMasterId() !=null && okrMaster.getLastOkrMasterId() != 0) {
			query += " and om.okrMasterId < "+okrMaster.getLastOkrMasterId();
		}
		
		if(okrMaster.getSortOrder() != null && okrMaster.getSortOrder() > 0) {
			// sortOrder ,, 1 - most downloaded , 2 - most accessed
			
			if(okrMaster.getSortOrder() == 1) {
				query += " order by om.totalDownloads desc, om.okrMasterId desc ";
			}

			if(okrMaster.getSortOrder() == 2) {
				query += " order by om.totalAccessed desc, om.okrMasterId desc ";
			}

		}



		logger.info("OkrServiceImpl->>> getQueryByCriteria End-->");
		return query;
	}

	@Override
	public void activeDeactiveOkr(OkrMaster okrMaster) throws Exception {
		logger.info("OkrServiceImpl->>> activeDeactiveOkr Start-->");
		String query = "Update OkrMaster set isDeactive='"+okrMaster.getIsDeactive()+"',modifiedDate=CURRENT_TIMESTAMP where okrMasterId='"+okrMaster.getOkrMasterId()+"'  ";
		genericDao.customUpdate(query);
		logger.info("OkrServiceImpl->>> activeDeactiveOkr End-->");
	}

	@Override
	public OkrMaster getOkrById(OkrMaster okrMaster) throws Exception {
		logger.info("OkrServiceImpl->>> getOkrById Start-->");

		okrMaster = (OkrMaster) genericDao.findOneInt(okrMaster.getOkrMasterId(), OkrMaster.class);
		
		logger.info("OkrServiceImpl->>> getOkrById End-->");
		return okrMaster;
	}


}

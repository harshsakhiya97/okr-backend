package com.weq.okr.module.download.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.weq.communication.model.email.Mail;
import com.weq.communication.service.email.EmailService;
import com.weq.okr.module.download.model.DownloadItem;
import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.okr.module.report.service.ReportService;
import com.weq.user.module.user.model.User;
import com.weq.utility.configuration.UtilityProperties;
import com.weq.utility.module.common.dao.GenericDao;

@Service
@Transactional
public class DownloadServiceImpl implements DownloadService{

	private static final Logger logger = Logger.getLogger(DownloadServiceImpl.class);
	
	@Autowired
	private GenericDao genericDao;

	@Autowired
	private UtilityProperties utilityProperties;

	@Autowired
	private ReportService reportService;

	@Autowired
	private EmailService emailService;

	@Override
	public DownloadItem saveDownloadItem(DownloadItem downloadItem) throws Exception {
		logger.info("DownloadServiceImpl->>> saveDownloadItem Start-->");
		
		if(downloadItem.getDownloadItemId()==null || downloadItem.getDownloadItemId()==0) {
			downloadItem.setIsDelete("N");
			downloadItem=(DownloadItem) genericDao.save(downloadItem);
		}else {
			downloadItem=(DownloadItem) genericDao.update(downloadItem);
		}

		logger.info("DownloadServiceImpl->>> saveDownloadItem End-->");
		return downloadItem;
		
	}

	@Override
	public void deleteDownloadItem(DownloadItem downloadItem) throws Exception {
		logger.info("DownloadServiceImpl->>> deleteDownloadItem Start-->");
		String query = "Update DownloadItem set isDelete='Y',modifiedDate=CURRENT_TIMESTAMP where downloadItemId='"+downloadItem.getDownloadItemId()+"'  ";
		genericDao.customUpdate(query);
		logger.info("DownloadServiceImpl->>> deleteDownloadItem End-->");
	}

	@Override
	public List<DownloadItem> getDownloadItemList(DownloadItem downloadItem) throws Exception {
		logger.info("DownloadServiceImpl->>> getDownloadItemList Start-->");
		
		List<DownloadItem> downloadItemList = new ArrayList<DownloadItem>();
		String query = " from DownloadItem di where di.isDelete='N' ";
				
		query = getQueryByCriteria(query,downloadItem);

		if(downloadItem.getPageNumber()!=null) {
			downloadItemList = genericDao.findCustomListPaginate(query,downloadItem.getPageNumber()!=null?downloadItem.getPageNumber():0,downloadItem.getPageSize()!=null?downloadItem.getPageSize():10);
		} else if (downloadItem.getRecordSize() != null) {
			downloadItemList = genericDao.fetchNextRecords(query,downloadItem.getRecordSize()!=null?downloadItem.getRecordSize():10);
		} else {
			downloadItemList = genericDao.findCustomList(query);
		}

		logger.info("DownloadServiceImpl->>> getDownloadItemList End-->");
		return downloadItemList;
	}
	
	@Override
	public Integer getDownloadItemListLength(DownloadItem downloadItem) throws Exception {
		logger.info("DownloadServiceImpl->>> getDownloadItemListLength start");
		
		String query="Select count(di.downloadItemId) from DownloadItem di where di.isDelete='N' ";	
		
		query = getQueryByCriteria(query,downloadItem);
		logger.info("DownloadServiceImpl->>> getDownloadItemListLength end");	
		return genericDao.countCustomList(query).intValue();
		
	}

	private String getQueryByCriteria(String query, DownloadItem downloadItem) {
		logger.info("DownloadServiceImpl->>> getQueryByCriteria Start-->");
		
		if(downloadItem.getCreatedDate() != null) {
			
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String dateAsString = df.format(downloadItem.getCreatedDate());
			query += " and di.createdDate like '"+dateAsString+"%' ";
		}

		if(downloadItem.getUser() != null && downloadItem.getUser().getId() != 0) {
			query += " and di.userId = '"+downloadItem.getUser().getId()+"' ";
		}

		if(downloadItem.getOkrMaster() != null && downloadItem.getOkrMaster().getOkrMasterId() != null) {
			query += " and di.okrMasterId = '"+downloadItem.getOkrMaster().getOkrMasterId()+"' ";
		}
		
		if(downloadItem.getLastDownloadItemId() !=null && downloadItem.getLastDownloadItemId() != 0) {
			query += " and di.downloadItemId < "+downloadItem.getLastDownloadItemId();
		}
		
		logger.info("DownloadServiceImpl->>> getQueryByCriteria End-->");
		return query;
	}

	@Override
	public DownloadItem getDownloadItemById(DownloadItem downloadItem) throws Exception {
		logger.info("DownloadServiceImpl->>> getDownloadItemById Start-->");

		downloadItem = (DownloadItem) genericDao.findOneInt(downloadItem.getDownloadItemId(), DownloadItem.class);
		
		logger.info("DownloadServiceImpl->>> getDownloadItemById End-->");
		return downloadItem;
	}

	@Override
	public DownloadItem saveMultipleDownloadItem(DownloadItem downloadItem) throws Exception {
		logger.info("DownloadServiceImpl->>> saveMultipleDownloadItem Start-->");

		String errorMessage = "SUCCESS";
	
		List<DownloadItem> downloadItemList = new ArrayList<DownloadItem>();

		if(downloadItem.getOkrMasterListTemp().size() > utilityProperties.getMaxOkrDownloadSize()) {
			errorMessage = "MORE-THEN-MAX-SIZE";
		} else {
			
			downloadItem.setCreatedDate(new Date());
			Integer todaysDownload = getDownloadItemListLength(downloadItem);
			
			if(todaysDownload > utilityProperties.getMaxOkrDownloadSize()) {
				errorMessage = "ALREADY-MAX-SIZE";
			} else {
				
				if((todaysDownload+downloadItem.getOkrMasterListTemp().size()) > utilityProperties.getMaxOkrDownloadSize()) {
					errorMessage = "MORE-THEN-MAX-SIZE";
				} else {
					
					for (OkrMaster okrMaster : downloadItem.getOkrMasterListTemp()) {
						
						DownloadItem downloadItemTemp = new DownloadItem();
						downloadItemTemp.setUser(downloadItem.getUser());
						downloadItemTemp.setOkrMaster(okrMaster);
						
						downloadItemTemp = saveDownloadItem(downloadItemTemp);
						
						String query = "Update OkrMaster set totalDownloads=totalDownloads+1,modifiedDate=CURRENT_TIMESTAMP where okrMasterId='"+okrMaster.getOkrMasterId()+"'  ";
						genericDao.customUpdate(query);

						downloadItemList.add(downloadItemTemp);
						
					}
					
				}
				
			}
			
			
		}
		
		downloadItem.setErrorMessage(errorMessage);
		
		if(errorMessage.equalsIgnoreCase("SUCCESS")) {
			createExcelAndShareOnMail(downloadItemList);
		}
		
		logger.info("DownloadServiceImpl->>> saveMultipleDownloadItem End-->");
		return downloadItem;
	}

	private void createExcelAndShareOnMail(List<DownloadItem> downloadItemList) {
		
		String reportPath = reportService.generateOkrDownloadFile(downloadItemList);

		User user = (User) genericDao.findOneLong(downloadItemList.get(0).getUser().getId(), User.class);
		Mail mail =  new Mail();
		mail.setTo(user.getEmail());
		mail.setSubject("Your OKR Reports");
		mail.setFrom(utilityProperties.getFromEmail());
	
		Map<String,Object> modelMap = new HashMap<String, Object>();
		modelMap.put("name", user.getFirstName());
		modelMap.put("companyLogo", utilityProperties.getCompanyLogo());

		List<File> fileAttachments = new ArrayList<File>();
		fileAttachments.add(new File(reportPath));
		mail.setModel(modelMap);
		mail.setFileAttachments(fileAttachments);
		
		mail.setTemplateCode("download-report-template");
		
		final CompletableFuture<String> sendStatus = CompletableFuture.supplyAsync(() -> {
			try {
				emailService.sendDirectEmail(mail);
				return "SUCCESS";
			} catch (Exception e) {
				logger.error("UserServiceImpl sendOtpToUserOTPMail ERROR" + e.getMessage(), e);
				e.printStackTrace();
				return "ERROR";
			}
			
		});
		
	}

}

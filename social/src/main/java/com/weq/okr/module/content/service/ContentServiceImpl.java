package com.weq.okr.module.content.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.weq.okr.module.content.model.ContentMaster;
import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.utility.module.aws.AmazonS3DTO;
import com.weq.utility.module.aws.service.AwsService;
import com.weq.utility.module.common.dao.GenericDao;

@Service
public class ContentServiceImpl implements ContentService {

	private static final Logger logger = Logger.getLogger(ContentServiceImpl.class);


	@Autowired
	private GenericDao genericDao;

	@Autowired
	private AwsService awsService;

	@Override
	public String getContentMasterOptimizeQuery() {
		
		return "SELECT new ContentMaster(cm.contentMasterId, cm.contentType, cm.entityType, cm.fileName, cm.filePath, cm.fileKey, "
				+ " cm.fileNameThumbnail,cm. filePathThumbnail, cm.fileKeyThumbnail, cm.contentTitle, cm.isDelete) FROM ContentMaster cm ";
	}

	
	@Override
	public ContentMaster getContentMasterById(ContentMaster contentMaster) throws Exception {
		logger.info("ContentServiceImpl->>> getContentMasterById --> "+contentMaster.getContentMasterId());
		return (ContentMaster) genericDao.findOneInt(contentMaster.getContentMasterId(), ContentMaster.class);
	}

	@Override
	public List<ContentMaster> getContentMasterList(ContentMaster contentMaster) throws Exception {
		logger.info("ContentServiceImpl->>> getContentMasterList Data -->");
		List<ContentMaster> contentMasterList = new ArrayList<ContentMaster>();
		String query = "FROM ContentMaster cm ";

		query = getContentMasterFilters(contentMaster, query);

		if (contentMaster.getPageNumber() != null) {

			contentMasterList = genericDao.findCustomListPaginate(query, contentMaster.getPageNumber() != null ? contentMaster.getPageNumber() : 0,
					contentMaster.getPageSize() != null ? contentMaster.getPageSize() : 10);
			
		} else {
			contentMasterList = genericDao.findCustomList(query);
		}

		return contentMasterList;

	}
	
	@Override
	public Integer getContentMasterListLength(ContentMaster contentMaster) throws Exception {
		logger.info("ContentServiceImpl->>> getContentMasterListLength Data -->");
		String query = "SELECT count(cm.contentMasterId) from ContentMaster cm ";

		query = getContentMasterFilters(contentMaster, query);

		return genericDao.countCustomList(query).intValue();
	}
	
	public String getContentMasterFilters(ContentMaster contentMaster, String query) {
		logger.info("ContentServiceImpl getContentMasterFilters Data-->");
		
		query += " WHERE cm.isDelete='N' ";
		
		query += " and cm.entityType IS NOT NULL ";

		return query;
	}
	
	@Override
	public ContentMaster saveContentMaster(ContentMaster contentMaster) throws Exception {
		logger.info("ContentServiceImpl ->>> saveContentMaster Data -->");

		if(contentMaster.getContentMasterId()==null || contentMaster.getContentMasterId()==0) {
			contentMaster.setIsDelete("N");
			contentMaster=(ContentMaster) genericDao.save(contentMaster);
		}else {
			contentMaster=(ContentMaster) genericDao.update(contentMaster);
		}
		
		return contentMaster;
	}
		
	@Override
	public void deleteContentMaster(Integer contentMasterId) throws Exception {
		logger.info("ContentServiceImpl->>> deleteContentMaster Data id-->" + contentMasterId);
		String updateQuery = "Update ContentMaster set isDelete='Y',modifiedDate = CURRENT_TIMESTAMP where contentMasterId = '"+contentMasterId+"' ";
		genericDao.customUpdate(updateQuery);
		logger.info("ContentServiceImpl->>> deleteContentMaster Data end id-->" + contentMasterId);
	}

	public void getContentMasterDetails(ContentMaster contentMaster,AmazonS3DTO amazonS3DTO){

		contentMaster.setFileKey(amazonS3DTO.getS3Key());
		contentMaster.setFilePath(amazonS3DTO.getFilePath());
		contentMaster.setFileName(amazonS3DTO.getFileName());
		contentMaster.setFileType(amazonS3DTO.getContentType());
		contentMaster.setFileKeyThumbnail(amazonS3DTO.getS3KeyThumbnail());
		contentMaster.setFilePathThumbnail(amazonS3DTO.getFilePathThumbnail());
		contentMaster.setFileNameThumbnail(amazonS3DTO.getFileNameThumbnail());

	}

	@Override
	public ContentMaster saveContentMasterWithSingleFile(MultipartFile file, ContentMaster contentMaster) throws Exception {

		// file upload logic
		ContentMaster contentMasterNew = uploadContentMasterFile(file);
		setActualContentData(contentMaster,contentMasterNew);
		return saveContentMaster(contentMaster);
	}

	@Override
	public ContentMaster uploadContentMasterFile(MultipartFile file) throws Exception {

		AmazonS3DTO amazonS3DTO = new AmazonS3DTO();
		String fileName = file.getOriginalFilename();
		String key = "OKR/Content/"+ UUID.randomUUID().toString()+"-"+fileName;		
		amazonS3DTO = awsService.uploadDataToAmazonS3(file, key);
//		amazonS3DTO = awsService.uploadDataToOurServer(file);

		ContentMaster contentMaster = new ContentMaster();
		
		getContentMasterDetails(contentMaster, amazonS3DTO);

		return contentMaster;

	}
	
	public void setActualContentData(ContentMaster contentMasterActual, ContentMaster contentMasterNew) {

		contentMasterActual.setFileKey(contentMasterNew.getFileKey());
		contentMasterActual.setFilePath(contentMasterNew.getFilePath());
		contentMasterActual.setFileName(contentMasterNew.getFileName());
		contentMasterActual.setFileType(contentMasterNew.getFileType());
		contentMasterActual.setFileKeyThumbnail(contentMasterNew.getFileKeyThumbnail());
		contentMasterActual.setFilePathThumbnail(contentMasterNew.getFilePathThumbnail());
		contentMasterActual.setFileNameThumbnail(contentMasterNew.getFileNameThumbnail());

		
	}
	
}

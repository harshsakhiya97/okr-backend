package com.weq.okr.module.report.service;

import java.util.List;

import com.weq.okr.module.download.model.DownloadItem;
import com.weq.utility.module.common.model.PredefinedMaster;

public interface ReportService {

	String generateOkrDownloadFile(List<DownloadItem> downloadItemList);

	List<String> getDownloadFileHeading();

	List<List<String>> getOkrMasterData(Integer srno, DownloadItem downloadItem);

}

package com.weq.okr.module.content.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.user.module.user.model.User;
import com.weq.utility.module.common.model.TimestampEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Data
@Entity
@EqualsAndHashCode(callSuper = false)
public class ContentMaster extends TimestampEntity {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Integer contentMasterId;
	
	@Size(max = 20)
	private String contentType;

	@Size(max = 20)
	private String entityType;

	@Size(max = 20)
	private String fileType;

	@Size(max = 100)
	private String fileName;
	
	@Size(max = 300)
	private String filePath;
	
	@Size(max = 200)
	private String fileKey;
	
	@Size(max = 100)
	private String fileNameThumbnail;

	@Size(max = 300)
	private String filePathThumbnail;

	@Size(max = 500)
	private String fileKeyThumbnail;

	@Size(max = 200)
	private String contentTitle;

	@Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;

	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "createdBy")
	private User createdBy;
	
	@Column(name = "createdBy", insertable = false, updatable = false)
	private Long createdById;
	
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "modifiedBy")
	private User modifiedBy;
	
	@Column(name = "modifiedBy", insertable = false, updatable = false)
	private Long modifiedById;

	@Transient
	private Integer pageNumber;

	@Transient
	private Integer pageSize;
	
	@Transient
	private String predefinedMasterNameTemp;
	

	public ContentMaster() {
		
	}
	
	public ContentMaster(Integer contentMasterId,String contentType,String entityType,String fileName,String filePath,String fileKey,String fileNameThumbnail,String filePathThumbnail,String fileKeyThumbnail,String contentTitle,String isDelete) {

		this.contentMasterId = contentMasterId;
		this.contentType = contentType;
		this.entityType = entityType;
		this.fileName = fileName;
		this.filePath = filePath;
		this.fileKey = fileKey;
		this.fileNameThumbnail = fileNameThumbnail;
		this.filePathThumbnail = filePathThumbnail;
		this.fileKeyThumbnail = fileKeyThumbnail;
		this.contentTitle = contentTitle;
		this.isDelete = isDelete;

	}
}
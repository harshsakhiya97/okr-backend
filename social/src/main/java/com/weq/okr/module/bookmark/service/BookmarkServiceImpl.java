package com.weq.okr.module.bookmark.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.weq.okr.module.bookmark.model.BookmarkMaster;
import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.user.module.user.model.User;
import com.weq.utility.module.common.dao.GenericDao;

@Service
public class BookmarkServiceImpl implements BookmarkService{

private static final Logger logger = Logger.getLogger(BookmarkServiceImpl.class);
	
	@Autowired
	private GenericDao genericDao;
	
	@Override
	public BookmarkMaster saveBookmark(BookmarkMaster bookmarkMaster) throws Exception {
		logger.info("BookmarkServiceImpl->>> saveBookmark Start-->");

		bookmarkMaster.setIsDelete("N");
		if(bookmarkMaster.getBookmarkId()==null || bookmarkMaster.getBookmarkId()==0) {
			bookmarkMaster = (BookmarkMaster) genericDao.save(bookmarkMaster);
		}else {
			bookmarkMaster = (BookmarkMaster) genericDao.update(bookmarkMaster);
		
		}	
		
		logger.info("BookmarkServiceImpl->>> saveBookmark End-->");
		return bookmarkMaster;
	}

	@Override
	public void deleteBookmark(BookmarkMaster bookmarkMaster) throws Exception {
		logger.info("BookmarkServiceImpl->>> deleteBookmark Start-->");
		String query = "Delete BookmarkMaster where bookmarkId='"+bookmarkMaster.getBookmarkId()+"'  ";
		genericDao.customUpdate(query);
		logger.info("BookmarkServiceImpl->>> deleteBookmark End-->");
	}

	@Override
	public List<BookmarkMaster> getBookmarkListByUser(BookmarkMaster bookmarkMaster) throws Exception {
		List<BookmarkMaster> bookmarkMasterList = new ArrayList<BookmarkMaster>();
		User user = bookmarkMaster.getCurrentUser();
		logger.info("BookmarkServiceImpl->>> getBookmarkListByUser Start-->");
		String query = "from BookmarkMaster where isDelete='N' ";
		query = getQueryByCriteria(query,bookmarkMaster);
		bookmarkMasterList = genericDao.fetchNextRecords(query,bookmarkMaster.getRecordSize()!=null?bookmarkMaster.getRecordSize():10);
		
		logger.info("BookmarkServiceImpl->>> getBookmarkListByUser End-->");
		return bookmarkMasterList;
	}

	
	private String getQueryByCriteria(String query, BookmarkMaster bookmarkMaster) {
		logger.info("BookmarkServiceImpl->>> getQueryByCriteria Start-->");
		
		if(bookmarkMaster.getCurrentUser()!=null && bookmarkMaster.getCurrentUser().getId()!=0) {
			query = query+ " and createdById='"+bookmarkMaster.getCurrentUser().getId()+"' ";
		}

		if(bookmarkMaster.getOkrMaster()!=null) {
			
			if(bookmarkMaster.getOkrMaster().getOkrMasterId() != null) {
				query = query+ " and okrMasterId='"+bookmarkMaster.getOkrMaster().getOkrMasterId()+"' ";
			}

	        if(bookmarkMaster.getOkrMaster().getIndustry() != null && bookmarkMaster.getOkrMaster().getIndustry().getPredefinedId() != null ) {
	            query += " and okrMaster.industryId = '"+bookmarkMaster.getOkrMaster().getIndustry().getPredefinedId()+"' ";
	        }

	        if(bookmarkMaster.getOkrMaster().getDesignation() != null && bookmarkMaster.getOkrMaster().getDesignation().getPredefinedId() != null ) {
	            query += " and okrMaster.designationId = '"+bookmarkMaster.getOkrMaster().getDesignation().getPredefinedId()+"' ";
	        }

	        if(bookmarkMaster.getOkrMaster().getDesignationLevel() != null && bookmarkMaster.getOkrMaster().getDesignationLevel().getPredefinedId() != null ) {
	            query += " and okrMaster.designationLevelId = '"+bookmarkMaster.getOkrMaster().getDesignationLevel().getPredefinedId()+"' ";
	        }

	        if(bookmarkMaster.getOkrMaster().getDepartment() != null && bookmarkMaster.getOkrMaster().getDepartment().getPredefinedId() != null ) {
	            query += " and okrMaster.departmentId = '"+bookmarkMaster.getOkrMaster().getDepartment().getPredefinedId()+"' ";
	        }

	        if(bookmarkMaster.getOkrMaster().getSubDepartment() != null && bookmarkMaster.getOkrMaster().getSubDepartment().getPredefinedId() != null ) {
	            query += " and okrMaster.subDepartmentId = '"+bookmarkMaster.getOkrMaster().getSubDepartment().getPredefinedId()+"' ";
	        }

	        if(bookmarkMaster.getOkrMaster().getIsDeactive() != null && bookmarkMaster.getOkrMaster().getIsDeactive() != "") {
	            query += " and okrMaster.isDeactive = '"+bookmarkMaster.getOkrMaster().getIsDeactive()+"' ";
	        }

	        if(bookmarkMaster.getOkrMaster().getObjective() != null && bookmarkMaster.getOkrMaster().getObjective() != "") {
	            query += " and okrMaster.objective like '%"+bookmarkMaster.getOkrMaster().getObjective()+"%' ";
	        }

	        if(bookmarkMaster.getOkrMaster().getSearchValue() != null && bookmarkMaster.getOkrMaster().getSearchValue() != "") {
	            query += " and ( okrMaster.objective like '%"+bookmarkMaster.getOkrMaster().getSearchValue()+"%' or okrMaster.keyResult like '%"+bookmarkMaster.getOkrMaster().getSearchValue()+"%' ) ";
	        }

		}

		if(bookmarkMaster.getLastBookmarkId()!=null && bookmarkMaster.getLastBookmarkId()!=0) {
			query = query+ " and bookmarkId < "+bookmarkMaster.getLastBookmarkId();
		}
		
		query = query+ " order by bookmarkId desc ";
		
		logger.info("BookmarkServiceImpl->>> getQueryByCriteria End-->");
		return query;
	}
	
	@Override
	public BookmarkMaster getBookmarkIdByUserAndOkr(BookmarkMaster bookmarkMaster) throws Exception {
		
		String query = "from BookmarkMaster where isDelete='N' ";
		query = getQueryByCriteria(query,bookmarkMaster);

		List<BookmarkMaster> bookmarkMasterList = genericDao.findCustomList(query);
		
		if(bookmarkMasterList.size() > 0) {
			return bookmarkMasterList.get(0);
		} else {
			return new BookmarkMaster();
		}

	}

}

package com.weq.okr.module.savedSearch.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.weq.user.module.user.model.User;
import com.weq.utility.module.common.model.PredefinedMaster;
import com.weq.utility.module.common.model.TimestampEntity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@JsonIgnoreProperties(
value = {"hibernateLazyInitializer", "handler"})
public class SavedSearchMaster extends TimestampEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Integer savedSearchMasterId;

	@Size(max = 200)
	private String searchName;

	@ManyToOne(targetEntity = PredefinedMaster.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "departmentId")
	private PredefinedMaster department;
	
	@Column(name = "departmentId", insertable = false, updatable = false)
	private Integer departmentId;

	@ManyToOne(targetEntity = PredefinedMaster.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "subDepartmentId")
	private PredefinedMaster subDepartment;
	
	@Column(name = "subDepartmentId", insertable = false, updatable = false)
	private Integer subDepartmentId;

	@Column(name="okr_counts", columnDefinition="int(10) default '0'")
	private Integer okrCounts;

	@Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete;
	
	@Column(name = "is_deactive", columnDefinition = "varchar(10) default 'N' ")
	private String isDeactive;
	
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "createdBy")
	private User createdBy;
	
	@Column(name = "createdBy", insertable = false, updatable = false)
	private Long createdById;
	
	@Transient
	private Integer lastSavedSearchMasterId;
	
	@Transient
	private Integer recordSize;
	
	public String getDepartmentName() {
		
		if(department != null && department.getName() != null) {
			return department.getName();
		} else {
			return "";
		}
		
	}

	public String getSubDepartmentName() {
		
		if(subDepartment != null && subDepartment.getName() != null) {
			return subDepartment.getName();
		} else {
			return "";
		}
		
	}

	
}

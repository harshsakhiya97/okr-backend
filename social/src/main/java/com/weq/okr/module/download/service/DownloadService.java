package com.weq.okr.module.download.service;

import java.util.List;

import com.weq.okr.module.download.model.DownloadItem;

public interface DownloadService {

	DownloadItem saveDownloadItem(DownloadItem downloadItem) throws Exception;

	void deleteDownloadItem(DownloadItem downloadItem) throws Exception;

	List<DownloadItem> getDownloadItemList(DownloadItem downloadItem) throws Exception;

	Integer getDownloadItemListLength(DownloadItem downloadItem) throws Exception;

	DownloadItem getDownloadItemById(DownloadItem downloadItem) throws Exception;

	DownloadItem saveMultipleDownloadItem(DownloadItem downloadItem) throws Exception;

}

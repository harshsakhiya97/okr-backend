package com.weq.okr.module.savedSearch.service;

import java.util.List;

import com.weq.okr.module.savedSearch.model.SavedSearchMaster;

public interface SavedSearchService {

	SavedSearchMaster saveSavedSearch(SavedSearchMaster savedSearchMaster) throws Exception;

	void deleteSavedSearch(SavedSearchMaster savedSearchMaster) throws Exception;

	List<SavedSearchMaster> getSavedSearchMasterList(SavedSearchMaster savedSearchMaster) throws Exception;

	Integer getSavedSearchMasterListLength(SavedSearchMaster savedSearchMaster) throws Exception;

}

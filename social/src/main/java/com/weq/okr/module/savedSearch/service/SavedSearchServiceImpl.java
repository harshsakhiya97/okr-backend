package com.weq.okr.module.savedSearch.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.okr.module.okr.service.OkrService;
import com.weq.okr.module.okr.service.OkrServiceImpl;
import com.weq.okr.module.savedSearch.model.SavedSearchMaster;
import com.weq.utility.module.common.dao.GenericDao;

@Service
@Transactional
public class SavedSearchServiceImpl implements SavedSearchService {
	private static final Logger logger = Logger.getLogger(SavedSearchServiceImpl.class);

	@Autowired
	private GenericDao genericDao;

	@Autowired
	private OkrService okrService;

	@Override
	public SavedSearchMaster saveSavedSearch(SavedSearchMaster savedSearchMaster) throws Exception {
		logger.info("SavedSearchMasterServiceImpl->>> saveSavedSearch Start-->");

		if(savedSearchMaster.getSavedSearchMasterId()==null || savedSearchMaster.getSavedSearchMasterId()==0) {

			savedSearchMaster.setIsDeactive("N");
			savedSearchMaster.setIsDelete("N");
			savedSearchMaster.setOkrCounts(0);
			
			savedSearchMaster=(SavedSearchMaster) genericDao.save(savedSearchMaster);
		}else {
			savedSearchMaster=(SavedSearchMaster) genericDao.update(savedSearchMaster);
		}
		
		logger.info("SavedSearchMasterServiceImpl->>> saveSavedSearch End-->");
		return savedSearchMaster;
	}

	@Override
	public void deleteSavedSearch(SavedSearchMaster savedSearchMaster) throws Exception {
		logger.info("OkrServiceImpl->>> deleteOkr Start-->");
		String query = "Update SavedSearchMaster set isDelete='Y',modifiedDate=CURRENT_TIMESTAMP where savedSearchMasterId='"+savedSearchMaster.getSavedSearchMasterId()+"'  ";
		genericDao.customUpdate(query);
		logger.info("OkrServiceImpl->>> deleteOkr End-->");
		
	}

	@Override
	public List<SavedSearchMaster> getSavedSearchMasterList(SavedSearchMaster savedSearchMaster) throws Exception {
		logger.info("OkrServiceImpl->>> getSavedSearchMasterList Start-->");
		
		List<SavedSearchMaster> savedSearchMasterList = new ArrayList<SavedSearchMaster>();
		String query = "select distinct om from SavedSearchMaster om  where om.isDelete='N' ";
				
		query = getQueryByCriteria(query,savedSearchMaster);
		
		savedSearchMasterList = genericDao.fetchNextRecords(query,savedSearchMaster.getRecordSize()!=null?savedSearchMaster.getRecordSize():10);

		setOkrCount(savedSearchMasterList);
		
		logger.info("OkrServiceImpl->>> getSavedSearchMasterList End-->");
		return savedSearchMasterList;
	}
	
	public void setOkrCount(List<SavedSearchMaster> savedSearchMasterList) throws Exception {
		
		for (SavedSearchMaster savedSearchMaster : savedSearchMasterList) {
			
			OkrMaster okrMaster = new OkrMaster();
			okrMaster.setDepartment(savedSearchMaster.getDepartment());
			okrMaster.setSubDepartment(savedSearchMaster.getSubDepartment());
			okrMaster.setSearchValue(savedSearchMaster.getSearchName());
			savedSearchMaster.setOkrCounts(okrService.getOkrMasterListLength(okrMaster));
			
		}
		
		
	}
	
	private String getQueryByCriteria(String query, SavedSearchMaster savedSearchMaster) {
		logger.info("OkrServiceImpl->>> getQueryByCriteria Start-->");
		
		if(savedSearchMaster.getCreatedBy() != null && savedSearchMaster.getCreatedBy().getId() != 0 ) {
			query += " and om.createdBy = '"+savedSearchMaster.getCreatedBy().getId()+"' ";
		}
		
		if(savedSearchMaster.getLastSavedSearchMasterId() !=null && savedSearchMaster.getLastSavedSearchMasterId()!=0) {
			query += " and om.savedSearchMasterId < "+savedSearchMaster.getLastSavedSearchMasterId();
		}

		return query;
	}

	@Override
	public Integer getSavedSearchMasterListLength(SavedSearchMaster savedSearchMaster) throws Exception {
		logger.info("OkrServiceImpl->>> getSavedSearchMasterListLength start");
		
		String query="Select count(om.savedSearchMasterId) from SavedSearchMaster om  where om.isDelete='N' ";	
		
		query = getQueryByCriteria(query,savedSearchMaster);
		logger.info("OkrServiceImpl->>> getSavedSearchMasterListLength end");	
		return genericDao.countCustomList(query).intValue();
	}

}

package com.weq.okr.module.report.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.weq.okr.module.download.model.DownloadItem;
import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.utility.configuration.UtilityProperties;
import com.weq.utility.module.common.dao.GenericDao;

@Service
public class ReportServiceImpl implements ReportService {

	private static final Logger logger = Logger.getLogger(ReportServiceImpl.class);

	@Autowired
	private GenericDao genericDao;

	
	@Autowired
	private UtilityProperties utilityProperties;

	@Override
	public String generateOkrDownloadFile(List<DownloadItem> downloadItemList) {
		
		XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("OKR");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

		Calendar calendarDate = Calendar.getInstance();
		calendarDate.setTime(new Date());

		String fileName = "okr-"+df.format(calendarDate.getTime())+".xlsx";
		
        String excelFilePath = utilityProperties.getDocumentUpload()+fileName;

        try {
        
        	saveHeaderInExportDownloadFile(workbook, sheet);

        	saveDataInExportDownloadFile(workbook, sheet, downloadItemList);

	        FileOutputStream outputStream = new FileOutputStream(excelFilePath);
	        workbook.write(outputStream);
	        workbook.close();

		} catch (IOException e) {
		        System.out.println("File IO error:");
		        e.printStackTrace();
		}
        
		String newFileFullPath = utilityProperties.getReportFullPathFolder()+fileName;
		
		File fileNew = new File(excelFilePath); 

        // renaming the file and moving it to a new location 
        if(fileNew.renameTo 
           (new File(newFileFullPath))) 
        { 
            // if file copied successfully then delete the original file 
        	fileNew.delete(); 
            System.out.println("File moved successfully"); 
        } 
        else
        { 
            System.out.println("Failed to move the file"); 
        } 

        
        
        return utilityProperties.getReportFolder()+fileName;

	}

	public void saveHeaderInExportDownloadFile(XSSFWorkbook workbook, XSSFSheet sheet) {
		
		List<String> headingList = getDownloadFileHeading();

        int rowsCount = 0;

        Row row = sheet.createRow(rowsCount);
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFillForegroundColor(IndexedColors.AQUA.getIndex());
        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        // Creating header
        
        int colsCount = 0;
        
        for (String header: headingList) {
	        Cell cell = row.createCell(colsCount++);
	        cell.setCellValue(header);
	        cell.setCellStyle(headerCellStyle);
		}

	}
	
	@Override
	public List<String> getDownloadFileHeading(){
		
		List<String> headingList = new ArrayList<String>();

		headingList.add("Sr No");
		headingList.add("Objective");
		headingList.add("Key Result");
		headingList.add("Industry");
		headingList.add("Designation");
		headingList.add("Designation Level");
		headingList.add("Department");
		headingList.add("Sub Department");

		return headingList;
	}

	@Override
	public List<List<String>> getOkrMasterData(Integer srno, DownloadItem downloadItem){
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");
		Calendar calendarDate = Calendar.getInstance();

		List<String> rowList1 = new ArrayList<String>();
		
		OkrMaster okrMaster = (OkrMaster) genericDao.findOneInt(downloadItem.getOkrMaster().getOkrMasterId(), OkrMaster.class);
		
		rowList1.add(srno+"");
		rowList1.add(okrMaster.getObjective());
		rowList1.add(okrMaster.getKeyResult());
		rowList1.add(okrMaster.getIndustryName());
		rowList1.add(okrMaster.getDesignationName());
		rowList1.add(okrMaster.getDesignationLevelName());
		rowList1.add(okrMaster.getDepartmentName());
		rowList1.add(okrMaster.getSubDepartmentName());

		List<List<String>> finalList = new ArrayList<List<String>>();
		finalList.add(rowList1);

		return finalList;
	}

	public void saveDataInExportDownloadFile(XSSFWorkbook workbook, XSSFSheet sheet, List<DownloadItem> downloadItemList) {

        int rowsCount = 1;
        int srno = 1;
        
		for (DownloadItem downloadItem : downloadItemList) {

	        Row row = sheet.createRow(rowsCount++);

	        List<List<String>> dataList = getOkrMasterData(srno++, downloadItem);

	        int colsCount = 0;

	        Cell cell = null;

	        List<String> rowList1 = dataList.get(0);
	        
	        for (String data : rowList1) {
	        	cell = row.createCell(colsCount++);
	        	cell.setCellValue(data);
				
			}
	        
	        int index = 0; 
	        int productColCount = 0;
			int productRowCount = rowsCount;

	        
	        for (List<String> datas: dataList) {

		        if(index > 0) {

			        int index1 = 0; 
		        	for (String data : datas) {

			        	cell = row.createCell(productColCount+(index1++));
			        	cell.setCellValue(data);

					}

					row = sheet.createRow(rowsCount++);

		        } else {

		        	for (String data : datas) {
		        		productColCount++;
					}

		        }
		        
		        index++;
		        
			}
	        
		}
		
	}

}

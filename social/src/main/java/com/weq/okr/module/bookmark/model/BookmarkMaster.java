package com.weq.okr.module.bookmark.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.weq.okr.module.okr.model.OkrMaster;
import com.weq.user.module.user.model.User;
import com.weq.utility.module.common.model.TimestampEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString(exclude = { "okrMaster" })
@JsonIgnoreProperties(
value = {"hibernateLazyInitializer", "handler"})
public class BookmarkMaster extends TimestampEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Integer bookmarkId;
	
	@ManyToOne(targetEntity = OkrMaster.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "okrMasterId")
	private OkrMaster okrMaster;
	
	@Column(name = "okrMasterId", insertable = false, updatable = false)
	private Integer okrMasterId;
	
	@Column(name = "is_delete", columnDefinition = "varchar(10) default 'N' ")
	private String isDelete; 
	
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "createdBy")
	private User createdBy;
	
	@Column(name = "createdBy", insertable = false, updatable = false)
	private Long createdById;
	
	@ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER,optional=true)
	@JoinColumn(name = "modifiedBy")
	private User modifiedBy;
	
	@Column(name = "modifiedBy", insertable = false, updatable = false)
	private Long modifiedById;
	
	@Transient
	private User currentUser;
	
	@Transient
	private Integer lastBookmarkId;
	
	@Transient
	private Integer recordSize;

}

package com.weq.okr.module.notification.service;

import java.util.List;

import com.weq.okr.module.notification.model.NotificationMaster;

public interface NotificationService {

	NotificationMaster saveNotification(NotificationMaster notificationMaster) throws Exception;

	List<NotificationMaster> getNotificationListByUser(NotificationMaster notificationMaster) throws Exception;
	
	void updateNotificationListByUser(NotificationMaster notificationMaster) throws Exception;
	
	Integer getUnseenNotificationCountByUser(NotificationMaster notificationMaster) throws Exception;

}

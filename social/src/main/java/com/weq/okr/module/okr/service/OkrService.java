package com.weq.okr.module.okr.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.weq.okr.module.okr.model.OkrMaster;

public interface OkrService {

	OkrMaster saveOkr(OkrMaster okrMaster,List<MultipartFile> okrReportFileList) throws Exception;

	void deleteOkr(OkrMaster okrMaster) throws Exception;

	List<OkrMaster> getOkrMasterList(OkrMaster okrMaster) throws Exception;

	Integer getOkrMasterListLength(OkrMaster okrMaster) throws Exception;

	void activeDeactiveOkr(OkrMaster okrMaster) throws Exception;

	OkrMaster getOkrById(OkrMaster okrMaster) throws Exception;

	List<OkrMaster> saveImportOkrMaster(List<OkrMaster> okrMasterList) throws Exception;

}
